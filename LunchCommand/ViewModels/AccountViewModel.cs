﻿using System;
using System.ComponentModel.DataAnnotations;
using LunchCommand.Core.Models;

namespace LunchCommand.ViewModels
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }
    }

    public class PasswordChangeViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

    }

    public class PasswordResetViewModel
    {
        [Display(Name = "User name")]
        [Required]
        public string Username { get; set; }
        
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        [Required]
        public string Email { get; set; }
    }

    public class ManageAccountViewModel
    {
        [Display(Name = "User name")]
        public string Username { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [MaxLength(15)]
        [MinLength(2)]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [MaxLength(15)]
        [MinLength(2)]
        public string LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        [Required]
        public string Email { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Confirm Email")]
        [Required]
        [Compare("Email", ErrorMessage = "The email and confirmation email do not match.")]
        public string ConfirmEmail { get; set; }

        public NotificationSettingsViewModel NotificationSettings { get; set; } = new NotificationSettingsViewModel();
    }

    public class NotificationSettingsViewModel
    {
        /// <summary>
        /// Gets or sets the medium by which the user wishes to revieve notifications.
        /// </summary>
        [Required]
        [EnumDataType(typeof(NotificationMedium))]
        [Display(Name = "Notification Type")]
        public NotificationMedium Medium { get; set; } = NotificationMedium.Browser;

        /// <summary>
        /// If they want to receive notifications when they have not yet decided whether or not they are going to lunch
        /// </summary>
        [Required]
        [Display(Name = "Remind me to decide if I am going to lunch")]
        public Boolean NotifyNotDecided { get; set; } = true;

        /// <summary>
        /// If they want to receive notifications when lunch has been decided
        /// </summary>
        [Required]
        [Display(Name = "Notify me when it is lunch time")]
        public Boolean NotifyLunchStarted { get; set; } = true;

        /// <summary>
        /// If the user should be notified of the start of lunch even if they hadn't decided whether or not they want to go
        /// </summary>
        [Required]
        [Display(Name = "Even if I haven't decided to go yet.")]
        public Boolean NotifyLunchStartedEvenWhenNotDecided { get; set; } = true;

        /// <summary>
        /// If they want to recive a notifiation when a venue is proposed
        /// </summary>
        [Required]
        [Display(Name = "Notify me when another user proposes a venue for lunch.")]
        public Boolean NotifyVenueProposed { get; set; } = true;

        /// <summary>
        /// How long before lunch starts to tell the user they have not decided
        /// </summary>
        [Required]
        [Display(Name = "minutes before lunchtime.")]
        [Range(1,30)]
        public Int16 MinutesToNotifyOfIndecision { get; set; } = 10;
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [MaxLength(32)]
        [MinLength(5)]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [MaxLength(15)]
        [MinLength(2)]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [MaxLength(15)]
        [MinLength(2)]
        public string LastName { get; set; }

        [Display(Name = "Invite Code")]
        public string InviteCode { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        [Required]
        public string Email { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "Confirm Email")]
        [Required]
        [Compare("Email", ErrorMessage = "The email and confirmation email do not match.")]
        public string ConfirmEmail { get; set; }
    }
}