﻿using System;

namespace LunchCommand.ViewModels
{
    /// <summary>
    /// Has a users name and whether or not they are going to lunch today
    /// </summary>
    public class UserStatusViewModel
    {
        public String UserName { get; set; }

        public UserDecision Decision { get; set; }
    }

    public enum UserDecision
    {
        // The order here is important for sorting

        Going,
        Unanswered,
        Undecided,
        NotGoing
    }
}