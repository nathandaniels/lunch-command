﻿using LunchCommand.Core.Models;
using System;
using System.ComponentModel.DataAnnotations;

namespace LunchCommand.ViewModels
{
    public class TeamViewModel
    {
        public TeamViewModel()
        {
            this.LunchStartTimeUtc = "11:45 AM";
        }

        public TeamViewModel(Team team)
        {
            this.Name = team.Name;
            this.TeamAddress = team.TeamAddress;
            this.TeamId = team.TeamId;
            this.LunchStartTimeUtc = team.LunchStartTimeUtc.ToString(@"hh\:mm");
        }

        public Int32 TeamId { get; set; }

        [MaxLength(50)]
        [MinLength(3)]
        [Required]
        [Display(Name = "Team Name")]
        public String Name { get; set; }

        [Display(Name = "Physical Address")]
        public String TeamAddress { get; set; }

        [Display(Name = "Lunch Start Time (UTC)")]
        [Required]
        [MinLength(4), MaxLength(8)]
        public String LunchStartTimeUtc { get; set; }

        public TimeSpan? ParsedTime
        {
            get
            {
                try
                {
                    return DateTime.Parse(this.LunchStartTimeUtc).TimeOfDay;
                }
                catch
                {
                    return null;
                }
            }
        }

        public void UpdateTeam(Team team)
        {
            team.Name = this.Name;
            team.TeamAddress = this.TeamAddress;
            team.LunchStartTimeUtc = this.ParsedTime.Value;
            if (team.AdminRole != null)
            {
                team.AdminRole.Name = String.Format("{0} Team Administrator", this.Name);
            }
        }
    }
}