﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchCommand.ViewModels
{
    public class SendInvitationViewModel
    {
        public const Int32 MaxAddressCount = 5; 

        public SendInvitationViewModel()
        {
            this.EmailAddresses = new List<String>();
        }

        public IList<String> EmailAddresses { get; set; }

        public IEnumerable<String> ValidAddresses
        {
            get
            {
                return this.EmailAddresses.Where(e => !String.IsNullOrWhiteSpace(e) && e.IndexOf('@') >= 1 && e.LastIndexOf('.') >= 3);
            }
        }
    }
}