﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LunchCommand.ViewModels
{
    public class PropositionViewModel
    {
        public String Proposer { get; set; }

        public IEnumerable<String> Followers { get; set; }

        public Int32 PropositionId { get; set; }

        public Boolean IsUserGoing { get; set; }

        public String Venue { get; set; }
    }
}