﻿using System;
using LunchCommand.Core.Models;

namespace LunchCommand.ViewModels
{
    public class VenueChoiceViewModel
    {
        public VenueChoiceViewModel()
        {

        }

        public VenueChoiceViewModel(Venue venue, DegreeOfPreference prefer, Int32 otherUsers, Boolean isNew)
        {
            this.VenueName = venue.Name;
            this.VenueId = venue.VenueId;
            this.OtherUsers = otherUsers;
            this.Preference = prefer;
            this.IsNew = isNew;
            this.GenreName = venue.Genre.Name;
        }

        public String VenueName { get; set; }

        public String GenreName { get; set; }

        public Int32 VenueId { get; set; }

        public Int32 OtherUsers { get; set; }

        public DegreeOfPreference Preference { get; set; }

        /// <summary>
        /// Gets or sets whether or not this venue is new to the user (they have not updated their prefs since it was created)
        /// </summary>
        public Boolean IsNew { get; set; }
    }
}