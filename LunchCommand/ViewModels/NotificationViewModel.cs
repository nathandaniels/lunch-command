﻿using System;
using LunchCommand.Core.Models;
using Newtonsoft.Json;

namespace LunchCommand.ViewModels
{
    public class NotificationViewModel
    {
        public NotificationViewModel(Notification notification)
        {
            this.Type = notification.Type;
            this.Message = notification.Message;
            this.PropositionId = notification.PropositionId;
        }

        public String Message { get; set; }

        public NotificationType Type { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public Int32? PropositionId { get; set; }
    }
}
