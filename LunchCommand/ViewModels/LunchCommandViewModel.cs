﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchCommand.Core.Models;

namespace LunchCommand.ViewModels
{
    /// <summary>
    /// The view model for the main page.  It's kind of a big deal
    /// </summary>
    public class LunchCommandViewModel
    {
        public LunchCommandViewModel()
        {
            this.VenueChoicesForUser = new List<VenueChoiceViewModel>();
        }

        public LunchCommandViewModel(User user, IEnumerable<Venue> allVenues)
        {
            this.User = user;
            this.VenueChoicesForUser = allVenues.Any()
                ? allVenues.OrderBy(v => v.Name)
                    .Select(v => new VenueChoiceViewModel(
                        v, // Venue
                        user.VenueDecisions.Where(vc => vc.Venue.VenueId == v.VenueId)
                            .Select(vc => vc.Degree)
                            .FirstOrDefault(), // Degree of preference
                        v.UsersWhoChose.Count(u => u.Degree == DegreeOfPreference.WantToGo), // Count of other users
                        v.DateCreated > user.LastTimeVenueDecisionMade && v.CreatedBy != null &&
                        v.CreatedBy.Id != user.Id)).ToArray()
                : new VenueChoiceViewModel[0]; // Is new
        }

        /// <summary>
        /// The user
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// The user's venue decisions
        /// </summary>
        public IEnumerable<VenueChoiceViewModel> VenueChoicesForUser { get; set; }
        
        /// <summary>
        /// (This is from the client) whether or not the user clicked Save Changes at the bottom of the venue column.
        /// We need to know this because when they make a lunch decision, it is the same postback as when they save venue choices
        /// </summary>
        public Boolean VenueChoicesMade { get; set; }

        /// <summary>
        /// Whether or not any venue is new to the user
        /// </summary>
        public Boolean AnyVenuesNew
        {
            get
            {
                return this.VenueChoicesForUser.Any(v => v.IsNew);
            }
        }
    }
}