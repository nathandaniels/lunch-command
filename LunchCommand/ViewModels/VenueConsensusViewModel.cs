﻿using System;
using LunchCommand.Core.Models;

namespace LunchCommand.ViewModels
{
    public class VenueConsensusViewModel
    {
        public VenueViewModel Venue { get; set; }

        public Int32 PercentageOfUsers { get; set; }
    }
}