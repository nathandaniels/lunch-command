﻿using System;
using System.Collections.Generic;

namespace LunchCommand.ViewModels
{
    public class PropositionListViewModel
    {
        public IEnumerable<PropositionViewModel> Propositions { get; set; }

        public IEnumerable<VenueViewModel> RemainingVenuesToPick { get; set; }

        public Boolean LunchHasStarted { get; set; }

        public IList<String> UndecidedUsers { get; set; }
    }
}