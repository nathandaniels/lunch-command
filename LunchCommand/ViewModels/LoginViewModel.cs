﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LunchCommand.ViewModels
{
    public class LoginViewModel
    {
        public String UserName { get; set; }

        public String Password { get; set; }

        public String ErrorMessage { get; set; }
    }
}