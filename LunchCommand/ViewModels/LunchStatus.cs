﻿namespace LunchCommand.ViewModels
{
    public enum LunchStatus
    {
        StartedWithConsensus = 0,
        StartedWithoutConsensus,
        NotStarted,
        DidNotHappen,
        UserHasDecidedManually
    }
}