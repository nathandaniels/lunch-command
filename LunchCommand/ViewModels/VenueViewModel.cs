﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using LunchCommand.Core.Models;

namespace LunchCommand.ViewModels
{
    public class VenueViewModel
    {
        public VenueViewModel()
        {

        }

        public VenueViewModel(Venue venue)
        {
            this.VenueId = venue.VenueId;
            this.Name = venue.Name;
            this.DaysOfWeek = venue.DaysOfWeek;
            this.Description = venue.Description;
            this.Address = venue.Address;
            this.IsQuick = venue.IsQuick;
            this.MinWeeksBetweenVisits = venue.MinWeeksBetweenVisits;
            this.IsSpecial = venue.IsSpecial;
            this.GenreID = venue.GenreID;
        }

        public Int32 VenueId { get; set; }

        [MaxLength(50)]
        [MinLength(3)]
        [Required]
        public String Name { get; set; }

        [MaxLength(1000)]
        [MinLength(10)]
        public String Description { get; set; }

        [MaxLength(1000)]
        [MinLength(10)]
        public String Address { get; set; }

        public Boolean IsQuick { get; set; }

        [Required]
        public Int32 MinWeeksBetweenVisits { get; set; }

        public VenueDaysOfWeek DaysOfWeek { get; set; }

        public Boolean IsSpecial { get; set; }

        public Int32 GenreID { get; set; }

        public void UpdateModel(Venue model)
        {
            model.Name = this.Name;
            model.DaysOfWeek = this.DaysOfWeek;
            model.Description = this.Description;
            model.Address = this.Address;
            model.IsQuick = this.IsQuick;
            model.MinWeeksBetweenVisits = this.MinWeeksBetweenVisits;
            model.IsSpecial = this.IsSpecial;
            model.GenreID = this.GenreID;
            model.Genre = null;
        }
    }
}