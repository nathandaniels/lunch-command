﻿using System;
using System.Collections.Generic;

namespace LunchCommand.ViewModels
{
    public class TeamMembersViewModel
    {
        public IList<TeamMemberViewModel> Members { get; set; }
    }

    public class TeamMemberViewModel
    {
        public Int32 UserId { get; set; }
        public String Name { get; set; }
        public Boolean IsAdmin { get; set; }
    }
}