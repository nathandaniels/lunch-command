﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchCommand.Core.Models;
using Newtonsoft.Json;

namespace LunchCommand.ViewModels
{
    public class LunchStateViewModel
    {
        public LunchStateViewModel(Lunch lunch, User user, IEnumerable<User> allUsers, IEnumerable<Venue> possibleVenues)
        {
            // Get the list of users going today and those not going
            this.UsersToday =
                allUsers.Select(u =>
                {
                    var decision = lunch.Users.FirstOrDefault(u2 => u2.UserId == u.Id);

                    var userDecision = UserDecision.Undecided;

                    if (decision != null)
                    {
                        userDecision = decision.Decision ? UserDecision.Going : UserDecision.NotGoing;
                    }
                    return new UserStatusViewModel { UserName = u.FullName, Decision = userDecision };

                }).OrderBy(z => z.Decision);


            this.PossibleVenues = possibleVenues.Distinct().Select(v => new VenueViewModel(v));
            var thisUsersDecision = user.LunchesDecided.FirstOrDefault(ld => ld.LunchId == lunch.LunchId);
            this.UserDecision = thisUsersDecision == null ? UserDecision.Unanswered : thisUsersDecision.Decision ? UserDecision.Going : UserDecision.NotGoing;
        }

        public LunchStatus Status { get; set; }

        public String StatusText
        {
            get
            {
                switch (this.Status)
                {
                    case LunchStatus.DidNotHappen:
                        return "There was no lunch today";
                    case LunchStatus.StartedWithConsensus:
                        return "Lunch has been decided!";
                    case LunchStatus.StartedWithoutConsensus:
                        return "Lunch has been decided without full consensus:";
                    case LunchStatus.NotStarted:
                        return "Lunch has not been decided yet";
                    case LunchStatus.UserHasDecidedManually:
                        return "You are in the group for";
                    default:
                        return "";
                }
            }
        }

        /// <summary>
        /// The amount of time to travel to the decided venue
        /// </summary>
        public Int32? MinutesToTravel { get; set; }

        /// <summary>
        /// The venue that was decided
        /// </summary>
        public VenueConsensusViewModel DecidedVenue { get; set; }

        /// <summary>
        /// The venu for the users in a hurry
        /// </summary>
        public VenueConsensusViewModel QuickVenue { get; set; }

        /// <summary>
        /// The venu for the most picky users
        /// </summary>
        public VenueConsensusViewModel DissentingVenue { get; set; }

        /// <summary>
        /// All of the other users today.
        /// </summary>
        public IEnumerable<UserStatusViewModel> UsersToday { get; private set; }

        /// <summary>
        /// The collection of today's possible venues
        /// </summary>
        public IEnumerable<VenueViewModel> PossibleVenues { get; set; }

        [JsonIgnore]
        public TimeSpan? TimeLeft { get; set; }

        /// <summary>
        /// Gets the total number of milliseconds remaining
        /// </summary>
        public Int32 TimeRemaining
        {
            get
            {
                if (this.TimeLeft == null)
                {
                    return 0;
                }

                return (int)this.TimeLeft.Value.TotalMilliseconds;
            }
        }

        /// <summary>
        /// If the current user is going to lunch
        /// </summary>
        public UserDecision UserDecision { get; private set; }
    }
}