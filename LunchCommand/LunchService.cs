﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchCommand.Core.Models;
using LunchCommand.Core.Models.Logging;
using LunchCommand.Core.Services.Maps;
using LunchCommand.ViewModels;
using LunchCommand.Core.Models.Settings;
using LunchCommand.Core.Models.Data;
using LunchCommand.Core.Services;
using LunchCommand.Core.Data.Services;

namespace LunchCommand
{
    // TODO:  Move [most of ]this to the core assembly

    /// <summary>
    /// All of the lunchy intelligence of this application
    /// </summary>
    public class LunchService : BaseService, ILunchService
    {
        private const bool QUICK = true;
        private const bool NOT_QUICK = false;
        private readonly bool? QUICK_OR_NOT = null;

        /// <summary>
        /// Absolutely makes sure there can't be more than one lunch decision process going on at one time
        /// </summary>
        private static object lunchDecisionLock = new object();

        /// <summary>
        /// The number of days after which venues and genres are considered equally old
        /// </summary>
        private readonly Int32 maxDays;

        /// <summary>
        /// The proposition service
        /// </summary>
        private IPropositionService PropositionService { get; }

        /// <summary>
        /// The user repository
        /// </summary>
        private IUserRepository UserRepository { get; }

        /// <summary>
        /// Constructor of the service
        /// </summary>        
        /// <param name="settings">The settings</param>
        /// <param name="logger">The logger</param>
        public LunchService(
            ILunchRepository lunchRepository,
            IPropositionService propositionService,
            IUserRepository userRepository,
            IMapService mapService,
            ISettings settings,
            ILogger logger) : base(lunchRepository, settings, logger)
        {
            this.UserRepository = userRepository;
            this.MapService = this.MapService;
            this.maxDays = settings.NumberOfDaysBeforeVenuesAreConsideredEquallyOld;
            this.PropositionService = propositionService;
        }

        /// <summary>
        /// The service to get the travel time to the selected venue
        /// </summary>
        private IMapService MapService { get; set; }

        /// <summary>
        /// Generates a LunchCommandViewModel for the provided user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public LunchCommandViewModel GenerateViewModel(User user)
        {
            var allVenues = user.Team.ActiveVenues.Where(v => v.TeamId == user.TeamId).ToArray();

            return new LunchCommandViewModel(user, allVenues);
        }

        /// <summary>
        /// Processes the lunch day decision of the specified user
        /// </summary>
        /// <param name="user">The user</param>
        /// <param name="decision">The decision</param>
        public void ProcessUserDecision(User user, UserDecision decision)
        {
            var lunch = this.GetOrCreateLunch(user.Team);
            var pastDecision = user.LunchesDecided.FirstOrDefault(l => l.Lunch.LunchId == lunch.LunchId);
            
            switch (decision)
            {
                case UserDecision.NotGoing:
                    this.PropositionService.RemoveUserFromAllPropositions(user, lunch);
                    goto case UserDecision.Going;
                case UserDecision.Going:
                    if (pastDecision == null)
                    {
                        user.LunchesDecided.Add(new LunchUser()
                        {
                            Decision = decision == UserDecision.Going,
                            Lunch = lunch,
                            User = user
                        });
                    }
                    else
                    {
                        pastDecision.Decision = decision == UserDecision.Going;
                    }

                    break;
                case UserDecision.Unanswered:
                    if (pastDecision != null)
                    {
                        user.LunchesDecided.Remove(pastDecision);
                    }
                    break;
                default:
                    throw new NotImplementedException();
            }
            
            this.UserRepository.Update(user);
        }

        /// <summary>
        /// Whne the view model comes back in a POST, it only has whan was in the form fields.
        /// This method processes the fields the user changed and then recreated the view model
        /// for the page to refresh with
        /// </summary>
        /// <param name="vm">The view model that came back from the client</param>
        public void RehydrateAndProcessViewModel(ref LunchCommandViewModel vm)
        {
            var user = this.UserRepository.FindById(vm.User.Id);
            var lunch = this.GetOrCreateLunch(user.Team);

            var venueChoices = vm.VenueChoicesForUser;
            var restoreChoices = true;

            // Venue Choices
            if (vm.VenueChoicesMade)
            {
                restoreChoices = false;
                user.VenueDecisions.Clear();
                user.LastTimeVenueDecisionMade = DateTime.Now;

                // Yeses
                foreach (var v in vm.VenueChoicesForUser)
                {
                    user.VenueDecisions.Add(
                        new UserVenueChoice()
                        {
                            VenueId = v.VenueId,
                            Degree = v.Preference,
                            UserId = user.Id
                        });
                }
            }

            this.UserRepository.Update(user);

            // reload lunch
            lunch = this.LunchRepository.FindById(lunch.LunchId);

            vm = this.GenerateViewModel(user);

            if (restoreChoices)
            {
                foreach (var vc in vm.VenueChoicesForUser)
                {
                    var oldVc = venueChoices.FirstOrDefault(vc2 => vc2.VenueId == vc.VenueId);
                    if (oldVc != null)
                    {
                        vc.Preference = oldVc.Preference;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the status of today's lunch.  If it's lunchtime and the venue hasn't been picked yet,
        /// this method picks the venue and saves that decision to the db.
        /// </summary>
        /// <param name="team">The team to get the lunch for</param>
        /// <returns>The status of today's lunch</returns>
        public LunchStateViewModel GetStatus(User user)
        {
            LunchStateViewModel state;
            var allVenues = user.Team.ActiveVenues.Where(v => v.TeamId == user.TeamId).ToArray();

            var started = false;

            lock (lunchDecisionLock)
            {
                var lunch = this.GetOrCreateLunch(user.Team);

                state = new LunchStateViewModel(
                    lunch, 
                    user,
                    user.Team.Users.ToArray(), 
                    this.GetPossibleVenues(
                        allVenues, 
                        null, 
                        lunch.GetUsersWhoAreGoingAndNotWithAProposedLunch()).Venues);

                // If it's not lunchtime...
                if (lunch.LunchTimeUtc > DateTime.UtcNow)
                {
                    state.TimeLeft = lunch.LunchTimeUtc - DateTime.UtcNow;
                    state.Status = LunchStatus.NotStarted;
                }
                else
                {
                    started = true;

                    // It's lunctime!
                    // Remove users who alreaady know where they are going
                    var users = lunch.GetUsersWhoAreGoingAndNotWithAProposedLunch();
                    if (lunch.Venue == null && users.Count() > 0)
                    {
                        this.Logger.Log(LogEntryType.Info, "Lunch status requested but lunch needs to be decided first");

                        // Try to get a consensus for the main venue
                        var possibilties = this.GetPossibleVenues(allVenues, NOT_QUICK, users);
                        if (!possibilties.Venues.Any())
                        {
                            this.Logger.Log(LogEntryType.Info, "Could not find any non-quick venues.  Resorting to quick ones for the main venue");
                            possibilties = this.GetPossibleVenues(allVenues, QUICK, users);
                        }

                        var consensus = this.GetConsensus(possibilties.Venues, lunch, NOT_QUICK);

                        // If that fails, in all likelyhood, no one wanted to go to lunch today
                        if (consensus == null)
                        {
                            this.Logger.Log(LogEntryType.Info, "Lunch could not be determined today");
                            state.Status = LunchStatus.DidNotHappen;
                        }
                        else
                        {
                            lunch.Venue = consensus;
                            lunch.PercentOfUsersAgreeing = possibilties.PercentHappy;

                            // Handle the dissenting venue, if there needs to be one
                            if (possibilties.UnhappyUsers.Count() > 0)
                            {
                                this.Logger.Log(LogEntryType.Info, "Looking up a dissenting venue");

                                var dissentingPossibilities = this.GetPossibleVenues(allVenues, QUICK_OR_NOT, possibilties.UnhappyUsers);

                                // Get the possible venues for the users who don't like the chosen one
                                var dissentingConsesus = this.GetConsensus(dissentingPossibilities.Venues, lunch, NOT_QUICK);

                                if (dissentingConsesus != null)
                                {
                                    lunch.DissentingVenue = dissentingConsesus;
                                    lunch.PercentOfUsersDissenting = dissentingPossibilities.PercentHappy;
                                }
                            }

                            // Handle the quick venue.  If the decided venue is already quick, use that
                            if (lunch.Venue.IsQuick)
                            {
                                lunch.QuickVenue = lunch.Venue;
                                lunch.PercentOfUsersAgreeingToQuick = lunch.PercentOfUsersAgreeing;
                            }
                            else
                            {
                                this.Logger.Log(LogEntryType.Info, "Looking up a quick venue");

                                var quickPossibilties = this.GetPossibleVenues(allVenues, QUICK, users);

                                consensus = this.GetConsensus(quickPossibilties.Venues, lunch, QUICK);

                                if (consensus != null)
                                {
                                    lunch.QuickVenue = consensus;
                                    lunch.PercentOfUsersAgreeingToQuick = possibilties.PercentHappy;
                                }
                            }
                        }

                        this.LunchRepository.Save(lunch);
                    }
                    else // If lunch has been decided in the past
                    {
                        if (lunch.Venue != null)
                        {
                            state.PossibleVenues.Concat(new[] { new VenueViewModel(lunch.Venue) });
                        }
                        if (lunch.QuickVenue != null)
                        {
                            state.PossibleVenues.Concat(new[] { new VenueViewModel(lunch.QuickVenue) });
                        }
                    }
                } // End the lock, finally

                Venue venueToLookUpTime = null;

                var group = lunch.Propositions.FirstOrDefault(p => p.ProposerId == user.Id || p.Followers.Any(f => f.UserId == user.Id));
                if (group != null)
                {
                    state.Status = LunchStatus.UserHasDecidedManually;
                    state.DecidedVenue = new VenueConsensusViewModel() { Venue = new VenueViewModel(group.Venue) };
                    venueToLookUpTime = group.Venue;
                }
                else if (started)
                {
                    {
                        // Build the status
                        state.DecidedVenue = lunch.Venue == null ? null : new VenueConsensusViewModel() { Venue = new VenueViewModel(lunch.Venue), PercentageOfUsers = lunch.PercentOfUsersAgreeing.GetValueOrDefault(0) };
                        state.DissentingVenue = lunch.DissentingVenue == null ? null : new VenueConsensusViewModel() { Venue = new VenueViewModel(lunch.DissentingVenue), PercentageOfUsers = lunch.PercentOfUsersDissenting.GetValueOrDefault(0) };
                        state.QuickVenue = lunch.QuickVenue == null ? null : new VenueConsensusViewModel() { Venue = new VenueViewModel(lunch.QuickVenue), PercentageOfUsers = lunch.PercentOfUsersAgreeingToQuick.GetValueOrDefault(0) };

                        state.Status = state.DecidedVenue == null ? LunchStatus.DidNotHappen : state.DissentingVenue == null ? LunchStatus.StartedWithConsensus : LunchStatus.StartedWithoutConsensus;

                        venueToLookUpTime = lunch.Venue;
                    }
                }

                try
                {
                    // Try to get the travel time to the decided venue
                    if (state.DecidedVenue != null && state.DecidedVenue.Venue != null)
                    {
                        var time = VenueTravelCache.GetTravelTime(lunch, venueToLookUpTime, user.Team.TeamAddress, this.MapService);

                        if (time.TravelTime.HasValue)
                        {
                            state.MinutesToTravel = (int)time.TravelTime.Value.TotalMinutes;
                        }
                    }
                }
                catch { }

                return state;
            }
        }

        /// <summary>
        /// Gets the venue to go to
        /// </summary>
        /// <param name="possibilities">The possible venues</param>
        /// <param name="forLunch">The lunch this is for</param>
        /// <returns>The venue decided upon</returns>
        private Venue GetConsensus(IEnumerable<Venue> possibilities, Lunch forLunch, Boolean forQuick)
        {
            if (!possibilities.Any())
            {
                this.Logger.Log(LogEntryType.Info, "There are no possibilities.  There can be no consensus");

                return null;
            }

            // narrow results down to the least-recently visited genres
            var genreDays = possibilities.Select(p => new { p = p, genreDays = Math.Min(p.Genre.GetDaysSinceLastVisited(forQuick, DateTime.MinValue), this.maxDays) });
            possibilities = genreDays.Where(p => p.genreDays == genreDays.Max(gd => gd.genreDays)).Select(p => p.p);

            this.Logger.Log(LogEntryType.Info, String.Format("Found {0} venues with genres visited least-recently", possibilities.Count()));

            if (possibilities.Count() <= 1)
            {
                return possibilities.FirstOrDefault();
            }
            else
            {
                // Further narrow down the choice to the least-recently visited venue
                // maxF is the oldest venue's age
                var maxf = possibilities
                    .Select(p2 => Math.Min(this.maxDays, p2.GetDaysSinceLastVisit(forQuick, DateTime.MinValue))).Max();

                possibilities = possibilities.Where(p =>
                    Math.Min(this.maxDays, p.GetDaysSinceLastVisit(forQuick, DateTime.MinValue)) == maxf);

                this.Logger.Log(LogEntryType.Info, String.Format("Found {0} venues visited least-recently", possibilities.Count()));

                if (possibilities.Count() <= 1)
                {
                    return possibilities.FirstOrDefault();
                }
                else
                {
                    var possibilitiesArray = possibilities.ToArray();
                    // Finally, just pick one at random
                    var index = new Random().Next(0, possibilitiesArray.Length);

                    this.Logger.Log(LogEntryType.Info, String.Format("Resorting to random venue.  Index: {0}", index));

                    return possibilitiesArray[index];
                }
            }
        }

        /// <summary>
        /// Gets all venues and the percentage of users in this lunch that want to go to them
        /// </summary>
        /// <param name="allVenues">The venues to choose from</param>
        /// <param name="quick">If true, only quick venues are considered.  If false, only not-quick or special venues are considered.  If null, all venues are considered</param>
        /// <param name="users">The users to consider</param>
        /// <returns></returns>
        internal VenueFilterResult GetPossibleVenues(IEnumerable<Venue> allVenues, Boolean? quick, IEnumerable<User> allUsers)
        {
            // Before we begin, remove any venue that will be impossible today.
            var filteredVenues = allVenues
                // Limit to quick/not-quick
                .Where(vc => vc.IsSpecial || !quick.HasValue || quick.Value == vc.IsQuick)
                // If we are looking at not-quick venues, remove any who have been visited too recently
                .Where(vc => (quick.HasValue && quick.Value) || (!vc.LastVisited.HasValue || ((DateTime.Now - vc.LastVisited.Value).TotalDays / 7.0) > vc.MinWeeksBetweenVisits))
                // If we are looking at quick venues, remove any who have been visited too recently
                .Where(vc => (!quick.HasValue || !quick.Value) || (!vc.LastVisitedAsQuick.HasValue || ((DateTime.Now - vc.LastVisitedAsQuick.Value).TotalDays / 7.0) > vc.MinWeeksBetweenVisits))
                // Remove any we can't visit today
                .Where(vc => vc.DaysOfWeek.CanGoOnDay(DateTime.Now.DayOfWeek))
                .Where(vc => !vc.Deleted).Distinct().ToArray();

            var filteredVenueIds = new HashSet<Int32>(filteredVenues.Select(f => f.VenueId));

            // No use is doing anything else if there are no possible venues
            if (!filteredVenues.Any() || !allUsers.Any())
            {
                return new VenueFilterResult() { Venues = new Venue[0] };
            }

            // Here's what we will return
            var possibleVenues = new List<Venue>();

            // Order the users so the most-picky is on top so we can remove them later if we need to
            allUsers = allUsers.OrderByDescending(u => u.VenuesRefusing.Count());

            // We will update this as needed on each iteration
            var usersToRemove = 0;
            var result = new VenueFilterResult();

            do
            {
                var users = new LinkedList<User>(allUsers);
                var unhappyUsers = new LinkedList<User>();

                // Remove picky users.  This will happen if we couldn't get a consensus in previous iterations
                for (var i = 0; i < usersToRemove; i++)
                {
                    unhappyUsers.AddLast(users.First.Value);
                    users.RemoveFirst();
                }

                // Now we determine the actual venues 
                // First of all, add the venues that the users actually want to go to (as long as they are in the filtered list)
                foreach (var u in users)
                {
                    possibleVenues.AddRange(u.VenuesPreferring.Where(vc => filteredVenueIds.Contains(vc.VenueId)));
                }

                // Second, remove all of the venues that the users don't want to go to
                var impossibleVenueIds = new HashSet<Int32>(users.SelectMany(u => u.VenuesRefusing).Select(v => v.VenueId));
                possibleVenues = possibleVenues.Where(pv => !impossibleVenueIds.Contains(pv.VenueId)).ToList();

                // Third, let's add one entry for every venue that is in neither the desired nor undesired lists 
                /*              var possibleVenueIds = new HashSet<Int32>(possibleVenues.Select(pv => pv.VenueId));
                                possibleVenues.AddRange(filteredVenues.Where(fv => !possibleVenueIds.Contains(fv.VenueId) && !impossibleVenueIds.Contains(fv.VenueId)));
                */
                usersToRemove++;

                result = new VenueFilterResult()
                {
                    Venues = possibleVenues,
                    HappyUsers = users,
                    UnhappyUsers = unhappyUsers
                };

            } while (!possibleVenues.Any() && usersToRemove < allUsers.Count());

            return result;
        }

        internal class VenueFilterResult
        {
            public IEnumerable<Venue> Venues { get; set; }

            public IEnumerable<User> HappyUsers { get; set; }

            public IEnumerable<User> UnhappyUsers { get; set; }

            public Int32 PercentHappy
            {
                get
                {
                    if (this.HappyUsers == null || this.UnhappyUsers == null)
                    {
                        return 0;
                    }

                    return (int)((double)this.HappyUsers.Count() / (double)(this.HappyUsers.Count() + UnhappyUsers.Count()) * 100);
                }
            }
        }
    }
}