﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace LunchCommand.Controllers
{
    [Authorize]
    public class UsersController : BaseAuthenticatedController
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
        }

        public ActionResult Index()
        {
            return this.View(this.LunchUser.Team.Users.OrderByDescending(u => u.LunchesDecided.Count(l => l.Decision)).ToArray());
        }
    }
}
