﻿using System.Linq;
using System.Net;
using System.Web.Mvc;
using LunchCommand.Core.Models;
using LunchCommand.ViewModels;
using LunchCommand.Core.Models.Data;

namespace LunchCommand.Controllers
{
    [Authorize]
    public class VenuesController : BaseAuthenticatedController
    {
        private readonly IGenreRepository genreRepository;
        private readonly IVenueRepository venueRepositiory;

        public VenuesController(IGenreRepository genreRepository, IVenueRepository venueRepositiory)
        {
            this.genreRepository = genreRepository;
            this.venueRepositiory = venueRepositiory;
        }

        // GET: Venues
        public ActionResult Index()
        {
            var venues = this.LunchUser.Team.ActiveVenues;
            return View(venues.ToList());
        }

        // GET: Venues/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id != null)
            {
                var venue = this.venueRepositiory.FindById(id.GetValueOrDefault());
                if (venue == null)
                {
                    return HttpNotFound();
                }

                if (venue.SharesTeamWith(this.LunchUser))
                {
                    ViewBag.GenreID = new SelectList(this.genreRepository.FindAll(), "GenreID", "Name", venue.GenreID);
                    return View(new VenueViewModel(venue));
                }
            }

            ViewBag.GenreID = new SelectList(this.genreRepository.FindAll(), "GenreID", "Name", null);
            return this.View(new VenueViewModel(new Venue()) { VenueId = -1 });
        }

        // POST: Venues/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(VenueViewModel venueModel)
        {
            if (ModelState.IsValid)
            {
                if (venueModel.VenueId >= 0)
                {
                    var venue = this.venueRepositiory.FindById(venueModel.VenueId);
                    if (venue.SharesTeamWith(this.LunchUser))
                    {
                        venueModel.UpdateModel(venue);
                        this.venueRepositiory.Save(venue);
                    }
                }
                else
                {
                    var venue = new Venue();
                    venue.Team = this.LunchUser.Team;
                    venue.CreatedBy = this.LunchUser;
                    venueModel.UpdateModel(venue);
                    this.venueRepositiory.Save(venue);
                }

                return RedirectToAction("Index");
            }
            ViewBag.GenreID = new SelectList(this.genreRepository.FindAll(), "GenreID", "Name", venueModel.GenreID);
            return View(venueModel);
        }

        // GET: Venues/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Venue venue = this.venueRepositiory.FindById(id.GetValueOrDefault());
            if (venue == null)
            {
                return HttpNotFound();
            }
            return View(venue);
        }

        // POST: Venues/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Venue venue = this.venueRepositiory.FindById(id);

            if (venue.SharesTeamWith(this.LunchUser))
            {
                venue.Deleted = true;
                this.venueRepositiory.Save(venue);
            }

            return RedirectToAction("Index");
        }
    }
}
