﻿using System.Web.Mvc;
using LunchCommand.Data;
using LunchCommand.ViewModels;

namespace LunchCommand.Controllers
{
    [Authorize]
    public class HomeController : BaseAuthenticatedController
    {
        private ILunchService LunchService { get; set; }

        public HomeController(ILunchService lunchService, LunchCommandUserManager userManager)
        {
            this.LunchService = lunchService;
        }

        const string ViewModelKey = "temporary_view_model";
        
        [HttpGet]
        public ActionResult Index()
        {
            var viewModel = this.Session[ViewModelKey] as LunchCommandViewModel;
            if (viewModel != null)
            {
                this.Session.Remove(ViewModelKey);
            }
            else
            {
                viewModel = this.LunchService.GenerateViewModel(this.LunchUser);
            }

            return View(viewModel);
        }

        [HttpGet]
        public ActionResult Algorithm()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index([Bind(Exclude = "User")]LunchCommandViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                viewModel.User = this.LunchUser;
                this.LunchService.RehydrateAndProcessViewModel(ref viewModel);
            }

            this.Session[ViewModelKey] = viewModel;

            return this.RedirectToAction("Index");
        }
    }
}