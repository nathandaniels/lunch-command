﻿using LunchCommand.Core.Models;
using LunchCommand.Core.Models.Logging;
using LunchCommand.Data;
using Microsoft.AspNet.Identity;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace LunchCommand.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        protected ILogger Logger { get; private set; }

        protected User LunchUser { get; private set; }

        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);

            var container = HttpContext.Current.ApplicationInstance.AsLcAppication().Container;
            var userManager = container.GetInstance<LunchCommandUserManager>();

            this.Logger = container.GetInstance<ILogger>();

            this.LunchUser = userManager.FindById(User.Identity.GetUserId<int>());
        }
    }
}