﻿using LunchCommand.Core.Services;
using LunchCommand.Services;
using LunchCommand.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LunchCommand.Controllers
{
    public class InvitationsController : BaseAuthenticatedController
    {
        private readonly IInvitationService invitationService;

        private readonly IReCaptchaService recaptchaService;

        public InvitationsController(IInvitationService inviteService, IReCaptchaService recaptchaService)
        {
            this.invitationService = inviteService;
            this.recaptchaService = recaptchaService;
        }

        // GET: Invitations
        public ActionResult Send()
        {
            return View(new SendInvitationViewModel());
        }

        // POST: Invitations
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Send(SendInvitationViewModel viewModel)
        {
            if (!viewModel.ValidAddresses.Any() || viewModel.EmailAddresses.Count > SendInvitationViewModel.MaxAddressCount)
            {
                this.ModelState.AddModelError(String.Empty, "At least one valid email address must be provided");
                return this.View(viewModel);
            }
            else if (!this.recaptchaService.ValidateReCaptcha(this.Request))
            {
                this.ModelState.AddModelError("recaptcha", "Could not validate that you are, in fact, not a robot.  Please try again.");
                return this.View(viewModel);
            }
            
            this.invitationService.SendInvites(viewModel.ValidAddresses, this.LunchUser, @Url.Action("Register", "Account", null, this.Request.Url.Scheme));
                        
            return RedirectToAction("Index", "Home");
        }
    }
}