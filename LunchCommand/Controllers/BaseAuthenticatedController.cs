﻿using LunchCommand.Core.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Web.Mvc;

namespace LunchCommand.Controllers
{
    public abstract class BaseAuthenticatedController : BaseController
    {
        protected User LunchUser { get; set; }

        protected Boolean UserIsTeamAdmin { get; set; }

        private Boolean redirectIfNotLoggedIn;

        protected BaseAuthenticatedController(Boolean redirectIfNotLoggedIn = true)
        {
            this.redirectIfNotLoggedIn = redirectIfNotLoggedIn;
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);

            try
            {
                this.LunchUser = this.UserManager.FindById(User.Identity.GetUserId<int>());
                if (this.LunchUser == null)
                {
                    if (this.redirectIfNotLoggedIn)
                    {
                        filterContext.HttpContext.Response.Redirect(Url.Action("Login", "Account"));
                        ViewBag.Authenticated = false;
                        ViewBag.NotificationListenerEnabled = false;
                    }

                    return;
                }
                else
                {
                    ViewBag.UserFirstName = this.LunchUser.FirstName;
                    ViewBag.Authenticated = true;
                    ViewBag.NotificationListenerEnabled = this.LunchUser.NotificationSettings?.NotificationsEnabled;
                    ViewBag.TeamName = this.LunchUser.Team.Name;
                }
            }
            catch (Exception)
            {
                if (this.redirectIfNotLoggedIn)
                {
                    filterContext.HttpContext.Response.Redirect(Url.Action("Login", "Account"));
                }

                return;
            }

            this.ViewBag.LunchUser = LunchUser;
            this.UserIsTeamAdmin = this.ViewBag.UserIsTeamAdmin = this.LunchUser.Team.UserIsAdmin(this.LunchUser);
        }
    }
}