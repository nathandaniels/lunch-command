﻿using System.Reflection;
using System.Web.Mvc;
using System.Web.Routing;

using LunchCommand.Data;
using LunchCommand.Core.Models.Logging;

namespace LunchCommand.Controllers
{
    public abstract class BaseController : Controller
    {
        protected ILogger Logger { get; set; }
        
        protected LunchCommandUserManager UserManager { get; set; }
        
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            var container = this.HttpContext.ApplicationInstance.AsLcAppication().Container;
            this.UserManager = container.GetInstance<LunchCommandUserManager>();
            this.Logger = container.GetInstance<ILogger>();
            string assemblyVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            ViewBag.Authenticated = false;
            ViewBag.Version = assemblyVersion;
        }
    }
}