﻿using LunchCommand.Core.Models;
using LunchCommand.Core.Services;
using LunchCommand.ViewModels;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LunchCommand.Controllers
{
    public class PropositionController : BaseApiController
    {
        private readonly IPropositionService propService;
        
        public PropositionController(IPropositionService propService)
        {
            this.propService = propService;
        }

        // GET api/<controller>
        public HttpResponseMessage Get()
        {
            Lunch lunch;
            var props = this.propService.GetPropositions(this.LunchUser.Team, out lunch).Select(p => new PropositionViewModel()
            {
                Proposer = p.ProposedBy.FullName,
                Followers = p.Followers.Select(f => f.User.FullName),
                IsUserGoing = p.Followers.Any(f => f.UserId == this.LunchUser.Id) || p.ProposerId == this.LunchUser.Id,
                PropositionId = p.PropositionId,
                Venue = p.Venue.Name
            });

            var users = lunch.UsersGoing.Select(u=>u.FullName);
            var groupedUsers = props.SelectMany(p => p.Followers.Concat(new[] { p.Proposer }));

            return this.Request.CreateResponse(
                new PropositionListViewModel()
                {
                    Propositions = props,
                    RemainingVenuesToPick = this.propService.GetVenuesNotBeingProposed(this.LunchUser).Select(v => new VenueViewModel(v)),
                    UndecidedUsers = users.Where(u => !groupedUsers.Any(ug => ug.Equals(u))).ToList(),
                    LunchHasStarted = lunch.HasStarted
                });
        }

        // PUT api/<controller>/5
        public HttpResponseMessage Put([FromBody]CreatePropositionViewModel cpvm)
        {
            this.propService.CreateProposition(this.LunchUser, cpvm.VenueId);
            return this.Request.CreateResponse(HttpStatusCode.OK);
        }
    }

    public class PropositionFollowingController : BaseApiController
    {
        private readonly IPropositionService propService;

        public PropositionFollowingController(IPropositionService propService)
        {
            this.propService = propService;
        }
        
        public HttpResponseMessage Put(PropositionViewModel prop)
        {
            this.propService.AddUserToFollowing(this.LunchUser, prop.PropositionId);
            return this.Request.CreateResponse(HttpStatusCode.OK);
        }

        public HttpResponseMessage Delete(PropositionViewModel prop)
        {
            this.propService.RemoveUserFromFollowing(this.LunchUser, prop.PropositionId);
            return this.Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}