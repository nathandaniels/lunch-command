﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using LunchCommand.Core.Models.Data;
using LunchCommand.Core.Models.Logging;
using LunchCommand.ViewModels;

namespace LunchCommand.Controllers
{
    [Authorize]
    public class TeamMemberController : BaseApiController
    {
        private readonly ITeamRepository teamRepo;
        private readonly IUserRepository userRepo;

        public TeamMemberController(ITeamRepository teamRepo, IUserRepository userRepo)
        {
            this.teamRepo = teamRepo;
            this.userRepo = userRepo;
        }

        [ResponseType(typeof(TeamMembersViewModel))]
        public HttpResponseMessage Get()
        {
            if (!this.LunchUser.Team.UserIsAdmin(this.LunchUser))
            {
                return new HttpResponseMessage(HttpStatusCode.Forbidden);
            }

            return this.Request.CreateResponse(

                new TeamMembersViewModel()
                {
                    Members = this.LunchUser.Team.Users.Where(u => u.Id != this.LunchUser.Id).Select(u =>
                          new TeamMemberViewModel()
                          {
                              UserId = u.Id,
                              Name = $"{u.FullName} ({u.UserName})",
                              IsAdmin = this.LunchUser.Team.UserIsAdmin(u)
                          }
                ).ToList()
                });
        }

        public HttpResponseMessage Put(TeamMembersViewModel viewModel)
        {
            var team = teamRepo.FindById(this.LunchUser.TeamId.Value);

            var toBeAdmin = viewModel.Members.Where(m => m.IsAdmin).Select(u => userRepo.FindById(u.UserId)).Where(u => !team.UserIsAdmin(u)).ToArray();
            var toBeDeAdmin = viewModel.Members.Where(m => !m.IsAdmin).Select(u => userRepo.FindById(u.UserId)).Where(u => team.UserIsAdmin(u)).ToArray();

            foreach (var member in toBeAdmin)
            {
                this.Logger.Log(LogEntryType.Info, $"User {this.LunchUser.Id} is marking user {member.Id} as an admin of team {team.TeamId}");
                team.AddAdmin(member);
            }

            foreach (var member in toBeDeAdmin)
            {
                this.Logger.Log(LogEntryType.Info, $"User {this.LunchUser.Id} is removing user {member.Id} as an admin of team {team.TeamId}");
                team.RemoveAdmin(member);
            }

            foreach(var doomedId in team.Users.Select(u => u.Id).Except(viewModel.Members.Select(m => m.UserId).Concat(new[] { this.LunchUser.Id })).ToArray())
            {
                this.Logger.Log(LogEntryType.Info, $"User {this.LunchUser.Id} is booting user {doomedId} from team {team.TeamId}");
                var user= team.RemoveUser(doomedId);
                userRepo.Update(user);
            }

            teamRepo.Save(team);

            return new HttpResponseMessage();
        }
    }
}
