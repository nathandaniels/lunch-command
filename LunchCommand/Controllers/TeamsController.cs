﻿using System;
using System.Net;
using System.Web.Mvc;
using LunchCommand.Core.Models;
using LunchCommand.ViewModels;
using LunchCommand.Services;
using LunchCommand.Core.Models.Data;

namespace LunchCommand.Controllers
{
    public class TeamsController : BaseAuthenticatedController
    {
        private readonly ITeamRepository teamRepo;
        private readonly IInviteRepository inviteRepo;
        private readonly IReCaptchaService recaptchaService;

        public TeamsController(ITeamRepository teamRepo, IInviteRepository inviteRepo, IReCaptchaService recaptchaService) : base(false)
        {
            this.teamRepo = teamRepo;
            this.inviteRepo = inviteRepo;
            this.recaptchaService = recaptchaService;
        }

        // GET: Teams/Create
        [AllowAnonymous]
        public ActionResult Create()
        {
            return View(new TeamViewModel());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public ActionResult Create(TeamViewModel teamModel)
        {
            if (ModelState.IsValid)
            {
                var team = this.teamRepo.FindByName(teamModel.Name);

                if (!this.recaptchaService.ValidateReCaptcha(this.Request))
                {
                    this.ModelState.AddModelError(String.Empty, "Could not validate reCAPTCHA.  Please try again.");
                }
                else if (team != null && team.TeamId != this.LunchUser.TeamId)
                {
                    this.ModelState.AddModelError(nameof(TeamViewModel.Name), "A team with that name already exists");
                }
                else if (teamModel.ParsedTime == null || teamModel.ParsedTime.Value.TotalHours >= 24 || teamModel.ParsedTime.Value.TotalHours <= 0)
                {
                    this.ModelState.AddModelError(nameof(TeamViewModel.LunchStartTimeUtc), "Please enter a start time in format \"HH:MM AM/PM\" or \"HH:MM\"");
                    return View(teamModel);
                }

                else
                {
                    team = new Team();
                    teamModel.UpdateTeam(team);
                    team.AdminRole = new Role() { Name = String.Format(team.Name + "Team Administrator") };
                    this.teamRepo.Save(team);

                    var invite = inviteRepo.Generate(team);

                    return RedirectToAction("Created", new { inviteCode = invite.InviteCode });
                }
            }

            return View(teamModel);
        }

        [AllowAnonymous]
        public ActionResult Created(String inviteCode)
        {
            this.ViewBag.InviteCode = inviteCode;
            return this.View();
        }

        [Authorize]
        // GET: Teams/Edit/5
        public ActionResult Edit()
        {
            if (this.UserIsTeamAdmin)
            {
                return View(new TeamViewModel(this.LunchUser.Team));
            }
            else
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit(TeamViewModel team)
        {
            if (!this.UserIsTeamAdmin)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            if (ModelState.IsValid && team.TeamId == this.LunchUser.TeamId && this.UserIsTeamAdmin)
            {
                var oldTeam = this.teamRepo.FindByName(team.Name);
                if (oldTeam != null && oldTeam.TeamId != team.TeamId)
                {
                    this.ModelState.AddModelError("name", "A team with that name already exists");
                    return View(team);
                }
                if (team.ParsedTime == null || team.ParsedTime.Value.TotalHours >= 24 || team.ParsedTime.Value.TotalHours <= 0)
                {
                    this.ModelState.AddModelError("LunchStartTime", "Please enter a start time in format \"HH:MM AM/PM\" or \"HH:MM\"");
                    return View(team);
                }

                oldTeam = this.teamRepo.FindById(team.TeamId);
                team.UpdateTeam(oldTeam);

                this.teamRepo.Save(oldTeam);
                return RedirectToAction("Index", "Home");
            }
            return View(team);
        }

        public ActionResult Members()
        {
            if (!this.UserIsTeamAdmin)
            {
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
            }

            return View("Members");
        }
    }
}
