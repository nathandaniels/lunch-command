﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using LunchCommand.Core.Services.Notifications;
using LunchCommand.ViewModels;

namespace LunchCommand.Controllers
{
    public class NotificationsController : BaseApiController
    {
        private IBrowserNotificationsService Service { get; }

        public NotificationsController(IBrowserNotificationsService notificationsService)
        {
            this.Service = notificationsService;
        }

        public HttpResponseMessage Get()
        {
            var notifications = this.Service.GetBrowserNotifications(this.LunchUser);
            return this.Request.CreateResponse<IEnumerable<NotificationViewModel>>(notifications.Select(n=>new NotificationViewModel(n)));
        }
    }
}