﻿using System.Linq;
using System.Web.Mvc;

namespace LunchCommand.Controllers
{
    [Authorize]
    public class LunchesController : BaseAuthenticatedController
    {
        // GET: Lunches
        public ActionResult Index()
        {
            return View(this.LunchUser.Team.Lunches.OrderByDescending(l=>l.LunchTimeUtc).ToArray());
        }
    }
}