﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using LunchCommand.Data;
using LunchCommand.Core.Models;
using LunchCommand.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Linq;
using LunchCommand.Services;
using LunchCommand.Core.Services.Mail;
using LunchCommand.Core.Models.Settings;
using LunchCommand.Core.Models.Data;

namespace LunchCommand.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
        private readonly IMailSettings mailSettings;
        private readonly IInviteRepository inviteRepository;
        private readonly ITeamRepository teamRepository;
        private readonly IReCaptchaService recaptchaService;
        private readonly IUserRepository userRepository;
        private readonly IMailService mailService;

        public AccountController(
            LunchCommandUserManager userManager,
            IMailSettings mailSettings,
            IInviteRepository inviteRepository,
            ITeamRepository teamRepository,
            IReCaptchaService recaptchaService,
            IUserRepository userRepository,
            IMailService mailService)
        {
            this.UserManager = userManager;
            this.mailService = mailService;
            this.mailSettings = mailSettings;
            this.teamRepository = teamRepository;
            this.userRepository = userRepository;
            this.recaptchaService = recaptchaService;
            this.inviteRepository = inviteRepository;
        }

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindAsync(model.UserName, model.Password);
                if (user != null)
                {
                    if (user.Team == null)
                    {
                        ModelState.AddModelError("", "Your account has been removed from the team by a team administrator.");
                    }
                    else
                    {
                        await SignInAsync(user, model.RememberMe);
                        return RedirectToLocal(returnUrl);
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Invalid username or password.");
                }
            }

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Register(String inviteCode, String emailAddress)
        {
            return View(new RegisterViewModel() { InviteCode = inviteCode, Email = emailAddress, ConfirmEmail = emailAddress });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var invite = this.inviteRepository.FindByCode(model.InviteCode);
                if (invite == null)
                {
                    this.ModelState.AddModelError("InviteCode", "Invite code was not found.");
                    return View(model);
                }

                var user = new User
                {
                    UserName = model.UserName,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    TeamId = invite.TeamId,
                    InvitedBy = invite.CreatingUser,
                    Email = model.Email
                };

                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await SignInAsync(user, isPersistent: false);

                    // The first user in a team becomes the first admin as well
                    if (invite.CreatingUser == null && invite.Team.Users.All(u => u.Id == user.Id))
                    {
                        invite.Team.AddAdmin(user);
                        this.teamRepository.Save(invite.Team);
                    }

                    this.inviteRepository.Delete(invite);

                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    AddErrors(result);
                }
            }

            return View(model);
        }

        public ActionResult Password(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            ViewBag.HasLocalPassword = HasPassword();
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Password(PasswordChangeViewModel model)
        {
            bool hasPassword = HasPassword();
            ViewBag.HasLocalPassword = hasPassword;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasPassword)
            {
                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId<int>(), model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }
            else
            {
                // User does not have a password so remove any validation errors caused by a missing OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId<int>(), model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }

            return View(model);
        }

        public ActionResult Manage()
        {
            ViewBag.ReturnUrl = Url.Action("Manage");
            var model = new ManageAccountViewModel();
            var user = this.userRepository.FindById(this.User.Identity.GetUserId<Int32>());

            model.FirstName = user.FirstName;
            model.LastName = user.LastName;
            model.Email = user.Email;
            model.ConfirmEmail = user.Email;
            model.Username = user.UserName;

            model.NotificationSettings.Medium = user.NotificationSettings.Medium;
            model.NotificationSettings.MinutesToNotifyOfIndecision = user.NotificationSettings.MinutesToNotifyOfIndecision;
            model.NotificationSettings.NotifyLunchStarted = user.NotificationSettings.NotifyLunchStarted;
            model.NotificationSettings.NotifyLunchStartedEvenWhenNotDecided = user.NotificationSettings.NotifyLunchStartedEvenWhenNotDecided;
            model.NotificationSettings.NotifyNotDecided = user.NotificationSettings.NotifyNotDecided;
            model.NotificationSettings.NotifyVenueProposed = user.NotificationSettings.NotifyVenueProposed;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(ManageAccountViewModel model)
        {
            bool hasPassword = HasPassword();
            ViewBag.HasLocalPassword = hasPassword;
            ViewBag.ReturnUrl = Url.Action("Manage");

            if (ModelState.IsValid)
            {
                var user = this.userRepository.FindById(this.User.Identity.GetUserId<Int32>());
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;
                user.Email = model.Email;

                user.NotificationSettings.Medium = model.NotificationSettings.Medium;
                user.NotificationSettings.MinutesToNotifyOfIndecision = model.NotificationSettings.MinutesToNotifyOfIndecision;
                user.NotificationSettings.NotifyLunchStarted = model.NotificationSettings.NotifyLunchStarted;
                user.NotificationSettings.NotifyLunchStartedEvenWhenNotDecided = model.NotificationSettings.NotifyLunchStartedEvenWhenNotDecided;
                user.NotificationSettings.NotifyNotDecided = model.NotificationSettings.NotifyNotDecided;
                user.NotificationSettings.NotifyVenueProposed = model.NotificationSettings.NotifyVenueProposed;

                this.userRepository.Update(user);

                return this.RedirectToAction("Index", "Home");
            }
            else
            {
                return View(model);
            }
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult ResetPassword()
        {
            return this.View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(PasswordResetViewModel model)
        {
            if (!this.recaptchaService.ValidateReCaptcha(this.Request))
            {
                this.ModelState.AddModelError("recaptcha", "Could not validate reCAPTCHA.  Please try again.");
            }
            else
            {
                var foundUser = this.UserManager.FindByEmail(model.Email);

                if (foundUser == null || !foundUser.UserName.Equals(model.Username, StringComparison.OrdinalIgnoreCase))
                {
                    this.ModelState.AddModelError("email", "Could not find an account with that email address and username.");
                }
                else
                {
                    var token = this.UserManager.GeneratePasswordResetToken(foundUser.Id);
                    var newPassword = Guid.NewGuid().ToString("N");

                    var body = $"A new password has been generated for this account.  The new password is as follows:\r\n\r\n{newPassword}\r\n\r\nBe sure to change this password once you log in.";

                    var mailSend = mailService.SendMessageAsync("Lunch Command", this.mailSettings.PasswordResetFromAddress, foundUser.Email, "Temporary Password", body);
                    var pwdChange = this.UserManager.ResetPassword(foundUser.Id, token, newPassword);
                    mailSend.Wait();

                    return this.RedirectToAction("Login");
                }
            }

            return this.View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [ChildActionOnly]
        public ActionResult RemoveAccountList()
        {
            var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId<int>());
            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
            return PartialView("_RemoveAccountPartial", linkedAccounts);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(User user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId<int>());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        #endregion
    }
}