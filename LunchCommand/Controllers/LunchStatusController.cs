﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using LunchCommand.ViewModels;

namespace LunchCommand.Controllers
{
    [Authorize]
    public class LunchStatusController : BaseApiController
    {
        private ILunchService LunchService { get; }

        public LunchStatusController(ILunchService lunchService)
        {
            this.LunchService = lunchService;
        }

        [ResponseType(typeof(LunchStateViewModel))]
        public HttpResponseMessage Get()
        {
            return this.Request.CreateResponse<LunchStateViewModel>(
                this.LunchService.GetStatus(this.LunchUser));
        }

        [ResponseType(typeof(LunchStateViewModel))]
        public HttpResponseMessage Put(UserStatusViewModel status)
        {
            this.LunchService.ProcessUserDecision(this.LunchUser, status.Decision);

            return this.Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
