﻿using LunchCommand.Core.Models.Settings;
using System;
using System.Collections.Specialized;
using System.Security;

namespace LunchCommand.Models
{
    public class WebConfigSettings : ISettings
    {
        /// <summary>
        /// The key used for bing maps
        /// </summary>
        public String BingMapsKey { get; internal set; }

        /// <summary>
        /// Exactly what it says so verbosely
        /// </summary>
        public Int32 NumberOfDaysBeforeVenuesAreConsideredEquallyOld { get; internal set; }

        /// <summary>
        /// The maximum ratio of outliers (venues with no likes nor disliked) to all venues allowed.  This limits 
        /// the likelyhood of outiers being picked when there are small groups
        /// </summary>
        public Double MaximumLikelyhoodOfOutlierBeingPicked { get; internal set; }

        /// <summary>
        /// The URL for the reCAPTCHA verification service from google
        /// </summary>
        public Uri GoogleReCaptchaUrl { get; internal set; }

        /// <summary>
        /// The query parameter that the recaptcha value is stored in 
        /// </summary>
        public String GoogleReCaptchaQueryParamName { get; internal set; }

        /// <summary>
        /// The secret key for the reCAPTCHA verification service from google
        /// </summary>
        public String GoogleReCaptchaSecretKey { get; internal set; }

        /// <summary>
        /// Whether or not reCAPTCHA is used in this application
        /// </summary>
        public Boolean EnableReCaptcha { get; internal set; }

        /// <summary>
        /// Gets the settings for the mail service
        /// </summary>
        public IMailSettings MailSettings { get; private set; }

        public static ISettings CreateFrom(NameValueCollection settings)
        {
            var ret = new WebConfigSettings();

            ret.BingMapsKey = settings["BingMapsKey"];

            ret.EnableReCaptcha = Boolean.Parse(settings["EnableReCaptcha"]);
            try
            {
                ret.GoogleReCaptchaUrl = new Uri(settings["GoogleReCaptchaUrl"]);
                ret.GoogleReCaptchaSecretKey = settings["GoogleReCaptchaSecretKey"];
                ret.GoogleReCaptchaQueryParamName = settings["GoogleReCaptchaQueryParamName"];
            }
            catch
            {
                if (ret.EnableReCaptcha)
                {
                    throw;
                }
            }

            var mailSettings = new MailSettingsClass();
            ret.MailSettings = mailSettings;
            mailSettings.InvitationFromAddress = settings["InvitationFromAddress"];
            mailSettings.PasswordResetFromAddress = settings["PasswordResetFromAddress"];
            mailSettings.SmtpUserName = settings["SmtpUserName"];
            mailSettings.SmtpPassword = settings["SmtpPassword"];
            mailSettings.SmtpServer = settings["SmtpServer"];
            mailSettings.SmtpPort = UInt16.Parse(settings["SmtpPort"]);

            int val;
            if (Int32.TryParse(settings["NumberOfDaysBeforeVenuesAreConsideredEquallyOld"], out val))
            {
                ret.NumberOfDaysBeforeVenuesAreConsideredEquallyOld = val;
            }
            else
            {
                ret.NumberOfDaysBeforeVenuesAreConsideredEquallyOld = 14;
            }

            if (Int32.TryParse(settings["MaximumLikelyhoodOfOutlierBeingPicked"], out val))
            {
                ret.MaximumLikelyhoodOfOutlierBeingPicked = val;
            }
            else
            {
                ret.MaximumLikelyhoodOfOutlierBeingPicked = 0.1;
            }
            
            return ret;
        }

        private class MailSettingsClass : IMailSettings
        {
            public string InvitationFromAddress { get; set; }

            public String PasswordResetFromAddress { get; set; }

            public String SmtpPassword { get; set; }

            public ushort SmtpPort { get; set; }

            public string SmtpServer { get; set; }

            public string SmtpUserName { get; set; }
        }
    }
}