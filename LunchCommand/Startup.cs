﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(LunchCommand.Startup))]
namespace LunchCommand
{   
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}