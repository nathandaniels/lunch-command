﻿module LunchCommand {

    export class TeamMemberService extends BaseService {

        public GetTeamMembers(): JQueryXHR {
            return super.ajax("TeamMember", "GET");
        }

        public SaveMembers(model: ITeamMembers): JQueryXHR{
            return super.ajax("TeamMember", "PUT", model);
        }
    }
} 
