var LunchCommand;
(function (LunchCommand) {
    ;
    class GenreDecisionGroup {
        constructor() {
            this.radioButtons = [];
        }
        get checked() {
            if (this.checkbox.get(0).indeterminate) {
                return null;
            }
            return this.checkbox.prop("checked");
        }
        set checked(val) {
            if (val !== null) {
                this.checkbox.prop("checked", val);
                this.checkbox.get(0).indeterminate = false;
            }
            else {
                this.checkbox.removeProp("checked");
                this.checkbox.get(0).indeterminate = true;
            }
        }
    }
    class GenreGroup {
        constructor(name) {
            this.name = name;
            this.decisions = {};
        }
        getDecisionGroup(dec) {
            this.decisions[dec] = this.decisions[dec] || new GenreDecisionGroup();
            return this.decisions[dec];
        }
    }
    class RadioButton {
        constructor(element, group) {
            this.el = element;
            this.group = group;
        }
        get checked() {
            return this.el.prop("checked");
        }
        set checked(val) {
            this.el.prop("checked", val);
        }
    }
    class HomeController {
        constructor(genreTable, venueTable) {
            this.genreTable = genreTable;
            this.venueTable = venueTable;
            this.genres = {};
        }
        initialize() {
            this.initializeDataStructures();
            this.initializeStates();
        }
        initializeDataStructures() {
            this.genreTable.find(".select-all").each((indy, element) => {
                var jqe = $(element);
                var groupName = jqe.data("group");
                var genreGroup = this.genres[groupName] = this.genres[groupName] || new GenreGroup(groupName);
                var decision = jqe.data("decision");
                var dg = genreGroup.getDecisionGroup(decision);
                dg.checkbox = jqe;
                dg.genreGroup = genreGroup;
                jqe.click(evt => {
                    this.handleCheckboxClick(dg);
                    evt.preventDefault();
                    return false;
                });
                jqe.change(evt => {
                    evt.preventDefault();
                    return false;
                });
            });
            this.venueTable.find(".radio-col > input[type=radio]").each((indy, element) => {
                var jqe = $(element);
                var groupName = jqe.data("group");
                var genreGroup = this.genres[groupName];
                var decision = jqe.data("decision");
                var dg = genreGroup.getDecisionGroup(decision);
                var rb = new RadioButton(jqe, dg);
                dg.radioButtons.push(rb);
                rb.el.change(evt => {
                    this.updateCheckboxState(dg.genreGroup);
                });
            });
            jQuery("body").on("shown.bs.popover", e => {
                window.setTimeout(() => {
                    if (this.lastDgToGetPopover) {
                        var dg = this.lastDgToGetPopover;
                        $($("[data-toggle=popover]").toArray().filter(e => e != dg.checkbox.get(0))).popover("destroy");
                        this.lastDgToGetPopover = null;
                        var popover = $('body .lcpo[data-bound=false]').first();
                        popover.attr("data-bound", "true");
                        popover.find("button.yes").click(e => {
                            dg.checkbox.popover("destroy");
                            dg.checked = true;
                            this.checkAllRadioButtons(dg);
                        });
                        popover.find("button.no").click(e => {
                            dg.checkbox.popover("destroy");
                        });
                    }
                }, 1);
            });
        }
        initializeStates() {
            for (var i in this.genres) {
                this.updateCheckboxState(this.genres[i]);
            }
        }
        updateCheckboxState(genreGroup) {
            for (var i in genreGroup.decisions) {
                var decisionGroup = genreGroup.decisions[i];
                if (decisionGroup.radioButtons.every(rb => rb.checked)) {
                    decisionGroup.checked = true;
                }
                else if (decisionGroup.radioButtons.every(rb => !rb.checked)) {
                    decisionGroup.checked = false;
                }
                else {
                    decisionGroup.checked = null;
                }
            }
        }
        checkAllRadioButtons(decisionGroup) {
            var toCheck = decisionGroup.checked;
            for (var i in decisionGroup.radioButtons) {
                var rb = decisionGroup.radioButtons[i];
                rb.checked = toCheck;
            }
            this.updateCheckboxState(decisionGroup.genreGroup);
        }
        handleCheckboxClick(decisionGroup) {
            window.setTimeout(() => {
                if (!decisionGroup.checked) {
                    decisionGroup.checkbox.popover({
                        html: true
                    });
                    decisionGroup.checkbox.popover("show");
                    this.lastDgToGetPopover = decisionGroup;
                }
            }, 10);
        }
    }
    LunchCommand.HomeController = HomeController;
})(LunchCommand || (LunchCommand = {}));
