﻿module LunchCommand {

    export interface IVenue {
        VenueId: number;
        Name: string;
        Description: string;
        Address: string;
        GenreID: number;
        IsQuick: boolean;
        Genre: IGenre;
        DaysSinceLastVisit: number;
        LastVisitedString: number;
    }
}