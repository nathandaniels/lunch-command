var LunchCommand;
(function (LunchCommand) {
    (function (LunchStatus) {
        LunchStatus[LunchStatus["StartedWithConsensus"] = 0] = "StartedWithConsensus";
        LunchStatus[LunchStatus["StartedWithoutConsensus"] = 1] = "StartedWithoutConsensus";
        LunchStatus[LunchStatus["NotStarted"] = 2] = "NotStarted";
        LunchStatus[LunchStatus["DidNotHappen"] = 3] = "DidNotHappen";
        LunchStatus[LunchStatus["UserHasDecidedManually"] = 4] = "UserHasDecidedManually";
    })(LunchCommand.LunchStatus || (LunchCommand.LunchStatus = {}));
    var LunchStatus = LunchCommand.LunchStatus;
})(LunchCommand || (LunchCommand = {}));
