﻿module LunchCommand {

    export class LunchStatusController {

        private lunchStatusService: LunchStatusService;
        private propService: PropositionService;
        private area: JQuery;
        private afterDecisionElements: JQuery;
        private beforeDecisionElements: JQuery;
        private lunchStatusText: JQuery;
        private mainVenueText: JQuery;
        private quickVenueText: JQuery;
        private quickVenueArea: JQuery;
        private dissentingArea: JQuery;
        private dissentingVenueText: JQuery;
        private mainConsensus: JQuery;
        private quickConsensus: JQuery;
        private dissentingConsensus: JQuery;
        private travelTimeText: JQuery;
        private timeText: JQuery;
        private propositionTemplate: JQuery;
        private propositionDropzone: JQuery;
        private ungroupedLunchers: JQuery;
        private newPropositionDiv: JQuery;
        private venuePropositionSelect: JQuery;
        private proposeButton: JQuery;

        private lastVenuesAvailableToPropose: number[] = [];

        private koBound: boolean = false;

        private windowFocusListener: WindowFocusListener = new WindowFocusListener();

        public lastState: KnockoutObservable<ILunchState> = ko.observable(null);
        public lastPropositions: KnockoutObservable<IProposition[]> = ko.observable([]);

        constructor(
            area: JQuery,
            propositionDropzone: JQuery,
            private usersGoingContainer: JQuery,
            private possibleVenuesContainer: JQuery,
            private decisionQuestionArea: JQuery) {

            this.area = area;
            this.propositionDropzone = propositionDropzone;
            this.lunchStatusService = new LunchStatusService();
            this.propService = new PropositionService();
            this.usersGoingContainer = usersGoingContainer;
            this.possibleVenuesContainer = possibleVenuesContainer;
            this.decisionQuestionArea = decisionQuestionArea;
            this.setUpTimer();
        }

        public initialize(): void {
            this.afterDecisionElements = this.area.find(".after-decision");
            this.beforeDecisionElements = this.area.find(".before-decision");
            this.lunchStatusText = this.area.find(".lunch-status");
            this.mainVenueText = this.area.find("#mainVenue");
            this.quickVenueText = this.area.find("#quickVenue");
            this.quickVenueArea = this.area.find(".quick-venue");
            this.dissentingArea = this.area.find(".dissenting-venue");
            this.dissentingVenueText = this.area.find("#dissentingVenue");
            this.mainConsensus = this.area.find("#mainConsensus");
            this.quickConsensus = this.area.find("#quickConsensus");
            this.dissentingConsensus = this.area.find("#dissentingConsensus");
            this.timeText = this.area.find("#timeUntilLunch");
            this.travelTimeText = this.area.find("#travelTime");

            this.propositionTemplate = this.propositionDropzone.find("#proposition-template");
            this.newPropositionDiv = this.propositionDropzone.find(".new-proposition");
            this.ungroupedLunchers = this.propositionDropzone.parent().find(".ungrouped-lunchers");
            this.venuePropositionSelect = this.newPropositionDiv.find(".venue-proposition-select");
            this.proposeButton = this.newPropositionDiv.find(".propose-button");

            this.venuePropositionSelect.change(e => {
                if (this.venuePropositionSelect.val() == -1) {
                    this.proposeButton.prop("disabled", true);
                }
                else {
                    this.proposeButton.prop("disabled", false);
                }
            });

            this.proposeButton.click(() => {
                this.propService.CreateProposition(this.venuePropositionSelect.val())
                    .done(() => this.getStatus());
            })

            this.getStatus();

            // When the user focuses the window, preempt any notifications on the propositions that 
            // might not have been displayed yet
            this.windowFocusListener.focused.subscribe(() => {
                if (this.windowFocusListener.focused()) {
                    for (var i in this.lastPropositions()) {
                        NotificationsListener.preemptPropositionNotification({
                            PropositionId: this.lastPropositions()[i].PropositionId
                        });
                    }
                }
            });
        }

        private getStatus(): void {
            this.lunchStatusService.GetStatus()
                .done((status: ILunchState) => this.processState(status))
                .done(() => this.bindViewModel())
                .done((status: ILunchState) => {
                    this.propService.GetPropositions()
                        .done((props: IPropositionList) => {
                            this.processPropositions(props, status.UserDecision);
                        });
                })
                .fail(failure => {
                    console.error("Failed to get lunch status");
                    console.info(failure);
                    window.setTimeout(() => { this.getStatus(); }, 60000);
                });
        }

        private processPropositions(props: IPropositionList, decision: UserDecision): void {
            this.propositionDropzone.find(".proposition").remove();

            var userHasChosenOne: boolean = false;

            // Handle propositions
            this.lastPropositions(props.Propositions);
            for (var i in props.Propositions) {
                var p = props.Propositions[i];
                props.Propositions[i];

                // Prevent notifications on this proposition if the window is currently in focus
                if (this.windowFocusListener.focused()) {
                    NotificationsListener.preemptPropositionNotification({
                        PropositionId: p.PropositionId
                    });
                }

                var newProp = jQuery(this.propositionTemplate.text());

                newProp.find(".proposed-venue").text(this.htmlDecode(p.Venue));
                newProp.find(".proposer").text(this.htmlDecode(p.Proposer));
                newProp.find(".followers").html(p.Followers.map(f => "<strong>" + this.htmlDecode(f) + "</strong>").join(", "));
                newProp.data("proposition", p);

                var pos = {
                    position: {
                        my: "center bottom",
                        at: "center top-5"
                    }
                };

                newProp.tooltip(pos);

                if (p.IsUserGoing) {
                    userHasChosenOne = true;
                    newProp.find(".proposition-header").attr("title", "Click to leave this group");
                        newProp.find(".proposition-header").click((e) => {
                            var toLeave = $(e.delegateTarget).parent(".proposition").data("proposition");
                            this.propService.LeaveProposition(toLeave)
                                .done(() => this.getStatus());
                        });
                    newProp.find(".proposition-header").removeClass("panel-info").addClass("panel-success");
                    newProp.find(".leave-or-join").text("leave");

                } else {
                    newProp.find(".proposition-header").attr("title", "Click to join " + this.htmlDecode(p.Proposer) + " at " + this.htmlDecode(p.Venue));
                        newProp.find(".proposition-header").click((e) => {
                            var toJoin = $(e.delegateTarget).parent(".proposition").data("proposition");
                            this.propService.JoinProposition(toJoin)
                                .done(() => this.getStatus());
                        });
                    newProp.find(".leave-or-join").text("join");
                }
                this.propositionDropzone.append(newProp);
            }

            if (props.Propositions.length && props.UndecidedUsers.length) {
                this.ungroupedLunchers.show();
                this.ungroupedLunchers.find(".followers").html(props.UndecidedUsers.map(f => "<strong>" + this.htmlDecode(f) + "</strong>").join(", "));

            } else {
                this.ungroupedLunchers.hide();
            }

            // Handle the new-props div
            if (userHasChosenOne || props.LunchHasStarted || decision !== UserDecision.Going) {
                this.newPropositionDiv.hide();
            }
            else {
                this.newPropositionDiv.show();
            }

            // Only update the dropdown if there were changes
            if (!this.lastVenuesAvailableToPropose.length || !this.lastVenuesAvailableToPropose.every(v => props.RemainingVenuesToPick.some(rv => rv.VenueId === v))) {
                var oldSelection = this.venuePropositionSelect.val();
                this.venuePropositionSelect.empty();
                var newOption = $("<option/>");
                newOption.text("Select a venue");
                newOption.val(-1);
                this.venuePropositionSelect.append(newOption);

                for (var i in props.RemainingVenuesToPick) {
                    var v = props.RemainingVenuesToPick[i];

                    newOption = $("<option/>");
                    newOption.text(this.htmlDecode(v.Name));
                    newOption.val(v.VenueId);
                    this.venuePropositionSelect.append(newOption);
                }
            }

            this.lastVenuesAvailableToPropose = props.RemainingVenuesToPick.map(v => v.VenueId);
        }

        private processState(status: ILunchState): void {

            this.lastState(status);

            this.lunchStatusText.text(status.StatusText);

            var timeout: number = 3600000;

            // Hide the proposition are if the user is not going

            switch (status.Status) {
                case LunchStatus.NotStarted:
                    this.beforeDecisionElements.show();
                    this.afterDecisionElements.hide();
                    timeout = 15000;

                    break;
                case LunchStatus.DidNotHappen:
                    this.beforeDecisionElements.hide();
                    this.afterDecisionElements.hide();
                    break;
                case LunchStatus.StartedWithConsensus:
                case LunchStatus.StartedWithoutConsensus:
                    this.beforeDecisionElements.hide();
                    NotificationsListener.clearAllPropositionNotificationPreemptions();

                    if (status.DecidedVenue == null) {
                        this.mainVenueText.text("No venues could be determined");
                    } else {
                        this.mainVenueText.text(status.DecidedVenue.Venue.Name);
                        this.mainConsensus.text(status.DecidedVenue.PercentageOfUsers + '%');

                        if (status.MinutesToTravel) {
                            this.travelTimeText.text("Current Travel Time: " + status.MinutesToTravel + " minutes");
                            this.travelTimeText.show();
                        } else {
                            this.travelTimeText.hide();
                        }

                    }

                    if (status.QuickVenue != null && status.QuickVenue.Venue.VenueId !== status.DecidedVenue.Venue.VenueId) {
                        this.quickVenueArea.show();
                        this.quickVenueText.text(status.QuickVenue.Venue.Name);
                        this.quickConsensus.text('(' + status.QuickVenue.PercentageOfUsers + '% Consensus)');
                    } else {
                        this.quickVenueArea.hide();
                    }

                    if (status.DissentingVenue != null) {
                        this.dissentingArea.show();
                        this.dissentingVenueText.text(status.DissentingVenue.Venue.Name);
                        this.dissentingConsensus.text('(' + status.DissentingVenue.PercentageOfUsers + '% Consensus among dissenters)');
                    } else {
                        this.dissentingArea.hide();
                    }

                    this.afterDecisionElements.show();
                    break;
                case LunchStatus.UserHasDecidedManually:
                    timeout = 15000;
                    NotificationsListener.clearAllPropositionNotificationPreemptions();
                    this.mainVenueText.text(status.DecidedVenue.Venue.Name);
                    this.beforeDecisionElements.hide();
                    this.mainConsensus.parent().hide();
                    if (status.MinutesToTravel) {
                        this.travelTimeText.text("Current Travel Time: " + status.MinutesToTravel + " minutes");
                        this.travelTimeText.show();
                    } else {
                        this.travelTimeText.hide();
                    }
                    this.dissentingArea.hide();
                    this.quickVenueArea.hide();
                    this.afterDecisionElements.show();

                    break;
            }

            window.setTimeout(() => { this.getStatus(); }, timeout);
        }

        private bindViewModel(): void {
            if (!this.koBound) {
                this.koBound = true;
                ko.applyBindings(new UsersGoingViewModel(this.lastState), this.usersGoingContainer.get(0));
                ko.applyBindings(this.lastState, this.possibleVenuesContainer.get(0));
                ko.applyBindings(new UserDecisionViewModel(this.lastState, x => { this.handleUserDecision(x); }), this.decisionQuestionArea.get(0));
            }
        }

        private handleUserDecision(decision: UserDecision) {
            this.lunchStatusService.AnnounceDecision({
                Decision: decision
            }).done(() => {
                this.getStatus();
            });
        }

        private setUpTimer() {

            var milliseconds = 0;

            this.lastState.subscribe(() => {
                if (this.lastState()) {
                    milliseconds = this.lastState().TimeRemaining;
                }
            });

            window.setInterval(() => {
                milliseconds = Math.max(0, milliseconds - 1000);

                if (milliseconds == 0) {
                    this.timeText.text("just a moment...");
                    return;
                }

                var totalSeconds = Math.floor(milliseconds / 1000);
                var totalMinutes = Math.floor(totalSeconds / 60);
                var totalHours = Math.floor(totalSeconds / 3600);

                var seconds = totalSeconds % 60;
                var minutes = totalMinutes % 60;

                this.timeText.text(totalHours + ":" + (minutes < 10 ? "0" : "") + minutes + ":" + (seconds < 10 ? "0" : "") + seconds);
            }, 1000);
        }

        private htmlDecode(value: string) {
            return $('<div/>').html(value).text();
        }
    }
} 