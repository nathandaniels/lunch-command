﻿module LunchCommand {

    interface IUserDecision {
        UserName: string;
        IsGoing: boolean;
        IsNotGoing: boolean;
        IsUndecided: boolean;
    }

    export class UsersGoingViewModel {

        public UsersDecided: KnockoutComputed<IUserDecision[]>;

        public Title: KnockoutComputed<string>;

        constructor(private status: KnockoutObservable<ILunchState>) {
            this.status = status;

            this.UsersDecided = ko.computed(() =>
                status().UsersToday.filter(ut => ut.Decision !== UserDecision.Undecided)
                    .map(ut => <IUserDecision>{
                        UserName: ut.UserName,
                        IsGoing: ut.Decision === UserDecision.Going,
                        IsNotGoing: ut.Decision === UserDecision.NotGoing,
                        IsUndecided: ut.Decision === UserDecision.Undecided
                    }));

            this.Title = ko.computed(() => {
                var going = this.UsersDecided().filter(ud => ud.IsGoing).length;
                if (going != 1) {
                    return going + " users going today";
                }

                return "1 user going today";
            });
        }
    }
}