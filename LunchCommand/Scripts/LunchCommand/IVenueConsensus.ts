﻿module LunchCommand {

    export interface IVenueConsensus {
        Venue: IVenue;
        PercentageOfUsers: number;
    }
}