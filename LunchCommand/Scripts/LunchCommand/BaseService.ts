﻿module LunchCommand {

    export class BaseService {

        public static BaseUrl: string;

        protected ajax(url: string, method: string, data?: any): JQueryXHR {
            return $.ajax(
                {
                    accepts: "application/json",
                    contentType: "application/json",
                    data: JSON.stringify(data),
                    url: this.getBaseUrl() + url,
                    method: method
                });
        }

        private getBaseUrl(): string {
            var url = BaseService.BaseUrl;
            if (url.charAt(url.length - 1) !== '/') {
                return url + '/';
            }

            return url;
        }
    }

} 