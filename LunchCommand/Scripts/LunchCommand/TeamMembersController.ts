﻿module LunchCommand {

    export class TeamMemberViewModel {
        public Name: KnockoutObservable<string> = ko.observable("");
        public Id: number;
        public IsAdmin: KnockoutObservable<boolean> = ko.observable(false);

        public Display: KnockoutComputed<string> = ko.computed(() => { return (this.IsAdmin() ? "ADMIN: " : "") + this.Name() });
    }

    export class TeamMembersViewModel {
        public Members: KnockoutObservableArray<TeamMemberViewModel> = ko.observableArray([]);
        public SelectedUsers: KnockoutObservableArray<TeamMemberViewModel> = ko.observableArray([]);

        public save: (callback:()=>void) => void;

        public promoteUsers(pro: boolean) {
            for (var i in this.SelectedUsers()) {
                this.SelectedUsers()[i].IsAdmin(pro);
            }
        }

        public removeUsers() {
            this.Members.removeAll(this.SelectedUsers());
        }
    }

    export class TeamMembersController {

        private service: TeamMemberService = new TeamMemberService();

        public viewModel: TeamMembersViewModel;

        constructor(page: JQuery) {

            this.service.GetTeamMembers()
                .done((members: ITeamMembers) => {

                    this.viewModel = new TeamMembersViewModel();
                    this.viewModel.Members(members.Members.map(m => {
                        var vm = new TeamMemberViewModel();
                        vm.Id = m.UserId;
                        vm.IsAdmin(m.IsAdmin);
                        vm.Name(m.Name)

                        return vm;
                    }));

                    this.viewModel.save = (callback: ()=> void) => {
                        var model: ITeamMembers = {
                            Members: this.viewModel.Members().map(m => { return { Name: m.Name(), IsAdmin: m.IsAdmin(), UserId: m.Id } })
                        };

                        this.service.SaveMembers(model).done(callback);
                    };

                    ko.applyBindings(this.viewModel, page.get(0));
                });
        }
    }
}