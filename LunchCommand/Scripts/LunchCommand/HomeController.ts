﻿module LunchCommand {

    interface Dictionary<TVal> {
        [key: string]: TVal;
    };

    class GenreDecisionGroup {
        checkbox: JQuery;
        radioButtons: RadioButton[];
        genreGroup: GenreGroup;

        get checked(): boolean {
            if ((<HTMLInputElement>this.checkbox.get(0)).indeterminate) {
                return null;
            }

            return this.checkbox.prop("checked");
        }

        set checked(val: boolean) {

            if (val !== null) {
                this.checkbox.prop("checked", val);
                (<HTMLInputElement>this.checkbox.get(0)).indeterminate = false;
            }
            else {
                this.checkbox.removeProp("checked");
                (<HTMLInputElement>this.checkbox.get(0)).indeterminate = true;
            }
        }

        constructor() {
            this.radioButtons = [];
        }
    }

    class GenreGroup {
        public decisions: Dictionary<GenreDecisionGroup>;
        public name: string;

        constructor(name: string) {
            this.name = name;
            this.decisions = {};
        }

        public getDecisionGroup(dec: string) {
            this.decisions[dec] = this.decisions[dec] || new GenreDecisionGroup();
            return this.decisions[dec];
        }
    }

    class RadioButton {
        public el: JQuery;
        public group: GenreDecisionGroup;

        public get checked(): boolean {
            return this.el.prop("checked");
        }

        public set checked(val: boolean) {
            this.el.prop("checked", val);
        }

        constructor(element: JQuery, group: GenreDecisionGroup) {
            this.el = element;
            this.group = group;
        }
    }

    export class HomeController {

        private genreTable: JQuery;
        private venueTable: JQuery;

        private genres: Dictionary<GenreGroup>;

        private lastDgToGetPopover: GenreDecisionGroup;

        constructor(genreTable: JQuery, venueTable: JQuery) {
            this.genreTable = genreTable;
            this.venueTable = venueTable;
            this.genres = {};
        }

        public initialize(): void {

            this.initializeDataStructures();
            this.initializeStates();
        }

        private initializeDataStructures(): void {
            this.genreTable.find(".select-all").each((indy, element) => {
                var jqe = $(element);
                var groupName = jqe.data("group");

                var genreGroup = this.genres[groupName] = this.genres[groupName] || new GenreGroup(groupName);

                var decision = jqe.data("decision");

                var dg = genreGroup.getDecisionGroup(decision);
                dg.checkbox = jqe;
                dg.genreGroup = genreGroup;
                
                jqe.click(evt=> {
                    this.handleCheckboxClick(dg);

                    evt.preventDefault();
                    return false;
                });

                jqe.change(evt => {
                    evt.preventDefault();
                    return false;
                });
            });

            this.venueTable.find(".radio-col > input[type=radio]").each((indy, element) => {
                var jqe = $(element);
                var groupName = jqe.data("group");
                var genreGroup = this.genres[groupName];
                var decision = jqe.data("decision");
                var dg = genreGroup.getDecisionGroup(decision);
                var rb = new RadioButton(jqe, dg);

                dg.radioButtons.push(rb);

                rb.el.change(evt => {
                    this.updateCheckboxState(dg.genreGroup);
                });
            });

            jQuery("body").on("shown.bs.popover", e=> {

                window.setTimeout(() => {
                    if (this.lastDgToGetPopover) {
                        var dg = this.lastDgToGetPopover;

                        $($("[data-toggle=popover]").toArray().filter(e=> e != dg.checkbox.get(0))).popover("destroy");

                        this.lastDgToGetPopover = null;
                        var popover = $('body .lcpo[data-bound=false]').first();
                        popover.attr("data-bound", "true");

                        popover.find("button.yes").click(e=> {
                            dg.checkbox.popover("destroy");
                            dg.checked = true;
                            this.checkAllRadioButtons(dg);
                        });

                        popover.find("button.no").click(e=> {
                            dg.checkbox.popover("destroy");
                        });
                    }
                }, 1);
            });
        }

        private initializeStates(): void {
            for (var i in this.genres) {
                this.updateCheckboxState(this.genres[i]);
            }
        }

        private updateCheckboxState(genreGroup: GenreGroup): void {

            for (var i in genreGroup.decisions) {
                var decisionGroup = genreGroup.decisions[i];

                if (decisionGroup.radioButtons.every(rb=> rb.checked)) {
                    decisionGroup.checked = true;
                }
                else if (decisionGroup.radioButtons.every(rb=> !rb.checked)) {
                    decisionGroup.checked = false;
                } else {
                    decisionGroup.checked = null;
                }
            }
        }

        private checkAllRadioButtons(decisionGroup: GenreDecisionGroup): void {
            var toCheck = decisionGroup.checked;
            for (var i in decisionGroup.radioButtons) {
                var rb = decisionGroup.radioButtons[i];
                rb.checked = toCheck;
            }

            this.updateCheckboxState(decisionGroup.genreGroup);
        }

        private handleCheckboxClick(decisionGroup: GenreDecisionGroup): void {

            window.setTimeout(() => {
                if (!decisionGroup.checked) {
                    decisionGroup.checkbox.popover({
                        html: true
                    });
                    decisionGroup.checkbox.popover("show");
                    this.lastDgToGetPopover = decisionGroup;
                }
            }, 10);
        }
    }
}