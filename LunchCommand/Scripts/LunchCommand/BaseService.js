var LunchCommand;
(function (LunchCommand) {
    class BaseService {
        ajax(url, method, data) {
            return $.ajax({
                accepts: "application/json",
                contentType: "application/json",
                data: JSON.stringify(data),
                url: this.getBaseUrl() + url,
                method: method
            });
        }
        getBaseUrl() {
            var url = BaseService.BaseUrl;
            if (url.charAt(url.length - 1) !== '/') {
                return url + '/';
            }
            return url;
        }
    }
    LunchCommand.BaseService = BaseService;
})(LunchCommand || (LunchCommand = {}));
