﻿module LunchCommand {

    export class LunchStatusService extends BaseService {

        public GetStatus(): JQueryXHR {
            return super.ajax("LunchStatus", "GET");
        }

        public AnnounceDecision(decision: IUserStatus): JQueryXHR{
            return super.ajax("LunchStatus", "PUT", decision);
        }
    }
} 