﻿module LunchCommand {
    export class PropositionService extends BaseService {
        public GetPropositions(): JQueryXHR {
            return super.ajax("Proposition", "GET");
        }

        public CreateProposition(venueId: number): JQueryXHR {
            return super.ajax("Proposition", "PUT", { VenueId: venueId });
        }

        public JoinProposition(proposition: IProposition): JQueryXHR {
            return super.ajax("PropositionFollowing", "PUT", proposition);
        }

        public LeaveProposition(proposition: IProposition): JQueryXHR {
            return super.ajax("PropositionFollowing", "DELETE", proposition);
        }
    }
}