var LunchCommand;
(function (LunchCommand) {
    class LunchStatusController {
        constructor(area, propositionDropzone, usersGoingContainer, possibleVenuesContainer, decisionQuestionArea) {
            this.usersGoingContainer = usersGoingContainer;
            this.possibleVenuesContainer = possibleVenuesContainer;
            this.decisionQuestionArea = decisionQuestionArea;
            this.lastVenuesAvailableToPropose = [];
            this.koBound = false;
            this.lastState = ko.observable(null);
            this.area = area;
            this.propositionDropzone = propositionDropzone;
            this.lunchStatusService = new LunchCommand.LunchStatusService();
            this.propService = new LunchCommand.PropositionService();
            this.usersGoingContainer = usersGoingContainer;
            this.possibleVenuesContainer = possibleVenuesContainer;
            this.decisionQuestionArea = decisionQuestionArea;
            this.setUpTimer();
        }
        initialize() {
            this.afterDecisionElements = this.area.find(".after-decision");
            this.beforeDecisionElements = this.area.find(".before-decision");
            this.lunchStatusText = this.area.find(".lunch-status");
            this.mainVenueText = this.area.find("#mainVenue");
            this.quickVenueText = this.area.find("#quickVenue");
            this.quickVenueArea = this.area.find(".quick-venue");
            this.dissentingArea = this.area.find(".dissenting-venue");
            this.dissentingVenueText = this.area.find("#dissentingVenue");
            this.mainConsensus = this.area.find("#mainConsensus");
            this.quickConsensus = this.area.find("#quickConsensus");
            this.dissentingConsensus = this.area.find("#dissentingConsensus");
            this.timeText = this.area.find("#timeUntilLunch");
            this.travelTimeText = this.area.find("#travelTime");
            this.propositionTemplate = this.propositionDropzone.find("#proposition-template");
            this.newPropositionDiv = this.propositionDropzone.find(".new-proposition");
            this.ungroupedLunchers = this.propositionDropzone.parent().find(".ungrouped-lunchers");
            this.venuePropositionSelect = this.newPropositionDiv.find(".venue-proposition-select");
            this.proposeButton = this.newPropositionDiv.find(".propose-button");
            this.venuePropositionSelect.change(e => {
                if (this.venuePropositionSelect.val() == -1) {
                    this.proposeButton.prop("disabled", true);
                }
                else {
                    this.proposeButton.prop("disabled", false);
                }
            });
            this.proposeButton.click(() => {
                this.propService.CreateProposition(this.venuePropositionSelect.val())
                    .done(() => this.getStatus());
            });
            this.getStatus();
        }
        getStatus() {
            this.lunchStatusService.GetStatus()
                .done((status) => this.processState(status))
                .done(() => this.bindViewModel())
                .done((status) => {
                this.propService.GetPropositions()
                    .done((props) => {
                    this.processPropositions(props, status.UserDecision);
                });
            })
                .fail(failure => {
                console.error("Failed to get lunch status");
                console.info(failure);
                window.setTimeout(() => { this.getStatus(); }, 60000);
            });
        }
        processPropositions(props, decision) {
            this.propositionDropzone.find(".proposition").remove();
            var userHasChosenOne = false;
            for (var i in props.Propositions) {
                var p = props.Propositions[i];
                props.Propositions[i];
                var newProp = jQuery(this.propositionTemplate.text());
                newProp.find(".proposed-venue").text(this.htmlDecode(p.Venue));
                newProp.find(".proposer").text(this.htmlDecode(p.Proposer));
                newProp.find(".followers").html(p.Followers.map(f => "<strong>" + this.htmlDecode(f) + "</strong>").join(", "));
                newProp.data("proposition", p);
                var pos = {
                    position: {
                        my: "center bottom",
                        at: "center top-5"
                    }
                };
                newProp.tooltip(pos);
                if (p.IsUserGoing) {
                    userHasChosenOne = true;
                    newProp.find(".proposition-header").attr("title", "Click to leave this group");
                    if (props.LunchHasStarted == false) {
                        newProp.find(".proposition-header").click((e) => {
                            var toLeave = $(e.delegateTarget).parent(".proposition").data("proposition");
                            this.propService.LeaveProposition(toLeave)
                                .done(() => this.getStatus());
                        });
                    }
                    newProp.find(".proposition-header").removeClass("panel-info").addClass("panel-success");
                    newProp.find(".leave-or-join").text("leave");
                }
                else {
                    newProp.find(".proposition-header").attr("title", "Click to join " + this.htmlDecode(p.Proposer) + " at " + this.htmlDecode(p.Venue));
                    if (props.LunchHasStarted == false) {
                        newProp.find(".proposition-header").click((e) => {
                            var toJoin = $(e.delegateTarget).parent(".proposition").data("proposition");
                            this.propService.JoinProposition(toJoin)
                                .done(() => this.getStatus());
                        });
                    }
                    newProp.find(".leave-or-join").text("join");
                }
                this.propositionDropzone.append(newProp);
            }
            if (props.Propositions.length && props.UndecidedUsers.length) {
                this.ungroupedLunchers.show();
                this.ungroupedLunchers.find(".followers").html(props.UndecidedUsers.map(f => "<strong>" + this.htmlDecode(f) + "</strong>").join(", "));
            }
            else {
                this.ungroupedLunchers.hide();
            }
            if (userHasChosenOne || props.LunchHasStarted || decision !== LunchCommand.UserDecision.Going) {
                this.newPropositionDiv.hide();
            }
            else {
                this.newPropositionDiv.show();
            }
            if (!this.lastVenuesAvailableToPropose.length || !this.lastVenuesAvailableToPropose.every(v => props.RemainingVenuesToPick.some(rv => rv.VenueId === v))) {
                var oldSelection = this.venuePropositionSelect.val();
                this.venuePropositionSelect.empty();
                var newOption = $("<option/>");
                newOption.text("Select a venue");
                newOption.val(-1);
                this.venuePropositionSelect.append(newOption);
                for (var i in props.RemainingVenuesToPick) {
                    var v = props.RemainingVenuesToPick[i];
                    newOption = $("<option/>");
                    newOption.text(this.htmlDecode(v.Name));
                    newOption.val(v.VenueId);
                    this.venuePropositionSelect.append(newOption);
                }
            }
            this.lastVenuesAvailableToPropose = props.RemainingVenuesToPick.map(v => v.VenueId);
        }
        processState(status) {
            this.lastState(status);
            this.lunchStatusText.text(status.StatusText);
            var timeout = 3600000;
            switch (status.Status) {
                case LunchCommand.LunchStatus.NotStarted:
                    this.beforeDecisionElements.show();
                    this.afterDecisionElements.hide();
                    timeout = 15000;
                    break;
                case LunchCommand.LunchStatus.DidNotHappen:
                    this.beforeDecisionElements.hide();
                    this.afterDecisionElements.hide();
                    break;
                case LunchCommand.LunchStatus.StartedWithConsensus:
                case LunchCommand.LunchStatus.StartedWithoutConsensus:
                    this.beforeDecisionElements.hide();
                    if (status.DecidedVenue == null) {
                        this.mainVenueText.text("No venues could be determined");
                    }
                    else {
                        this.mainVenueText.text(status.DecidedVenue.Venue.Name);
                        this.mainConsensus.text(status.DecidedVenue.PercentageOfUsers + '%');
                        if (status.MinutesToTravel) {
                            this.travelTimeText.text("Current Travel Time: " + status.MinutesToTravel + " minutes");
                            this.travelTimeText.show();
                        }
                        else {
                            this.travelTimeText.hide();
                        }
                    }
                    if (status.QuickVenue != null && status.QuickVenue.Venue.VenueId !== status.DecidedVenue.Venue.VenueId) {
                        this.quickVenueArea.show();
                        this.quickVenueText.text(status.QuickVenue.Venue.Name);
                        this.quickConsensus.text('(' + status.QuickVenue.PercentageOfUsers + '% Consensus)');
                    }
                    else {
                        this.quickVenueArea.hide();
                    }
                    if (status.DissentingVenue != null) {
                        this.dissentingArea.show();
                        this.dissentingVenueText.text(status.DissentingVenue.Venue.Name);
                        this.dissentingConsensus.text('(' + status.DissentingVenue.PercentageOfUsers + '% Consensus among dissenters)');
                    }
                    else {
                        this.dissentingArea.hide();
                    }
                    this.afterDecisionElements.show();
                    break;
                case LunchCommand.LunchStatus.UserHasDecidedManually:
                    timeout = 15000;
                    this.mainVenueText.text(status.DecidedVenue.Venue.Name);
                    this.beforeDecisionElements.hide();
                    this.mainConsensus.parent().hide();
                    if (status.MinutesToTravel) {
                        this.travelTimeText.text("Current Travel Time: " + status.MinutesToTravel + " minutes");
                        this.travelTimeText.show();
                    }
                    else {
                        this.travelTimeText.hide();
                    }
                    this.dissentingArea.hide();
                    this.quickVenueArea.hide();
                    this.afterDecisionElements.show();
                    break;
            }
            window.setTimeout(() => { this.getStatus(); }, timeout);
        }
        bindViewModel() {
            if (!this.koBound) {
                this.koBound = true;
                ko.applyBindings(new LunchCommand.UsersGoingViewModel(this.lastState), this.usersGoingContainer.get(0));
                ko.applyBindings(this.lastState, this.possibleVenuesContainer.get(0));
                ko.applyBindings(new LunchCommand.UserDecisionViewModel(this.lastState, x => { this.handleUserDecision(x); }), this.decisionQuestionArea.get(0));
            }
        }
        handleUserDecision(decision) {
            this.lunchStatusService.AnnounceDecision({
                Decision: decision
            }).done(() => {
                this.getStatus();
            });
        }
        setUpTimer() {
            var milliseconds = 0;
            this.lastState.subscribe(() => {
                if (this.lastState()) {
                    milliseconds = this.lastState().TimeRemaining;
                }
            });
            window.setInterval(() => {
                milliseconds = Math.max(0, milliseconds - 1000);
                if (milliseconds == 0) {
                    this.timeText.text("just a moment...");
                    return;
                }
                var totalSeconds = Math.floor(milliseconds / 1000);
                var totalMinutes = Math.floor(totalSeconds / 60);
                var totalHours = Math.floor(totalSeconds / 3600);
                var seconds = totalSeconds % 60;
                var minutes = totalMinutes % 60;
                this.timeText.text(totalHours + ":" + (minutes < 10 ? "0" : "") + minutes + ":" + (seconds < 10 ? "0" : "") + seconds);
            }, 1000);
        }
        htmlDecode(value) {
            return $('<div/>').html(value).text();
        }
    }
    LunchCommand.LunchStatusController = LunchStatusController;
})(LunchCommand || (LunchCommand = {}));
