﻿module LunchCommand {
    export interface IPropositionList {

        Propositions: IProposition[];

        RemainingVenuesToPick: IVenue[];

        LunchHasStarted: boolean;

        UndecidedUsers: string[];
    }
}