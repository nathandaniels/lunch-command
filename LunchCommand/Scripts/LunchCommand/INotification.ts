﻿module LunchCommand {
    export enum NotificationType {
        LunchStarted,
        NotDecided,
        NewProposition
    }

    export interface INotification {
        Message: string;
        Type: NotificationType;
        PropositionId?: number;
    }
}