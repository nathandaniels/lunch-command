﻿module LunchCommand {

    export enum LunchStatus {
        StartedWithConsensus = 0,
        StartedWithoutConsensus,
        NotStarted,
        DidNotHappen,
        UserHasDecidedManually
    }
}