﻿module LunchCommand {

    export interface IDecisionCallback {
        (decision: UserDecision): void;
    }

    export class UserDecisionViewModel {
    
        public lunchIsNigh: KnockoutComputed<boolean>;

        public userHasDecided: KnockoutComputed<boolean>;

        public userIsGoing: KnockoutComputed<boolean>;

        private callback: IDecisionCallback;
        
        constructor(state: KnockoutObservable<ILunchState>, decisionCallback: IDecisionCallback) {
            this.callback = decisionCallback;

            this.lunchIsNigh = ko.computed(() => state().Status === LunchStatus.NotStarted);
            this.userHasDecided = ko.computed(() => state().UserDecision !== UserDecision.Unanswered);
            this.userIsGoing = ko.computed(() => state().UserDecision === UserDecision.Going);
        }

        public decideYes(): void {
            this.callback(UserDecision.Going);
        }

        public decideNo(): void {
            this.callback(UserDecision.NotGoing);
        }
    }

}