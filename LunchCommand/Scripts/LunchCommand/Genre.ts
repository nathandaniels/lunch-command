﻿module LunchCommand {
    export interface IGenre {
        Name: string;
        GenreID: number;
        VenueCount: number;
        DaysSinceLastEaten: number;
    }
} 