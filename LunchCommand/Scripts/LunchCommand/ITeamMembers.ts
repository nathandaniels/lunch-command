﻿module LunchCommand {

    export interface ITeamMember {
        UserId: number;
        Name: string;
        IsAdmin: boolean;
    }

    export interface ITeamMembers {
        Members: ITeamMember[];
    }
}