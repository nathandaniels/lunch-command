﻿module LunchCommand {
    export interface IProposition {

        Proposer: string;

        Followers: string[];

        PropositionId: number;

        IsUserGoing: boolean;

        Venue: string;
    }
}