﻿module LunchCommand {

    export class WindowFocusListener {
        public focused: KnockoutObservable<boolean> = ko.observable(true);

        constructor() {
            $(window).focus(() => { this.focused(true); })
            $(window).blur(() => { this.focused(false); })
        }
    }
}