﻿module LunchCommand {
    export class BroswerNotificationsService extends BaseService {

        public GetNotifications(): JQueryXHR {
            return super.ajax("Notifications", "GET");
        }
    }
}