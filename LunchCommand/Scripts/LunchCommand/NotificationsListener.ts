﻿declare var Notification: any;

module LunchCommand {

    export interface IPropositionNotificationInfo {
        PropositionId: number;
    }

    export class NotificationsListener {

        private service: BroswerNotificationsService;

        private canNotify: boolean;

        private static preemptedPropositionNotifications: IPropositionNotificationInfo[] = [];

        constructor(public imageUrl: string) {
            this.service = new BroswerNotificationsService();
            this.imageUrl = imageUrl;

            this.canNotify = ("Notification" in window);

            if (this.canNotify && Notification.permission === "default") {
                Notification.requestPermission();
            }
        }

        public static preemptPropositionNotification(info: IPropositionNotificationInfo) {

            // Don't notify on preempted proposition notifications
            if (!NotificationsListener.preemptedPropositionNotifications.some(info2 => info2.PropositionId === info.PropositionId)) {
                NotificationsListener.preemptedPropositionNotifications.push(info);
            }
        }

        public static clearAllPropositionNotificationPreemptions() {
            NotificationsListener.preemptedPropositionNotifications = [];
        }

        public begin(): void {
            if (this.canNotify) {

                var getNotifications = (displayNotifications: boolean) => {
                    this.service.GetNotifications().done((notes: INotification[]) => {
                        if (Notification.permission === "granted" && displayNotifications) {
                            // Fire away
                            for (var i in notes) {

                                // Don't notify on preempted proposition notifications
                                if (NotificationsListener.preemptedPropositionNotifications.some(info => info.PropositionId === notes[i].PropositionId)) {
                                    continue;
                                }

                                var notification = new Notification(
                                    "Lunch Command",
                                    {
                                        body: notes[i].Message,
                                        icon: this.imageUrl,
                                        badge: this.imageUrl,
                                        lang: "en"
                                    }
                                );
                            }
                        }
                    });
                };

                // Get notifications right away but supress actually showing them (since the user is obviously on the page)
                getNotifications(false);

                window.setInterval(getNotifications, 30 * 1000, true);
            }
        }
    }
}