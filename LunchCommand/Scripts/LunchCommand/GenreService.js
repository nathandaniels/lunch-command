var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var LunchCommand;
(function (LunchCommand) {
    var LunchStatusService = (function (_super) {
        __extends(LunchStatusService, _super);
        function LunchStatusService() {
            _super.apply(this, arguments);
        }
        LunchStatusService.prototype.GetStatus = function () {
            return _super.prototype.ajax.call(this, "LunchStatus", "GET");
        };
        return LunchStatusService;
    })(LunchCommand.BaseService);
    LunchCommand.LunchStatusService = LunchStatusService;
})(LunchCommand || (LunchCommand = {}));
//# sourceMappingURL=GenreService.js.map