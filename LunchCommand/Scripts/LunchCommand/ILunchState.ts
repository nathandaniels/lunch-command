﻿ module LunchCommand {

     export enum UserDecision {
         Going,
         Unanswered,
         Undecided,
         NotGoing
     }

     export interface IUserStatus {
         UserName?: string;
         Decision: UserDecision;
     }

    export interface ILunchState {
        Status: LunchStatus;
        StatusText: string;
        TimeRemaining: number;
        DecidedVenue: IVenueConsensus;
        QuickVenue: IVenueConsensus;
        DissentingVenue: IVenueConsensus;
        MinutesToTravel: number;
        UsersToday: IUserStatus[];
        PossibleVenues: IVenue[];
        UserDecision: UserDecision;
    }
}