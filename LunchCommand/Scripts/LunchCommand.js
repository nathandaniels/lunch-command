var LunchCommand;
(function (LunchCommand) {
    class BaseService {
        ajax(url, method, data) {
            return $.ajax({
                accepts: "application/json",
                contentType: "application/json",
                data: JSON.stringify(data),
                url: this.getBaseUrl() + url,
                method: method
            });
        }
        getBaseUrl() {
            var url = BaseService.BaseUrl;
            if (url.charAt(url.length - 1) !== '/') {
                return url + '/';
            }
            return url;
        }
    }
    LunchCommand.BaseService = BaseService;
})(LunchCommand || (LunchCommand = {}));
var LunchCommand;
(function (LunchCommand) {
    class BroswerNotificationsService extends LunchCommand.BaseService {
        GetNotifications() {
            return super.ajax("Notifications", "GET");
        }
    }
    LunchCommand.BroswerNotificationsService = BroswerNotificationsService;
})(LunchCommand || (LunchCommand = {}));
var LunchCommand;
(function (LunchCommand) {
    (function (NotificationType) {
        NotificationType[NotificationType["LunchStarted"] = 0] = "LunchStarted";
        NotificationType[NotificationType["NotDecided"] = 1] = "NotDecided";
        NotificationType[NotificationType["NewProposition"] = 2] = "NewProposition";
    })(LunchCommand.NotificationType || (LunchCommand.NotificationType = {}));
    var NotificationType = LunchCommand.NotificationType;
})(LunchCommand || (LunchCommand = {}));
var LunchCommand;
(function (LunchCommand) {
    class NotificationsListener {
        constructor(imageUrl) {
            this.imageUrl = imageUrl;
            this.service = new LunchCommand.BroswerNotificationsService();
            this.imageUrl = imageUrl;
            this.canNotify = ("Notification" in window);
            if (this.canNotify && Notification.permission === "default") {
                Notification.requestPermission();
            }
        }
        static preemptPropositionNotification(info) {
            if (!NotificationsListener.preemptedPropositionNotifications.some(info2 => info2.PropositionId === info.PropositionId)) {
                NotificationsListener.preemptedPropositionNotifications.push(info);
            }
        }
        static clearAllPropositionNotificationPreemptions() {
            NotificationsListener.preemptedPropositionNotifications = [];
        }
        begin() {
            if (this.canNotify) {
                var getNotifications = (displayNotifications) => {
                    this.service.GetNotifications().done((notes) => {
                        if (Notification.permission === "granted" && displayNotifications) {
                            for (var i in notes) {
                                if (NotificationsListener.preemptedPropositionNotifications.some(info => info.PropositionId === notes[i].PropositionId)) {
                                    continue;
                                }
                                var notification = new Notification("Lunch Command", {
                                    body: notes[i].Message,
                                    icon: this.imageUrl,
                                    badge: this.imageUrl,
                                    lang: "en"
                                });
                            }
                        }
                    });
                };
                getNotifications(false);
                window.setInterval(getNotifications, 30 * 1000, true);
            }
        }
    }
    NotificationsListener.preemptedPropositionNotifications = [];
    LunchCommand.NotificationsListener = NotificationsListener;
})(LunchCommand || (LunchCommand = {}));
var LunchCommand;
(function (LunchCommand) {
    class TeamMemberService extends LunchCommand.BaseService {
        GetTeamMembers() {
            return super.ajax("TeamMember", "GET");
        }
        SaveMembers(model) {
            return super.ajax("TeamMember", "PUT", model);
        }
    }
    LunchCommand.TeamMemberService = TeamMemberService;
})(LunchCommand || (LunchCommand = {}));
var LunchCommand;
(function (LunchCommand) {
    class LunchStatusService extends LunchCommand.BaseService {
        GetStatus() {
            return super.ajax("LunchStatus", "GET");
        }
        AnnounceDecision(decision) {
            return super.ajax("LunchStatus", "PUT", decision);
        }
    }
    LunchCommand.LunchStatusService = LunchStatusService;
})(LunchCommand || (LunchCommand = {}));
var LunchCommand;
(function (LunchCommand) {
    ;
    class GenreDecisionGroup {
        constructor() {
            this.radioButtons = [];
        }
        get checked() {
            if (this.checkbox.get(0).indeterminate) {
                return null;
            }
            return this.checkbox.prop("checked");
        }
        set checked(val) {
            if (val !== null) {
                this.checkbox.prop("checked", val);
                this.checkbox.get(0).indeterminate = false;
            }
            else {
                this.checkbox.removeProp("checked");
                this.checkbox.get(0).indeterminate = true;
            }
        }
    }
    class GenreGroup {
        constructor(name) {
            this.name = name;
            this.decisions = {};
        }
        getDecisionGroup(dec) {
            this.decisions[dec] = this.decisions[dec] || new GenreDecisionGroup();
            return this.decisions[dec];
        }
    }
    class RadioButton {
        constructor(element, group) {
            this.el = element;
            this.group = group;
        }
        get checked() {
            return this.el.prop("checked");
        }
        set checked(val) {
            this.el.prop("checked", val);
        }
    }
    class HomeController {
        constructor(genreTable, venueTable) {
            this.genreTable = genreTable;
            this.venueTable = venueTable;
            this.genres = {};
        }
        initialize() {
            this.initializeDataStructures();
            this.initializeStates();
        }
        initializeDataStructures() {
            this.genreTable.find(".select-all").each((indy, element) => {
                var jqe = $(element);
                var groupName = jqe.data("group");
                var genreGroup = this.genres[groupName] = this.genres[groupName] || new GenreGroup(groupName);
                var decision = jqe.data("decision");
                var dg = genreGroup.getDecisionGroup(decision);
                dg.checkbox = jqe;
                dg.genreGroup = genreGroup;
                jqe.click(evt => {
                    this.handleCheckboxClick(dg);
                    evt.preventDefault();
                    return false;
                });
                jqe.change(evt => {
                    evt.preventDefault();
                    return false;
                });
            });
            this.venueTable.find(".radio-col > input[type=radio]").each((indy, element) => {
                var jqe = $(element);
                var groupName = jqe.data("group");
                var genreGroup = this.genres[groupName];
                var decision = jqe.data("decision");
                var dg = genreGroup.getDecisionGroup(decision);
                var rb = new RadioButton(jqe, dg);
                dg.radioButtons.push(rb);
                rb.el.change(evt => {
                    this.updateCheckboxState(dg.genreGroup);
                });
            });
            jQuery("body").on("shown.bs.popover", e => {
                window.setTimeout(() => {
                    if (this.lastDgToGetPopover) {
                        var dg = this.lastDgToGetPopover;
                        $($("[data-toggle=popover]").toArray().filter(e => e != dg.checkbox.get(0))).popover("destroy");
                        this.lastDgToGetPopover = null;
                        var popover = $('body .lcpo[data-bound=false]').first();
                        popover.attr("data-bound", "true");
                        popover.find("button.yes").click(e => {
                            dg.checkbox.popover("destroy");
                            dg.checked = true;
                            this.checkAllRadioButtons(dg);
                        });
                        popover.find("button.no").click(e => {
                            dg.checkbox.popover("destroy");
                        });
                    }
                }, 1);
            });
        }
        initializeStates() {
            for (var i in this.genres) {
                this.updateCheckboxState(this.genres[i]);
            }
        }
        updateCheckboxState(genreGroup) {
            for (var i in genreGroup.decisions) {
                var decisionGroup = genreGroup.decisions[i];
                if (decisionGroup.radioButtons.every(rb => rb.checked)) {
                    decisionGroup.checked = true;
                }
                else if (decisionGroup.radioButtons.every(rb => !rb.checked)) {
                    decisionGroup.checked = false;
                }
                else {
                    decisionGroup.checked = null;
                }
            }
        }
        checkAllRadioButtons(decisionGroup) {
            var toCheck = decisionGroup.checked;
            for (var i in decisionGroup.radioButtons) {
                var rb = decisionGroup.radioButtons[i];
                rb.checked = toCheck;
            }
            this.updateCheckboxState(decisionGroup.genreGroup);
        }
        handleCheckboxClick(decisionGroup) {
            window.setTimeout(() => {
                if (!decisionGroup.checked) {
                    decisionGroup.checkbox.popover({
                        html: true
                    });
                    decisionGroup.checkbox.popover("show");
                    this.lastDgToGetPopover = decisionGroup;
                }
            }, 10);
        }
    }
    LunchCommand.HomeController = HomeController;
})(LunchCommand || (LunchCommand = {}));
var LunchCommand;
(function (LunchCommand) {
    (function (UserDecision) {
        UserDecision[UserDecision["Going"] = 0] = "Going";
        UserDecision[UserDecision["Unanswered"] = 1] = "Unanswered";
        UserDecision[UserDecision["Undecided"] = 2] = "Undecided";
        UserDecision[UserDecision["NotGoing"] = 3] = "NotGoing";
    })(LunchCommand.UserDecision || (LunchCommand.UserDecision = {}));
    var UserDecision = LunchCommand.UserDecision;
})(LunchCommand || (LunchCommand = {}));
var LunchCommand;
(function (LunchCommand) {
    (function (LunchStatus) {
        LunchStatus[LunchStatus["StartedWithConsensus"] = 0] = "StartedWithConsensus";
        LunchStatus[LunchStatus["StartedWithoutConsensus"] = 1] = "StartedWithoutConsensus";
        LunchStatus[LunchStatus["NotStarted"] = 2] = "NotStarted";
        LunchStatus[LunchStatus["DidNotHappen"] = 3] = "DidNotHappen";
        LunchStatus[LunchStatus["UserHasDecidedManually"] = 4] = "UserHasDecidedManually";
    })(LunchCommand.LunchStatus || (LunchCommand.LunchStatus = {}));
    var LunchStatus = LunchCommand.LunchStatus;
})(LunchCommand || (LunchCommand = {}));
var LunchCommand;
(function (LunchCommand) {
    class LunchStatusController {
        constructor(area, propositionDropzone, usersGoingContainer, possibleVenuesContainer, decisionQuestionArea) {
            this.usersGoingContainer = usersGoingContainer;
            this.possibleVenuesContainer = possibleVenuesContainer;
            this.decisionQuestionArea = decisionQuestionArea;
            this.lastVenuesAvailableToPropose = [];
            this.koBound = false;
            this.windowFocusListener = new LunchCommand.WindowFocusListener();
            this.lastState = ko.observable(null);
            this.lastPropositions = ko.observable([]);
            this.area = area;
            this.propositionDropzone = propositionDropzone;
            this.lunchStatusService = new LunchCommand.LunchStatusService();
            this.propService = new LunchCommand.PropositionService();
            this.usersGoingContainer = usersGoingContainer;
            this.possibleVenuesContainer = possibleVenuesContainer;
            this.decisionQuestionArea = decisionQuestionArea;
            this.setUpTimer();
        }
        initialize() {
            this.afterDecisionElements = this.area.find(".after-decision");
            this.beforeDecisionElements = this.area.find(".before-decision");
            this.lunchStatusText = this.area.find(".lunch-status");
            this.mainVenueText = this.area.find("#mainVenue");
            this.quickVenueText = this.area.find("#quickVenue");
            this.quickVenueArea = this.area.find(".quick-venue");
            this.dissentingArea = this.area.find(".dissenting-venue");
            this.dissentingVenueText = this.area.find("#dissentingVenue");
            this.mainConsensus = this.area.find("#mainConsensus");
            this.quickConsensus = this.area.find("#quickConsensus");
            this.dissentingConsensus = this.area.find("#dissentingConsensus");
            this.timeText = this.area.find("#timeUntilLunch");
            this.travelTimeText = this.area.find("#travelTime");
            this.propositionTemplate = this.propositionDropzone.find("#proposition-template");
            this.newPropositionDiv = this.propositionDropzone.find(".new-proposition");
            this.ungroupedLunchers = this.propositionDropzone.parent().find(".ungrouped-lunchers");
            this.venuePropositionSelect = this.newPropositionDiv.find(".venue-proposition-select");
            this.proposeButton = this.newPropositionDiv.find(".propose-button");
            this.venuePropositionSelect.change(e => {
                if (this.venuePropositionSelect.val() == -1) {
                    this.proposeButton.prop("disabled", true);
                }
                else {
                    this.proposeButton.prop("disabled", false);
                }
            });
            this.proposeButton.click(() => {
                this.propService.CreateProposition(this.venuePropositionSelect.val())
                    .done(() => this.getStatus());
            });
            this.getStatus();
            this.windowFocusListener.focused.subscribe(() => {
                if (this.windowFocusListener.focused()) {
                    for (var i in this.lastPropositions()) {
                        LunchCommand.NotificationsListener.preemptPropositionNotification({
                            PropositionId: this.lastPropositions()[i].PropositionId
                        });
                    }
                }
            });
        }
        getStatus() {
            this.lunchStatusService.GetStatus()
                .done((status) => this.processState(status))
                .done(() => this.bindViewModel())
                .done((status) => {
                this.propService.GetPropositions()
                    .done((props) => {
                    this.processPropositions(props, status.UserDecision);
                });
            })
                .fail(failure => {
                console.error("Failed to get lunch status");
                console.info(failure);
                window.setTimeout(() => { this.getStatus(); }, 60000);
            });
        }
        processPropositions(props, decision) {
            this.propositionDropzone.find(".proposition").remove();
            var userHasChosenOne = false;
            this.lastPropositions(props.Propositions);
            for (var i in props.Propositions) {
                var p = props.Propositions[i];
                props.Propositions[i];
                if (this.windowFocusListener.focused()) {
                    LunchCommand.NotificationsListener.preemptPropositionNotification({
                        PropositionId: p.PropositionId
                    });
                }
                var newProp = jQuery(this.propositionTemplate.text());
                newProp.find(".proposed-venue").text(this.htmlDecode(p.Venue));
                newProp.find(".proposer").text(this.htmlDecode(p.Proposer));
                newProp.find(".followers").html(p.Followers.map(f => "<strong>" + this.htmlDecode(f) + "</strong>").join(", "));
                newProp.data("proposition", p);
                var pos = {
                    position: {
                        my: "center bottom",
                        at: "center top-5"
                    }
                };
                newProp.tooltip(pos);
                if (p.IsUserGoing) {
                    userHasChosenOne = true;
                    newProp.find(".proposition-header").attr("title", "Click to leave this group");
                    newProp.find(".proposition-header").click((e) => {
                        var toLeave = $(e.delegateTarget).parent(".proposition").data("proposition");
                        this.propService.LeaveProposition(toLeave)
                            .done(() => this.getStatus());
                    });
                    newProp.find(".proposition-header").removeClass("panel-info").addClass("panel-success");
                    newProp.find(".leave-or-join").text("leave");
                }
                else {
                    newProp.find(".proposition-header").attr("title", "Click to join " + this.htmlDecode(p.Proposer) + " at " + this.htmlDecode(p.Venue));
                    newProp.find(".proposition-header").click((e) => {
                        var toJoin = $(e.delegateTarget).parent(".proposition").data("proposition");
                        this.propService.JoinProposition(toJoin)
                            .done(() => this.getStatus());
                    });
                    newProp.find(".leave-or-join").text("join");
                }
                this.propositionDropzone.append(newProp);
            }
            if (props.Propositions.length && props.UndecidedUsers.length) {
                this.ungroupedLunchers.show();
                this.ungroupedLunchers.find(".followers").html(props.UndecidedUsers.map(f => "<strong>" + this.htmlDecode(f) + "</strong>").join(", "));
            }
            else {
                this.ungroupedLunchers.hide();
            }
            if (userHasChosenOne || props.LunchHasStarted || decision !== LunchCommand.UserDecision.Going) {
                this.newPropositionDiv.hide();
            }
            else {
                this.newPropositionDiv.show();
            }
            if (!this.lastVenuesAvailableToPropose.length || !this.lastVenuesAvailableToPropose.every(v => props.RemainingVenuesToPick.some(rv => rv.VenueId === v))) {
                var oldSelection = this.venuePropositionSelect.val();
                this.venuePropositionSelect.empty();
                var newOption = $("<option/>");
                newOption.text("Select a venue");
                newOption.val(-1);
                this.venuePropositionSelect.append(newOption);
                for (var i in props.RemainingVenuesToPick) {
                    var v = props.RemainingVenuesToPick[i];
                    newOption = $("<option/>");
                    newOption.text(this.htmlDecode(v.Name));
                    newOption.val(v.VenueId);
                    this.venuePropositionSelect.append(newOption);
                }
            }
            this.lastVenuesAvailableToPropose = props.RemainingVenuesToPick.map(v => v.VenueId);
        }
        processState(status) {
            this.lastState(status);
            this.lunchStatusText.text(status.StatusText);
            var timeout = 3600000;
            switch (status.Status) {
                case LunchCommand.LunchStatus.NotStarted:
                    this.beforeDecisionElements.show();
                    this.afterDecisionElements.hide();
                    timeout = 15000;
                    break;
                case LunchCommand.LunchStatus.DidNotHappen:
                    this.beforeDecisionElements.hide();
                    this.afterDecisionElements.hide();
                    break;
                case LunchCommand.LunchStatus.StartedWithConsensus:
                case LunchCommand.LunchStatus.StartedWithoutConsensus:
                    this.beforeDecisionElements.hide();
                    LunchCommand.NotificationsListener.clearAllPropositionNotificationPreemptions();
                    if (status.DecidedVenue == null) {
                        this.mainVenueText.text("No venues could be determined");
                    }
                    else {
                        this.mainVenueText.text(status.DecidedVenue.Venue.Name);
                        this.mainConsensus.text(status.DecidedVenue.PercentageOfUsers + '%');
                        if (status.MinutesToTravel) {
                            this.travelTimeText.text("Current Travel Time: " + status.MinutesToTravel + " minutes");
                            this.travelTimeText.show();
                        }
                        else {
                            this.travelTimeText.hide();
                        }
                    }
                    if (status.QuickVenue != null && status.QuickVenue.Venue.VenueId !== status.DecidedVenue.Venue.VenueId) {
                        this.quickVenueArea.show();
                        this.quickVenueText.text(status.QuickVenue.Venue.Name);
                        this.quickConsensus.text('(' + status.QuickVenue.PercentageOfUsers + '% Consensus)');
                    }
                    else {
                        this.quickVenueArea.hide();
                    }
                    if (status.DissentingVenue != null) {
                        this.dissentingArea.show();
                        this.dissentingVenueText.text(status.DissentingVenue.Venue.Name);
                        this.dissentingConsensus.text('(' + status.DissentingVenue.PercentageOfUsers + '% Consensus among dissenters)');
                    }
                    else {
                        this.dissentingArea.hide();
                    }
                    this.afterDecisionElements.show();
                    break;
                case LunchCommand.LunchStatus.UserHasDecidedManually:
                    timeout = 15000;
                    LunchCommand.NotificationsListener.clearAllPropositionNotificationPreemptions();
                    this.mainVenueText.text(status.DecidedVenue.Venue.Name);
                    this.beforeDecisionElements.hide();
                    this.mainConsensus.parent().hide();
                    if (status.MinutesToTravel) {
                        this.travelTimeText.text("Current Travel Time: " + status.MinutesToTravel + " minutes");
                        this.travelTimeText.show();
                    }
                    else {
                        this.travelTimeText.hide();
                    }
                    this.dissentingArea.hide();
                    this.quickVenueArea.hide();
                    this.afterDecisionElements.show();
                    break;
            }
            window.setTimeout(() => { this.getStatus(); }, timeout);
        }
        bindViewModel() {
            if (!this.koBound) {
                this.koBound = true;
                ko.applyBindings(new LunchCommand.UsersGoingViewModel(this.lastState), this.usersGoingContainer.get(0));
                ko.applyBindings(this.lastState, this.possibleVenuesContainer.get(0));
                ko.applyBindings(new LunchCommand.UserDecisionViewModel(this.lastState, x => { this.handleUserDecision(x); }), this.decisionQuestionArea.get(0));
            }
        }
        handleUserDecision(decision) {
            this.lunchStatusService.AnnounceDecision({
                Decision: decision
            }).done(() => {
                this.getStatus();
            });
        }
        setUpTimer() {
            var milliseconds = 0;
            this.lastState.subscribe(() => {
                if (this.lastState()) {
                    milliseconds = this.lastState().TimeRemaining;
                }
            });
            window.setInterval(() => {
                milliseconds = Math.max(0, milliseconds - 1000);
                if (milliseconds == 0) {
                    this.timeText.text("just a moment...");
                    return;
                }
                var totalSeconds = Math.floor(milliseconds / 1000);
                var totalMinutes = Math.floor(totalSeconds / 60);
                var totalHours = Math.floor(totalSeconds / 3600);
                var seconds = totalSeconds % 60;
                var minutes = totalMinutes % 60;
                this.timeText.text(totalHours + ":" + (minutes < 10 ? "0" : "") + minutes + ":" + (seconds < 10 ? "0" : "") + seconds);
            }, 1000);
        }
        htmlDecode(value) {
            return $('<div/>').html(value).text();
        }
    }
    LunchCommand.LunchStatusController = LunchStatusController;
})(LunchCommand || (LunchCommand = {}));
var LunchCommand;
(function (LunchCommand) {
    class PropositionService extends LunchCommand.BaseService {
        GetPropositions() {
            return super.ajax("Proposition", "GET");
        }
        CreateProposition(venueId) {
            return super.ajax("Proposition", "PUT", { VenueId: venueId });
        }
        JoinProposition(proposition) {
            return super.ajax("PropositionFollowing", "PUT", proposition);
        }
        LeaveProposition(proposition) {
            return super.ajax("PropositionFollowing", "DELETE", proposition);
        }
    }
    LunchCommand.PropositionService = PropositionService;
})(LunchCommand || (LunchCommand = {}));
var LunchCommand;
(function (LunchCommand) {
    class TeamMemberViewModel {
        constructor() {
            this.Name = ko.observable("");
            this.IsAdmin = ko.observable(false);
            this.Display = ko.computed(() => { return (this.IsAdmin() ? "ADMIN: " : "") + this.Name(); });
        }
    }
    LunchCommand.TeamMemberViewModel = TeamMemberViewModel;
    class TeamMembersViewModel {
        constructor() {
            this.Members = ko.observableArray([]);
            this.SelectedUsers = ko.observableArray([]);
        }
        promoteUsers(pro) {
            for (var i in this.SelectedUsers()) {
                this.SelectedUsers()[i].IsAdmin(pro);
            }
        }
        removeUsers() {
            this.Members.removeAll(this.SelectedUsers());
        }
    }
    LunchCommand.TeamMembersViewModel = TeamMembersViewModel;
    class TeamMembersController {
        constructor(page) {
            this.service = new LunchCommand.TeamMemberService();
            this.service.GetTeamMembers()
                .done((members) => {
                this.viewModel = new TeamMembersViewModel();
                this.viewModel.Members(members.Members.map(m => {
                    var vm = new TeamMemberViewModel();
                    vm.Id = m.UserId;
                    vm.IsAdmin(m.IsAdmin);
                    vm.Name(m.Name);
                    return vm;
                }));
                this.viewModel.save = (callback) => {
                    var model = {
                        Members: this.viewModel.Members().map(m => { return { Name: m.Name(), IsAdmin: m.IsAdmin(), UserId: m.Id }; })
                    };
                    this.service.SaveMembers(model).done(callback);
                };
                ko.applyBindings(this.viewModel, page.get(0));
            });
        }
    }
    LunchCommand.TeamMembersController = TeamMembersController;
})(LunchCommand || (LunchCommand = {}));
var LunchCommand;
(function (LunchCommand) {
    class UserDecisionViewModel {
        constructor(state, decisionCallback) {
            this.callback = decisionCallback;
            this.lunchIsNigh = ko.computed(() => state().Status === LunchCommand.LunchStatus.NotStarted);
            this.userHasDecided = ko.computed(() => state().UserDecision !== LunchCommand.UserDecision.Unanswered);
            this.userIsGoing = ko.computed(() => state().UserDecision === LunchCommand.UserDecision.Going);
        }
        decideYes() {
            this.callback(LunchCommand.UserDecision.Going);
        }
        decideNo() {
            this.callback(LunchCommand.UserDecision.NotGoing);
        }
    }
    LunchCommand.UserDecisionViewModel = UserDecisionViewModel;
})(LunchCommand || (LunchCommand = {}));
var LunchCommand;
(function (LunchCommand) {
    class UsersGoingViewModel {
        constructor(status) {
            this.status = status;
            this.status = status;
            this.UsersDecided = ko.computed(() => status().UsersToday.filter(ut => ut.Decision !== LunchCommand.UserDecision.Undecided)
                .map(ut => ({
                UserName: ut.UserName,
                IsGoing: ut.Decision === LunchCommand.UserDecision.Going,
                IsNotGoing: ut.Decision === LunchCommand.UserDecision.NotGoing,
                IsUndecided: ut.Decision === LunchCommand.UserDecision.Undecided
            })));
            this.Title = ko.computed(() => {
                var going = this.UsersDecided().filter(ud => ud.IsGoing).length;
                if (going != 1) {
                    return going + " users going today";
                }
                return "1 user going today";
            });
        }
    }
    LunchCommand.UsersGoingViewModel = UsersGoingViewModel;
})(LunchCommand || (LunchCommand = {}));
var LunchCommand;
(function (LunchCommand) {
    class WindowFocusListener {
        constructor() {
            this.focused = ko.observable(true);
            $(window).focus(() => { this.focused(true); });
            $(window).blur(() => { this.focused(false); });
        }
    }
    LunchCommand.WindowFocusListener = WindowFocusListener;
})(LunchCommand || (LunchCommand = {}));
