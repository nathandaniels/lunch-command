﻿using LunchCommand.Core.Models;
using LunchCommand.ViewModels;

namespace LunchCommand
{
    public interface ILunchService
    {
        LunchCommandViewModel GenerateViewModel(User user);
        LunchStateViewModel GetStatus(User user);
        void ProcessUserDecision(User user, UserDecision decision);
        void RehydrateAndProcessViewModel(ref LunchCommandViewModel vm);
    }
}