﻿using LunchCommand.Core.Models.Settings;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace LunchCommand.Services
{
    public class ReCaptchaService : IReCaptchaService
    {
        private ISettings settings;

        public ReCaptchaService(ISettings settings)
        {
            this.settings = settings;
        }

        public Boolean ValidateReCaptcha(HttpRequestBase request)
        {
            if (this.settings.EnableReCaptcha == false)
            {
                return true;
            }

            var response = request.Params[this.settings.GoogleReCaptchaQueryParamName];
            
            if (response == null)
            {
                return false;
            }

            var client = new WebClient();

            var query = HttpUtility.ParseQueryString(string.Empty);

            query.Add("secret", this.settings.GoogleReCaptchaSecretKey);
            query.Add("response", response);
            query.Add("remoteip", request.ServerVariables["REMOTE_ADDR"]);

            var queryString = query.ToString();

            var uri = String.Format("{0}?{1}", this.settings.GoogleReCaptchaUrl.ToString(), queryString);

            var json = client.DownloadString(uri);

            var jsonParsed = JObject.Parse(json);

            var valid = Boolean.Parse((String)jsonParsed["success"]);

            return valid;
        }
    }
}