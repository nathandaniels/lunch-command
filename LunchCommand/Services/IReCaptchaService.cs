﻿using System.Web;

namespace LunchCommand.Services
{
    public interface IReCaptchaService
    {
        bool ValidateReCaptcha(HttpRequestBase request);
    }
}