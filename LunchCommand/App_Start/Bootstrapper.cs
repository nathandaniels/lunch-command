﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using LunchCommand.Core.Models.Settings;
using LunchCommand.Data.Bootstrapping;
using LunchCommand.Services;
using StructureMap;
using StructureMap.Graph;
using StructureMap.Graph.Scanning;

namespace LunchCommand.App_Start
{
    internal class Bootstrapper
    {
        public IContainer IocContainer { get; }

        private Registry WebRegistry { get; }

        public Bootstrapper()
        {
            this.IocContainer = new Container();
            this.WebRegistry = new Registry();
        }

        public void Bootstrap(ISettings settings)
        {
            this.WebRegistry.Scan(scanner =>
            {
                scanner.AssemblyContainingType<Bootstrapper>();
                scanner.Convention<ControllerConvention>();
            });

            this.WebRegistry.For<ISettings>().Use(settings);
            this.WebRegistry.For<IMailSettings>().Use(settings.MailSettings);
            this.WebRegistry.For<ILunchService>().Use<LunchService>();
            this.WebRegistry.For<IReCaptchaService>().Use<ReCaptchaService>().Singleton();

            this.IocContainer.Configure(x =>
            {
                x.AddRegistry<CoreLunchRegistry>();
                x.AddRegistry(this.WebRegistry);
            });

            ControllerBuilder.Current.SetControllerFactory(typeof(InjectedControllerFactory));
        }

        private class ControllerConvention : IRegistrationConvention
        {
            public void ScanTypes(TypeSet types, Registry registry)
            {
                foreach(var type in types.FindTypes(TypeClassification.Concretes))
                {
                    if (!type.IsAbstract && typeof(IController).IsAssignableFrom(type) || typeof(ApiController).IsAssignableFrom(type))
                    {
                        registry.For(type).Use(type);
                    }
                }
            }
        }
    }
}