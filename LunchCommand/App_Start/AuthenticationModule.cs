﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LunchCommand.App_Start
{
    public class AuthenticationModule : IHttpModule
    {
        public void Init(HttpApplication context)
        {
            context.AuthenticateRequest += context_AuthenticateRequest;
        }

        void context_AuthenticateRequest(object appAsObject, EventArgs e)
        {
            var app = appAsObject as HttpApplication;

            
        }

        public void Dispose()
        {
        }
    }
}