﻿using System;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Routing;

namespace LunchCommand.App_Start
{
    internal class InjectedControllerFactory : DefaultControllerFactory, IHttpControllerActivator
    {
        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {
            var container = HttpContext.Current.ApplicationInstance.AsLcAppication().Container;
            return container.GetInstance(controllerType) as IHttpController;
        }

        public override IController CreateController(RequestContext requestContext, string controllerName)
        {
            var controllerType = this.GetControllerType(requestContext, controllerName);
            var container = requestContext.HttpContext.ApplicationInstance.AsLcAppication().Container;
            return container.GetInstance(controllerType) as IController;
        }
    }
}