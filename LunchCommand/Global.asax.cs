﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using LunchCommand.App_Start;
using LunchCommand.Models;
using LunchCommand.Core.Models.Settings;
using System.Web.Configuration;
using StructureMap;
using LunchCommand.Core.Models.Logging;

namespace LunchCommand
{
    public class LunchCommandApplication : HttpApplication
    {
        public IContainer Container
        {
            get
            {
                return HttpContext.Current.Items["container"] as IContainer;
            }
            set
            {
                HttpContext.Current.Items["container"] = value;
            }
        }

        private static IContainer ParentContainer { get; set; }

        private ISettings Settings { get; set; }

        public LunchCommandApplication()
        {
            this.BeginRequest += MvcApplication_BeginRequest;
            this.EndRequest += MvcApplication_EndRequest;
            this.Error += LunchCommandApplication_Error;
        }

        private void LunchCommandApplication_Error(object sender, EventArgs e)
        {
            var logger = ParentContainer.GetInstance<ILogger>();
            logger.Log(LogEntryType.Error, this.Server.GetLastError().ToString());
        }

        protected void Application_Start()
        {
            this.Settings = WebConfigSettings.CreateFrom(WebConfigurationManager.AppSettings);

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            
            var bootstrapper = new Bootstrapper();
            bootstrapper.Bootstrap(this.Settings);
            ParentContainer = bootstrapper.IocContainer;
        }

        void MvcApplication_EndRequest(object sender, EventArgs e)
        {
            this.Container.Dispose();
            this.Container = null;
        }

        void MvcApplication_BeginRequest(object sender, EventArgs e)
        {
            this.Container = ParentContainer.CreateChildContainer();
        }
    }

    public static class ApplicationExtensions
    {
        public static LunchCommandApplication AsLcAppication(this HttpApplication app)
        {
            return app as LunchCommandApplication;
        }
    }
}
