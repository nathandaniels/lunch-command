﻿using System;
using System.Threading.Tasks;
using LunchCommand.Core.Models;
using Microsoft.AspNet.Identity;

namespace LunchCommand.Data
{
    public class LunchCommandUserManager : UserManager<User, int>
    {
        [Obsolete]
        public LunchCommandUserManager(IUserStore<User, int> store) : base(store)
        { }

        public override Task<IdentityResult> CreateAsync(User user)
        {
            user.NotificationSettings = user.NotificationSettings ?? new UserNotificationSettings(user);
            return base.CreateAsync(user);
        }

        public override Task<IdentityResult> CreateAsync(User user, string password)
        {
            user.NotificationSettings = user.NotificationSettings ?? new UserNotificationSettings(user);
            return base.CreateAsync(user, password);
        }
    }
}