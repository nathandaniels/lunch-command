﻿using LunchCommand.Core.Models.Data;
using System.Linq;
using System.Collections.Generic;
using LunchCommand.Core.Models;
using System;

namespace LunchCommand.Data
{
    public class GenreRepository : IGenreRepository
    {
        private LunchCommandContext Context { get; set; }

        [Obsolete]
        public GenreRepository(LunchCommandContext context)
        {
            this.Context = context;
        }

        public IEnumerable<Genre> FindAll()
        {
            return this.Context.Genres.ToArray();
        }
    }
}