﻿using System;
using LunchCommand.Core.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace LunchCommand.Data
{
    public class LunchCommandUserStore : UserStore<User, Role, int, UserLogin, UserRole, UserClaim>
    {
        [Obsolete]
        public LunchCommandUserStore(LunchCommandContext context) : base(context)
        {}
    }
}