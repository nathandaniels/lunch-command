﻿using System;
using LunchCommand.Core.Models;
using LunchCommand.Core.Models.Data;
using System.Data.Entity;

namespace LunchCommand.Data
{
    public class VenueRepository : IVenueRepository
    {
        private LunchCommandContext Context { get; set; }

        [Obsolete]
        public VenueRepository(LunchCommandContext context)
        {
            this.Context = context;
        }

        public Venue FindById(Int32 id)
        {
            return this.Context.Venues.Find(id);
        }

        public void Save(Venue venue)
        {
            if (venue.VenueId > 0)
            {
                this.Context.Entry(venue).State = EntityState.Modified;
            }
            else
            {
                this.Context.Venues.Add(venue);
            }

            this.Context.SaveChanges();
        }
    }
}