﻿using System;
using LunchCommand.Core.Models.Logging;
using LunchCommand.Core.Models.Data;

namespace LunchCommand.Data.Logging
{
    public class DatabaseLogger : ILogger
    {
        private ILogRepository LogRepo { get; set; }

        [Obsolete]
        public DatabaseLogger(ILogRepository repo)
        {
            this.LogRepo = repo;
        }

        public void Log(LogEntryType entryType, string message)
        {
            this.LogRepo.SaveLogEntry(new LogEntry()
            {
                Message = message,
                Timestamp = DateTime.Now,
                Type = entryType
            });
        }
    }
}