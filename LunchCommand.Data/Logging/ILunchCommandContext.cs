﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using LunchCommand.Models;
using LunchCommand.Models.Logging;

namespace LunchCommand.Data
{
    public interface ILunchCommandContext
    {
        IEnumerable<Venue> ActiveVenues { get; }
        IDbSet<Genre> Genres { get; set; }
        IDbSet<LogEntry> LogEntries { get; set; }
        IDbSet<Lunch> Lunches { get; set; }
        IDbSet<User> Users { get; set; }
        IDbSet<Venue> Venues { get; set; }
    }
}
