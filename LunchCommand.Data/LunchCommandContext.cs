﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using LunchCommand.Core.Models;
using LunchCommand.Core.Models.Logging;
using Microsoft.AspNet.Identity.EntityFramework;

namespace LunchCommand.Data
{
    public class LunchCommandContext : IdentityDbContext<User, Role, int, UserLogin, UserRole, UserClaim>
    {
        public LunchCommandContext()
            : base("DefaultConnection")
        {

        }

        public IDbSet<Venue> Venues { get; set; }
        
        public IDbSet<Genre> Genres { get; set; }

        public IDbSet<Lunch> Lunches { get; set; }

        public IDbSet<LogEntry> LogEntries { get; set; }

        public IDbSet<Team> Teams { get; set; }

        public IDbSet<Invite> Invites { get; set; }

        public IDbSet<Proposition> Propositions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Entity<UserLogin>().HasKey(l => l.UserId);
            modelBuilder.Entity<Role>().HasKey(r => r.Id);
            modelBuilder.Entity<UserRole>().HasKey(r => new { r.RoleId, r.UserId });
        }
    }
}