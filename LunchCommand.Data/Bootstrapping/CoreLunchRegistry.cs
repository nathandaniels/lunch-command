﻿using System.Linq;
using StructureMap;
using Microsoft.AspNet.Identity.EntityFramework;
using LunchCommand.Core.Models;
using StructureMap.Graph;
using StructureMap.Graph.Scanning;
using Microsoft.AspNet.Identity;
using LunchCommand.Core.Services.Maps;
using LunchCommand.Core.Services.Mail;
using LunchCommand.Core.Models.Logging;
using LunchCommand.Data.Logging;
using LunchCommand.Core.Services;
using LunchCommand.Core.Services.Notifications;
using LunchCommand.Core.Data.Services.Maps;
using LunchCommand.Core.Data.Services.Mail;

namespace LunchCommand.Data.Bootstrapping
{
    public class CoreLunchRegistry : Registry, IRegistrationConvention
    {
        public CoreLunchRegistry()
        {
            this.Scan(
                scanner =>
                {
                    scanner.AssemblyContainingType<CoreLunchRegistry>();
                    scanner.AssemblyContainingType<User>();
                    scanner.Exclude(t => t.Name.Contains("Repository"));
                    scanner.WithDefaultConventions();
                });

            this.Scan(scanner =>
            {
                scanner.TheCallingAssembly();
                scanner.With(this);
            });

            this.For<UserStore<User, Role, int, UserLogin, UserRole, UserClaim>>().Use<LunchCommandUserStore>().ContainerScoped();
            this.For<IUserStore<User, int>>().Use<LunchCommandUserStore>().ContainerScoped();
            this.ForConcreteType<LunchCommandUserManager>().Configure.ContainerScoped();
            this.For<LunchCommandContext>().ContainerScoped();
            this.For<ILogger>().Use<DatabaseLogger>();

            this.For<IMapService>().Use<BingMapsService>().Singleton();
            this.For<IMailService>().Use<MailService>().ContainerScoped();
            this.For<INotificationMemory>().Use<NotificationMemory>().Singleton();
        }

        public void ScanTypes(TypeSet types, Registry registry)
        {
            foreach (var type in types.FindTypes(TypeClassification.Concretes).Where(t => t.Name.EndsWith("Repository")))
            {
                foreach (var @interface in type.GetInterfaces())
                {
                    registry.For(@interface).Use(type).ContainerScoped();
                }
            }
        }
    }
}
