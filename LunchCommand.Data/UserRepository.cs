﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using LunchCommand.Core.Models;
using LunchCommand.Core.Models.Data;

namespace LunchCommand.Data
{
    public class UserRepository: IUserRepository
    {
        private LunchCommandContext Context { get; set; }

        [Obsolete]
        public UserRepository(LunchCommandContext context)
        {
            this.Context = context;
        }

        public User FindById(Int32 id)
        {
            return this.Context.Users.Find(id);
        }

        public User FindByEmail(String emailAddress)
        {
            return this.Context.Users.FirstOrDefault(u => u.Email.Equals(emailAddress));
        }

        public User Update(User user)
        {
            this.Context.Entry(user).State = System.Data.Entity.EntityState.Modified;
            try
            {
                this.Context.SaveChanges();

            }
            catch (Exception e)
            {
                throw e;
            }

            return user;
        }

        public IEnumerable<User> FindAllActiveWithEmailNotifications()
        {
            return this.Context.Users.Where(
                u => u.NotificationSettings.Medium == NotificationMedium.Email
                && u.TeamId != null
                && DbFunctions.DiffHours(u.Team.LunchStartTimeUtc, DateTime.UtcNow.TimeOfDay) >= 0);
        }
    }
}