﻿using LunchCommand.Core.Models.Logging;
using LunchCommand.Core.Models.Data;
using System;

namespace LunchCommand.Data
{
    public class LogRepository: ILogRepository
    {
        private LunchCommandContext Context { get; set; }

        [Obsolete]
        public LogRepository(LunchCommandContext context)
        {
            this.Context = context;
        }

        public void SaveLogEntry(LogEntry entry)
        {
            this.Context.LogEntries.Add(entry);
            this.Context.SaveChanges();
        }
    }
}