﻿using System;
using System.Linq;
using LunchCommand.Core.Models;
using LunchCommand.Core.Models.Data;
using System.Data.Entity;

namespace LunchCommand.Data
{
    public class PropositionRepository : IPropositionRepository
    {
        private LunchCommandContext Context { get; set; }

        [Obsolete]
        public PropositionRepository(LunchCommandContext context)
        {
            this.Context = context;
        }
        
        public void Save(Proposition prop)
        {
            if (prop.PropositionId <= 0)
            {
                this.Context.Propositions.Add(prop);
            }
            else
            {
                try {
                    this.Context.Entry(prop).State = EntityState.Modified;
                    this.Context.SaveChanges();
                }catch(Exception e)
                {
                    var x = e;
                }
            }
        }

        public void Delete(Proposition proposition)
        {
            this.Context.Propositions.Remove(proposition);
            this.Context.SaveChanges();
        }
    }
}