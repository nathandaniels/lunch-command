﻿using System;
using LunchCommand.Core.Models;
using LunchCommand.Core.Services.Notifications;

namespace LunchCommand.Core.Data.Services.Notifications
{
    internal class NotificationMemento: INotificationMemento
    {
        public NotificationMemento(User user, Lunch lunch)
        {
            this.UserId = user.Id;
            this.LunchTime = lunch.LunchTimeUtc;
        }

        protected Int32 UserId { get; set; }

        public DateTime LunchTime { get; protected set; }

        public Int32 LunchId { get; protected set; }

        public override bool Equals(object obj)
        {
            return obj?.GetHashCode() == this.GetHashCode();
        }

        public override int GetHashCode()
        {
            return (this.UserId << 16) + this.LunchId;
        }
    }
}
