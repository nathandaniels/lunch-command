﻿using System;
using System.Collections.Generic;
using LunchCommand.Core.Models;
using LunchCommand.Core.Models.Data;
using LunchCommand.Core.Models.Logging;
using LunchCommand.Core.Models.Settings;
using LunchCommand.Core.Services;
using LunchCommand.Core.Services.Notifications;

namespace LunchCommand.Core.Data.Services.Notifications
{
    /// <summary>
    /// Handles notifications
    /// </summary>
    public class BrowserNotificationsService : BaseNotificationsService, IBrowserNotificationsService
    {
        private INotificationMemory Memory { get; }

        /// <summary>
        /// Creates a new notification service.
        /// </summary>
        /// <param name="memory">The notification memory</param>
        /// <param name="propositionService">The proposition service</param>
        /// <param name="repo">The data gateway</param>
        /// <param name="settings">The settings</param>
        /// <param name="logger">The logger</param>
        public BrowserNotificationsService(
            INotificationMemory memory,
            IPropositionService propositionService,
            ILunchRepository repo,
            ISettings settings,
            ILogger logger)
            : base(memory, propositionService, repo, settings, logger)
        {
            this.Memory = memory;
        }

        /// <summary>
        /// Gets the notifications for a specific user. 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Notification> GetBrowserNotifications(User user)
        {
            if (user.NotificationSettings.Medium.HasFlag(NotificationMedium.Browser))
            {
                return base.GetNotifications(user, NotificationMedium.Browser);
            }
            else
            {
                return new Notification[0];
            }
        }
    }
}
