﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchCommand.Core.Models;
using LunchCommand.Core.Models.Data;
using LunchCommand.Core.Models.Logging;
using LunchCommand.Core.Models.Settings;
using LunchCommand.Core.Services;
using LunchCommand.Core.Services.Notifications;

namespace LunchCommand.Core.Data.Services.Notifications
{
    /// <summary>
    /// Handles notifications
    /// </summary>
    public class BaseNotificationsService : BaseService
    {
        /// <summary>
        /// The memory
        /// </summary>
        private INotificationMemory Memory { get; }

        /// <summary>
        /// The proposition service used to get information on propositions for todays lunch
        /// </summary>
        private IPropositionService PropositionService { get; }

        /// <summary>
        /// Creates a new notification service.
        /// </summary>
        /// <param name="memory">The notification memory</param>
        /// <param name="propositionService">The proposition service</param>
        /// <param name="repo">The data gateway</param>
        /// <param name="settings">The settings</param>
        /// <param name="logger">The logger</param>
        public BaseNotificationsService(
            INotificationMemory memory,
            IPropositionService propositionService,
            ILunchRepository repo,
            ISettings settings,
            ILogger logger)
            : base(repo, settings, logger)
        {
            this.Memory = memory;
            this.PropositionService = propositionService;
        }

        /// <summary>
        /// Gets the notifications for a specific user.  Also causes 
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<Notification> GetNotifications(User user, NotificationMedium medium)
        {
            var settings = user.NotificationSettings;

            if (settings.NotificationsEnabled)
            {
                Lunch lunch;
                var props = this.PropositionService.GetPropositions(user.Team, out lunch);
               
                var nowUtc = DateTime.UtcNow;
                var diff = (lunch.LunchTimeUtc - nowUtc).TotalMinutes;
                var memento = new NotificationMemento(user, lunch);
                var decided = (lunch.Users.FirstOrDefault(u => u.UserId == user.Id)?.Decision);

                if (user.NotificationSettings.NotifyNotDecided
                    && diff <= settings.MinutesToNotifyOfIndecision
                    && diff > 0 // Don't also notify after lunch has decided
                    && !this.Memory.HasNotificationBeenSent(NotificationType.NotDecided, medium, memento)
                    && decided == null)
                {
                    this.Memory.RegisterNotificationSent(NotificationType.NotDecided, medium, memento);
                    yield return new Notification(NotificationType.NotDecided, "You have not yet decided if you will be going to lunch today.");
                }
                /* If we are to notify of the start of lunch and lunch time is 
                 * in the past (but not more than 5 minutes) and we haven't 
                 * notified already AND the user has either decided to go or 
                 * they haven't decided and want to be told anyway
                 */
                else if (user.NotificationSettings.NotifyLunchStarted
                    && diff <= 0
                    && diff > -5
                    && !this.Memory.HasNotificationBeenSent(NotificationType.LunchStarted, medium, memento)
                    && (decided == true || (settings.NotifyLunchStartedEvenWhenNotDecided && decided == null)))
                {
                    this.Memory.RegisterNotificationSent(NotificationType.LunchStarted, medium, memento);

                    var proposedVenueGoing = this.PropositionService.GetProposedVenueUserIsGoingTo(lunch, user.Id);
                    if (proposedVenueGoing != null)
                    {
                        yield return new Notification(NotificationType.LunchStarted, $"Lunch time!  You are going to: {proposedVenueGoing.Name}");
                    }

                    if (lunch.Venue == null)
                    {
                        yield return new Notification(NotificationType.LunchStarted, $"Lunch consensus could not be reached.");
                    }
                    else
                    {
                        yield return new Notification(NotificationType.LunchStarted, $"Lunch has been decided: {lunch.Venue.Name}");
                    }

                }

                // Propositions
                if (settings.NotifyVenueProposed)
                {
                    foreach (var proposition in props)
                    {
                        memento = new PropositionNotificationMemento(proposition.Venue, proposition.ProposedBy, user, lunch);

                        if (!this.Memory.HasNotificationBeenSent(NotificationType.NewProposition, medium, memento)
                            && user.Id != proposition.ProposerId)
                        {
                            this.Memory.RegisterNotificationSent(NotificationType.NewProposition, medium, memento);

                            yield return new Notification(NotificationType.NewProposition, $"{proposition.ProposedBy.FirstName} has proposed {proposition.Venue.Name} for lunch today.")
                            {
                                PropositionId = proposition.PropositionId
                            };
                        }
                    }
                }
            }
        }
    }
}
