﻿using System;
using LunchCommand.Core.Models;

namespace LunchCommand.Core.Data.Services.Notifications
{
    internal class PropositionNotificationMemento : NotificationMemento
    {
        public PropositionNotificationMemento(Venue venue, User proposer, User user, Lunch lunch) : base(user, lunch)
        {
            this.ProposedVenueId = venue.VenueId;
            this.ProposingUserId = proposer.Id;
        }

        protected Int32 ProposedVenueId { get; set; }

        protected Int32 ProposingUserId { get; set; }

        public override int GetHashCode()
        {
            return new { allurbase = base.GetHashCode(), this.ProposedVenueId, this.ProposingUserId }.GetHashCode();
        }
    }
}
