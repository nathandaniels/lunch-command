﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using LunchCommand.Core.Models;
using LunchCommand.Core.Models.Data;
using LunchCommand.Core.Models.Logging;
using LunchCommand.Core.Models.Settings;
using LunchCommand.Core.Services;
using LunchCommand.Core.Services.Mail;
using LunchCommand.Core.Services.Notifications;

namespace LunchCommand.Core.Data.Services.Notifications
{
    /// <summary>
    /// Handles notifications
    /// </summary>
    public class EmailNotificationsService : BaseNotificationsService, IEmailNotificationService
    {
        private INotificationMemory Memory { get; }

        private IMailService MailService { get; }

        private Task PollingTask { get; set; }

        private CancellationTokenSource CancelationTokenSource { get; }

        private IUserRepository UserRepository { get; }

        /// <summary>
        /// Creates a new notification service.
        /// </summary>
        /// <param name="memory">The notification memory</param>
        /// <param name="propositionService">The proposition service</param>
        /// <param name="repo">The data gateway</param>
        /// <param name="userRepository">The user repository</param>
        /// <param name="settings">The settings</param>
        /// <param name="logger">The logger</param>
        /// <param name="mailService">The mail service</param>
        public EmailNotificationsService(
            INotificationMemory memory,
            IPropositionService propositionService,
            ILunchRepository repo,
            IUserRepository userRepo,
            ISettings settings,
            ILogger logger,
            IMailService mailService)
            : base(memory, propositionService, repo, settings, logger)
        {
            this.Memory = memory;
            this.MailService = mailService;
            this.CancelationTokenSource = new CancellationTokenSource();
        }

        public void Begin()
        {
            var token = this.CancelationTokenSource.Token;
            this.PollingTask = new Task(() =>
            {
                while (!token.IsCancellationRequested)
                {
                    Thread.Sleep(new TimeSpan(0, 1, 0));
                    this.Process();
                }

            }, token);
        }

        private void Process()
        {
            //var notifications = 
        }

        /// <summary>
        /// Gets the notifications for a specific user.  Also causes 
        /// </summary>
        /// <returns></returns>
        private IEnumerable<Notification> GetEmailNotifications(User user)
        {
            if (user.NotificationSettings.Medium.HasFlag(NotificationMedium.Browser))
            {
                return base.GetNotifications(user, NotificationMedium.Browser);
            }
            else
            {
                return new Notification[0];
            }
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
