﻿using LunchCommand.Core.Models;
using LunchCommand.Core.Models.Data;
using LunchCommand.Core.Models.Logging;
using LunchCommand.Core.Models.Settings;
using LunchCommand.Core.Services.Mail;
using System;
using System.Collections.Generic;
using LunchCommand.Core.Services;

namespace LunchCommand.Core.Data.Services
{
    public class InvitationService : BaseService, IInvitationService
    {
        private IMailService mailService;
        
        private IInviteRepository inviteRepository;

        /// <summary>
        /// Creates a  new invitation service
        /// </summary>
        /// <param name="settings">The settings</param>
        /// <param name="logger"></param>
        public InvitationService(
            ISettings settings, 
            ILunchRepository lunchRepository,
            IInviteRepository inviteRepository,
            IMailService mailService,
            ILogger logger) : base(lunchRepository, settings, logger)
        {
            this.inviteRepository = inviteRepository;
            this.mailService = mailService;
        }

        public void SendInvites(IEnumerable<String> recipients, User sender, String urlForRegisterPage)
        {
            var inviteRepo = this.inviteRepository;

            foreach (var recipient in recipients)
            {
                // create the invitation
                var invite = inviteRepo.Generate(sender);

                this.Logger.Log(LogEntryType.Info, String.Format("Sending invite {0} to {1} on behalf of {2}", invite.InviteCode, recipient, sender.UserName));

                var messageBody = String.Format("<div style=\"font-family: 'Helvetica; padding: 20px\">Greetings! <p>{0} has invited you to join {1} on Lunch Command.</p>" +
                "<p><a href='{2}?inviteCode={3}&emailAddress={4}'>Click here</a> to register.  Alternatively, go to {2} and use the invite code: <h3>{3}</h3></p></div>" +
                "<p>Happy lunches!</p>", sender.FullName, sender.Team.Name, urlForRegisterPage, invite.InviteCode, Uri.EscapeUriString(recipient));

                var subject = String.Format("Lunch Command invitation from {0}", sender.FullName);

                this.mailService.SendMessageAsync(
                    sender.FullName,
                    this.Settings.MailSettings.InvitationFromAddress,
                    recipient,
                    subject,
                    messageBody);
            }
        }
    }
}
