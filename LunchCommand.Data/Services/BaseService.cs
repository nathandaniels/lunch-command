﻿using LunchCommand.Core.Models;
using LunchCommand.Core.Models.Data;
using LunchCommand.Core.Models.Logging;
using LunchCommand.Core.Models.Settings;
using System;

namespace LunchCommand.Core.Data.Services
{
    public abstract class BaseService
    {
        /// <summary>
        /// Absolutely makes sure there can't be more than one lunch created at one time
        /// </summary>
        private static object lunchCreationLock = new object();

        public BaseService(ILunchRepository lunchRepository, ISettings settings, ILogger logger)
        {
            this.Settings = settings;
            this.LunchRepository = lunchRepository;
            this.Logger = logger;
        }

        protected ISettings Settings {get; set; }

        protected ILunchRepository LunchRepository { get; set; }

        protected ILogger Logger { get; set; }


        /// <summary>
        /// Gets the lunch of the day or creates it if it doesn't already exist
        /// </summary>
        /// <returns>Lunch</returns>
        protected Lunch GetOrCreateLunch(Team team)
        {
            lock (lunchCreationLock)
            {
                var lunch = this.LunchRepository.FindLunchOfDay(DateTime.UtcNow, team);

                if (lunch == null)
                {
                    var time = team.LunchStartTimeUtc;

                    lunch = new Lunch()
                    {
                        LunchTimeUtc = DateTime.UtcNow.Date + time,
                        TeamId = team.TeamId
                    };

                    this.LunchRepository.Save(lunch);
                }

                return lunch;
            }
        }
    }
}
