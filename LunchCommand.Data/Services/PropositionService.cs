﻿using LunchCommand.Core.Models.Logging;
using LunchCommand.Core.Models.Data;
using LunchCommand.Core.Models.Settings;
using System.Collections.Generic;
using LunchCommand.Core.Models;
using System;
using System.Linq;
using LunchCommand.Core.Services;

namespace LunchCommand.Core.Data.Services
{
    public sealed class PropositionService : BaseService, IPropositionService
    {
        private readonly IPropositionRepository propositionRepository;

        private readonly IUserRepository userRepository;

        /// <summary>
        /// Constructor of the service
        /// </summary>        
        /// <param name="settings">The settings</param>
        /// <param name="logger">The logger</param>
        public PropositionService(
            ILunchRepository lunchRepository, 
            IPropositionRepository propositionRepository, 
            IUserRepository userRepository,
            ISettings settings, 
            ILogger logger) : base(lunchRepository, settings, logger)
        {
            this.propositionRepository = propositionRepository;
            this.userRepository = userRepository;
        }

        /// <summary>
        /// Gets all of the propositions for the current lunch for the porovided team
        /// </summary>
        /// <param name="team">The team</param>
        /// <param name="lunch">Today's lunch</param>
        /// <returns>The propositions</returns>
        public IEnumerable<Proposition> GetPropositions(Team team, out Lunch lunch)
        {
            lunch = this.GetOrCreateLunch(team);
            return lunch.Propositions;
        }

        public void CreateProposition(User user, Int32 venueId)
        {
            var lunch = this.GetOrCreateLunch(user.Team);

            // Make sure the user is not already in a group
            if (this.GetProposedVenueUserIsGoingTo(lunch, user.Id) != null)
            {
                this.Logger.Log(LogEntryType.Warn, String.Format("{0} is attempting to propose a venue but is already going to one", user.UserName));
                return;
            }

            // Make sure this venue is in the team            
            if (!user.Team.Venues.Any(v => v.VenueId == venueId))
            {
                this.Logger.Log(LogEntryType.Warn, String.Format("{0} is attempting to propose a venue ({1}) not on their team!", user.UserName, venueId));
                return;
            }

            var existingProp = lunch.Propositions.FirstOrDefault(p => p.VenueId == venueId);

            if (existingProp != null)
            {
                existingProp.Followers.Add(new PropositionFollowing() { User = user, Proposition = existingProp });
                this.propositionRepository.Save(existingProp);
            }
            else
            {
                var newProp = new Proposition()
                {
                    Lunch = lunch,
                    Followers = new List<PropositionFollowing>(),
                    ProposedBy = user,
                    VenueId = venueId
                };

                lunch.Propositions.Add(newProp);
                this.LunchRepository.Save(lunch);
            }
        }

        /// <summary>
        /// Gets the proposed venue that the specified user is going to.
        /// </summary>
        /// <param name="lunch">The lunch in question</param>
        /// <param name="userId">The id of the user</param>
        /// <returns>The venue or <c>null</c> if the user is not going following any proposition.</returns>
        public Venue GetProposedVenueUserIsGoingTo(Lunch lunch, Int32 userId)
        {
            return lunch.Propositions.FirstOrDefault(p => p.Followers.Select(pf => pf.User).Union(new[] { p.ProposedBy }).Any(u => u.Id == userId))?.Venue;
        }

        /// <summary>
        /// Adds a user to a proposed lunch group
        /// </summary>
        /// <param name="user"></param>
        /// <param name="propositionId"></param>
        public void AddUserToFollowing(User user, Int32 propositionId)
        {
            var lunch = this.GetOrCreateLunch(user.Team);

            var prop = lunch.Propositions.FirstOrDefault(p => p.PropositionId == propositionId);

            if (prop == null)
            {
                this.Logger.Log(LogEntryType.Warn, String.Format("{0} is attempting to join a proposed lunch that is not on their team ({1})", user.UserName, propositionId));
                return;
            }

            this.RemoveUserFromAllPropositions(user, lunch);
            
            // Ensure this user is in this lunch
            { 
                var pastDecision = user.LunchesDecided.FirstOrDefault(l => l.Lunch.LunchId == lunch.LunchId);
                if (pastDecision == null)
                {
                    user.LunchesDecided.Add(new LunchUser()
                    {
                        Decision = true,
                        Lunch = lunch,
                        User = user
                    });
                }
                else
                {
                    pastDecision.Decision = true;
                }

                this.userRepository.Update(user);
            }

            prop.Followers.Add(new PropositionFollowing() { Proposition = prop, User = user });
            this.propositionRepository.Save(prop);
        }

        /// <summary>
        /// Gets all of the venues that the user does not hate and that are not currenly being proposed
        /// </summary>
        /// <param name="user">The user</param>
        /// <returns>The venues</returns>
        public IEnumerable<Venue> GetVenuesNotBeingProposed(User user)
        {
            var lunch = this.GetOrCreateLunch(user.Team);

            return user.VenueDecisions.Where(v =>v.Degree != DegreeOfPreference.WillNotGo && !lunch.Propositions.Any(p => p.VenueId == v.VenueId)).Select(v=>v.Venue).OrderBy(v=>v.Name);
        }

        public void RemoveUserFromAllPropositions(User user, Lunch lunch)
        {
            // Remove them from any other group they are following (there should never be more than one but we handle multiple just in case
            var alreadyFollowing = lunch.Propositions.Where(p => p.Followers.Any(f => f.UserId == user.Id));

            foreach (var af in alreadyFollowing)
            {
                this.RemoveUserFromFollowing(user, af.PropositionId, lunch);
            }

            // Remove them from any other group they are leading (there should never be more than one but we handle multiple just in case
            var leading = lunch.Propositions.Where(p => p.ProposerId == user.Id).ToArray();

            foreach (var l in leading)
            {
                this.RemoveUserFromFollowing(user, l.PropositionId, lunch);
            }
        }

        public void RemoveUserFromFollowing(User user, Int32 propositionId)
        {
            this.RemoveUserFromFollowing(user, propositionId, this.GetOrCreateLunch(user.Team));
        }

        private void RemoveUserFromFollowing(User user, Int32 propositionId, Lunch lunch)
        {
            var proposition = lunch.Propositions.First(p => p.PropositionId == propositionId);

            if (proposition.ProposerId == user.Id)
            {
                proposition.ProposedBy = null;

                // If there are no more followers, remove this proposition
                if (!proposition.Followers.Any())
                {
                    this.propositionRepository.Delete(proposition);
                }
                else
                {
                    // otherwise, promote one of the followers to the leader
                    var topFollower = proposition.Followers.First();
                    proposition.ProposedBy = topFollower.User;
                    proposition.Followers.Remove(topFollower);
                    this.propositionRepository.Save(proposition);
                }
            }
            else if (proposition.Followers.Any(f=>f.UserId == user.Id))
            {
                proposition.Followers = proposition.Followers.Where(f => f.UserId != user.Id).ToList();
                this.propositionRepository.Save(proposition);

            }
        }
    }
}