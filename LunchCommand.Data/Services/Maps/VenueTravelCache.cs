﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LunchCommand.Core.Models;
using System.Collections.Concurrent;
using LunchCommand.Core.Services.Maps;

namespace LunchCommand.Core.Data.Services.Maps
{
    /// <summary>
    /// Caches the travel times for venues so that we don't spam Bing's servers
    /// </summary>
    public static class VenueTravelCache
    {
        private static ConcurrentDictionary<Int32, VenueTravelTime> cache = new ConcurrentDictionary<Int32, VenueTravelTime>();

        public static VenueTravelTime GetTravelTime(Lunch forLunch, Venue venue, String startAddress, IMapService service)
        {
            // Don't bother checking if we're an hour past lunch already
            if (forLunch.LunchTimeUtc > DateTime.UtcNow.Date.AddHours(1))
            {
                return new VenueTravelTime()
                {
                    Obtained = DateTime.Now
                };
            }

            var time = cache.GetOrAdd(venue.VenueId, x =>
            {
                return new VenueTravelTime()
                    {
                        TravelTime = service.GetTravelTime(startAddress, venue.Address),
                        Obtained = DateTime.Now
                    };
            });

            if ((DateTime.Now - time.Obtained).TotalMinutes >= 10)
            {
                time = cache[venue.VenueId] = new VenueTravelTime()
                {
                    TravelTime = service.GetTravelTime(startAddress, venue.Address),
                    Obtained = DateTime.Now
                };
            }

            return time;
        }
    }
}