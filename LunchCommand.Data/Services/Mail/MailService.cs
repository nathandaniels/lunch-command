﻿using System;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using LunchCommand.Core.Models.Settings;
using LunchCommand.Core.Services.Mail;

namespace LunchCommand.Core.Data.Services.Mail
{
    public sealed class MailService : IMailService
    {
        private IMailSettings mailSettings;

        private SmtpClient mailClient;

        [Obsolete]
        public MailService(IMailSettings mailSettings)
        {
            this.mailSettings = mailSettings;
            this.mailClient = new SmtpClient(mailSettings.SmtpServer, mailSettings.SmtpPort);
            mailClient.Credentials = new NetworkCredential(this.mailSettings.SmtpUserName, this.mailSettings.SmtpPassword);
        }

        public Task SendMessageAsync(String fromName, String fromAddress, String to, String subject, String body)
        {
            var message = new MailMessage(String.Format("{0} <{1}>", fromName, fromAddress), to, subject, body);
            message.IsBodyHtml = true;
            this.mailClient.Send(message);
            return null;
        }

        public void Dispose()
        {
            this.mailClient.Dispose();
        }
    }
}
