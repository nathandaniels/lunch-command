﻿using System;
using System.Data.Entity;
using System.Linq;
using LunchCommand.Core.Models;
using LunchCommand.Core.Models.Data;

namespace LunchCommand.Data
{
    public class LunchRepository : ILunchRepository
    {
        private LunchCommandContext Context { get; set; }

        [Obsolete]
        public LunchRepository(LunchCommandContext context)
        {
            this.Context = context;
        }

        public Lunch FindById(Int32 id)
        {
            return this.Context.Lunches.Find(id);
        }

        public Lunch FindLunchOfDay(DateTime date, Team team)
        {
            // Gets the kunch that is no more than 12 hours in the future and no more than 12 hours in the past
            return this.Context.Lunches.FirstOrDefault(l =>
                DbFunctions.DiffHours(l.LunchTimeUtc, DateTime.UtcNow) >= -12
                && DbFunctions.DiffHours(l.LunchTimeUtc, DateTime.UtcNow) < 12
                && l.TeamId == team.TeamId);
        }

        public void Save(Lunch lunch)
        {
            if (lunch.LunchId > 0)
            {
                this.Context.Entry(lunch).State = EntityState.Modified;
            }
            else
            {
                this.Context.Lunches.Add(lunch);
            }

            this.Context.SaveChanges();
        }
    }
}