﻿using System;
using System.Data.Entity;
using LunchCommand.Core.Models;

namespace LunchCommand.Data
{
    internal class LunchCommandContextInitializer : CreateDatabaseIfNotExists<LunchCommandContext>
    {
        protected override void Seed(LunchCommandContext context)
        {
            base.Seed(context);

            context.Genres.Add("Burgers");
            context.Genres.Add("BBQ");
            context.Genres.Add("American Misc.");
            context.Genres.Add("Italian");
            context.Genres.Add("Mexican");
            context.Genres.Add("Chinese");
            context.Genres.Add("Japanese");
            context.Genres.Add("Brazilian");
            context.Genres.Add("Vietnamese");
            context.Genres.Add("Mediterranean");
            context.Genres.Add("German");
            context.Genres.Add("English / Irish");
            context.Genres.Add("Indian");
            context.Genres.Add("Others");
            context.Genres.Add("Cuban");
            
            context.SaveChanges();
        }
    }
}