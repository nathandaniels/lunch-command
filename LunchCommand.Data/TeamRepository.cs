﻿using System;
using System.Linq;
using LunchCommand.Core.Models;
using LunchCommand.Core.Models.Data;
using System.Data.Entity;

namespace LunchCommand.Data
{
    public class TeamRepository : ITeamRepository
    {
        private LunchCommandContext Context { get; set; }

        [Obsolete]
        public TeamRepository(LunchCommandContext context)
        {
            this.Context = context;
        }

        public Team FindByName(String name)
        {
            return this.Context.Teams.FirstOrDefault(t => t.Name == name);
        }

        public Team FindById(Int32 id)
        {
            return this.Context.Teams.Find(id);
        }

        public void Add(Team team)
        {
            this.Context.SaveChanges();
        }

        public void Save(Team team)
        {
            if (team.TeamId <= 0)
            {
                this.Context.Teams.Add(team);
            }
            else
            {
                this.Context.Entry(team).State = EntityState.Modified;
                this.Context.SaveChanges();
            }
        }
    }
}