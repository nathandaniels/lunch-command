﻿using System;
using System.Linq;
using LunchCommand.Core.Models;
using LunchCommand.Core.Models.Data;

namespace LunchCommand.Data
{
    public class InviteRepository: IInviteRepository
    {
        private LunchCommandContext Context { get; set; }

        [Obsolete]
        public InviteRepository(LunchCommandContext context)
        {
            this.Context = context;
        }

        public Invite FindByCode(String code)
        {
            return this.Context.Invites.FirstOrDefault(i => i.InviteCode == code);
        }

        public Invite Generate(User user)
        {
            var code = Guid.NewGuid().ToString("N");

            var invite = new Invite()
            {
                Team = user.Team,
                InviteCode = code,
                CreatingUser = user
            };

            this.Context.Invites.Add(invite);
            this.Context.SaveChanges();

            return invite;
        }

        public Invite Generate(Team team)
        {
            var code = Guid.NewGuid().ToString("N");

            var invite = new Invite()
            {
                Team = team,
                InviteCode = code,
            };

            this.Context.Invites.Add(invite);
            this.Context.SaveChanges();

            return invite;
        }

        public void Delete(Invite invite)
        {
            this.Context.Invites.Remove(invite);
            this.Context.SaveChanges();
        }
    }
}