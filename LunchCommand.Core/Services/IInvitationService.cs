﻿using System.Collections.Generic;
using LunchCommand.Core.Models;

namespace LunchCommand.Core.Services
{
    public interface IInvitationService
    {
        void SendInvites(IEnumerable<string> recipients, User sender, string urlForRegisterPage);
    }
}