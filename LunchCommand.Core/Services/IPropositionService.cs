﻿using System;
using System.Collections.Generic;
using LunchCommand.Core.Models;

namespace LunchCommand.Core.Services
{
    public interface IPropositionService
    {
        void AddUserToFollowing(User user, int propositionId);
        void CreateProposition(User user, int venueId);
        IEnumerable<Proposition> GetPropositions(Team team, out Lunch lunch);
        IEnumerable<Venue> GetVenuesNotBeingProposed(User user);
        void RemoveUserFromAllPropositions(User user, Lunch lunch);
        void RemoveUserFromFollowing(User user, int propositionId);

        /// <summary>
        /// Gets the proposed venue that the specified user is going to.
        /// </summary>
        /// <param name="lunch">The lunch in question</param>
        /// <param name="userId">The id of the user</param>
        /// <returns>The venue or <c>null</c> if the user is not going following any proposition.</returns>
        Venue GetProposedVenueUserIsGoingTo(Lunch lunch, Int32 userId);

    }
}