﻿using System;
using System.Threading.Tasks;

namespace LunchCommand.Core.Services.Mail
{
    public interface IMailService: IDisposable
    {
        Task SendMessageAsync(String fromName, String fromAddress, String to, String subject, String body);
    }
}
