﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LunchCommand.Core.Models;
using LunchCommand.Core.Models.Logging;

namespace LunchCommand.Core.Services.Notifications
{
    public class NotificationMemory : INotificationMemory
    {
        private IDictionary<NotificationType, IDictionary<NotificationMedium, ConcurrentDictionary<INotificationMemento, Byte>>> Memory { get; }

        private Task CleanupTask { get; }

        private CancellationToken CleanupCancel { get; }

        private ILogger Logger { get; }

        public NotificationMemory(ILogger logger)
        {
            this.Memory = new Dictionary<NotificationType, IDictionary<NotificationMedium, ConcurrentDictionary<INotificationMemento, Byte>>>();
            this.Logger = logger;

            foreach (var type in Enum.GetValues(typeof(NotificationType)).Cast<NotificationType>())
            {
                this.Memory[type] = new Dictionary<NotificationMedium, ConcurrentDictionary<INotificationMemento, Byte>>();

                foreach (var medium in Enum.GetValues(typeof(NotificationMedium)).Cast<NotificationMedium>())
                {
                    this.Memory[type][medium] = new ConcurrentDictionary<INotificationMemento, byte>();
                }
            }

            this.CleanupCancel = new CancellationTokenSource().Token;
            this.CleanupTask = new Task(() =>
            {
                var interval = (int)new TimeSpan(12, 0, 0).TotalSeconds;
                var waitTime = new TimeSpan(0, 0, 1);

                var x = interval;
                while (!this.CleanupCancel.IsCancellationRequested)
                {
                    Thread.Sleep(waitTime);
                    if (x-- <= 0)
                    {
                        this.Cleanup();
                        x = interval;
                    }
                }
            }, this.CleanupCancel);

            this.CleanupTask.Start();
        }

        /// <summary>
        /// Starts a task that will clean out old entries in the notification memory banks.
        /// </summary>
        private void Cleanup()
        {
            foreach (var val in this.Memory)
            {
                foreach (var kvp in val.Value)
                {
                    foreach (var oldEntry in kvp.Value.Keys.Where(m => m.LunchTime < DateTime.UtcNow.AddDays(-2)).ToArray())
                    {
                        this.Logger.Log(LogEntryType.Info, $"Deleting notification memory for lunch id {oldEntry.LunchId} ({oldEntry.LunchTime}). {oldEntry.GetType().Name}");
                        // byte trash;
                        // kvp.Value.TryRemove(oldEntry, out trash);
                    }
                }
            }
        }

        /// <summary>
        /// Tells whether or not a notification like this has been sent
        /// </summary>
        /// <param name="type">The type of notification</param>
        /// <param name="medium">The medium of the notification</param>
        /// <param name="memento">The details of the notification</param>
        /// <returns>Whether or not such a notification has been sent</returns>
        public Boolean HasNotificationBeenSent(NotificationType type, NotificationMedium medium, INotificationMemento memento)
        {
            return this.Memory[type][medium].ContainsKey(memento);
        }

        /// <summary>
        /// Remembers that a notification has been sent
        /// </summary>
        /// <param name="type">The type of notification</param>
        /// <param name="medium">The medium of the notification</param>
        /// <param name="memento">The details of the notification</param>
        public void RegisterNotificationSent(NotificationType type, NotificationMedium medium, INotificationMemento memento)
        {
            if (!this.Memory[type][medium].TryAdd(memento, 0))
            {
                throw new InvalidOperationException("Notification already sent");
            }
        }

        public void Dispose()
        {
            this.CleanupTask.Dispose();
        }
    }
}
