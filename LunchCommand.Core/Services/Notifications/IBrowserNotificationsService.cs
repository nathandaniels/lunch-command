﻿using System.Collections.Generic;
using LunchCommand.Core.Models;

namespace LunchCommand.Core.Services.Notifications
{
    public interface IBrowserNotificationsService
    {
        IEnumerable<Notification> GetBrowserNotifications(User user);
    }
}