﻿using System;

namespace LunchCommand.Core.Services.Notifications
{
    public interface IEmailNotificationService: IDisposable
    {
        void Begin();
    }
}
