﻿using System;

namespace LunchCommand.Core.Services.Notifications
{
    public interface INotificationMemento
    {
        DateTime LunchTime { get; }

        Int32 LunchId { get; }
    }
}
