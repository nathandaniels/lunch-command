﻿using System;
using LunchCommand.Core.Models;

namespace LunchCommand.Core.Services.Notifications
{
    public interface INotificationMemory: IDisposable
    {
        bool HasNotificationBeenSent(NotificationType type, NotificationMedium medium, INotificationMemento memento);
        void RegisterNotificationSent(NotificationType type, NotificationMedium medium, INotificationMemento memento);
    }
}