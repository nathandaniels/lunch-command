﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchCommand.Core.Services.Maps
{
    public interface IMapService
    {
        TimeSpan? GetTravelTime(String startAddress, String endAddress);
    }
}
