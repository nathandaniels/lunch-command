﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LunchCommand.Core.Models
{
    public abstract class TeamSpecificEntity: ITeamSpecificEntity
    {
        [Required]
        public virtual Int32 TeamId { get; set; }

        public virtual Team Team { get; set; }

        public virtual Boolean SharesTeamWith(ITeamSpecificEntity ent)
        {
            return this.TeamId == ent.TeamId;
        }
    }
}
