﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace LunchCommand.Core.Models
{
    public class Lunch : TeamSpecificEntity
    {
        public Lunch()
        {
            this.Users = new List<LunchUser>();
        }

        [Index]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 LunchId { get; set; }

        public Int32? VenueId { get; set; }

        public Int32? QuickVenueId { get; set; }

        public Int32? DissentingVenueId { get; set; }

        [Column("LunchTime")]
        public DateTime LunchTimeUtc { get; set; }

        [ForeignKey("VenueId")]
        public virtual Venue Venue { get; set; }

        [ForeignKey("QuickVenueId")]
        public virtual Venue QuickVenue { get; set; }

        [ForeignKey("DissentingVenueId")]
        public virtual Venue DissentingVenue { get; set; }

        public virtual ICollection<Proposition> Propositions { get; set; }

        public virtual ICollection<LunchUser> Users { get; set; }

        public Int32? PercentOfUsersAgreeing { get; set; }

        public Int32? PercentOfUsersDissenting { get; set; }

        public Int32? PercentOfUsersAgreeingToQuick { get; set; }

        [NotMapped]
        public IEnumerable<User> UsersGoing
        {
            get
            {
                return this.Users.Where(u => u.Decision).Select(u => u.User).ToArray();
            }
        }

        [NotMapped]
        public bool HasStarted
        {
            get
            {
                return this.LunchTimeUtc < DateTime.UtcNow;
            }
        }

        /// <summary>
        /// Gets all of the users who are going to lunch but are not part of a proposed group
        /// </summary>
        /// <returns></returns>
        public IEnumerable<User> GetUsersWhoAreGoingAndNotWithAProposedLunch()
        {
            return this.UsersGoing.Where(u => !this.Propositions.Any(p => p.ProposerId == u.Id || p.Followers.Any(f => f.UserId == u.Id))).ToArray();
        }
    }
}
