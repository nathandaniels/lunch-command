﻿namespace LunchCommand.Core.Models
{
    public enum NotificationMedium
    {
        None,
        Browser,
        Email
    }
}
