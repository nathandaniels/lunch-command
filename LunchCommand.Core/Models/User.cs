﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;

namespace LunchCommand.Core.Models
{
    public class User : IdentityUser<int, UserLogin, UserRole, UserClaim>, ITeamSpecificEntity
    {
        public User()
        {
            this.CreationDate = DateTime.Now;
            this.LastActionTime = this.CreationDate;
            this.LastTimeVenueDecisionMade = this.CreationDate;
        }

        [MaxLength(16)]
        [MinLength(1)]
        [Required]
        public String FirstName { get; set; }

        [MaxLength(16)]
        [MinLength(1)]
        [Required]
        public String LastName { get; set; }

        public String FullName
        {
            get
            {
                return String.Format("{0} {1}", this.FirstName, this.LastName);
            }
        }

        [Index("IX_User_UserName_TeamId", Order = 2, IsUnique = true)]
        [Required]
        [MaxLength(255)]
        [MinLength(3)]
        public override string UserName { get; set; }

        /// <summary>
        /// Gets or sets all of the decisions the user has made about venues
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<UserVenueChoice> VenueDecisions { get; set; }

        /// <summary>
        /// Gets or sets all of the decisions the user has made about lunch
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<LunchUser> LunchesDecided { get; set; }

        /// <summary>
        /// Gets the date this user was created
        /// </summary>
        [Required]
        public DateTime CreationDate { get; private set; }

        /// <summary>
        /// Gets or sets the last time the user has done anything in the system
        /// </summary>
        [Required]
        public DateTime LastActionTime { get; set; }

        /// <summary>
        /// Gets or sets the last time the user upodated their venue choices
        /// </summary>
        [Required]
        public DateTime LastTimeVenueDecisionMade { get; set; }

        /// <summary>
        /// Gets or sets the team id of the user
        /// </summary>
        [Index("IX_User_UserName_TeamId", Order = 1, IsUnique = true)]
        public virtual Int32? TeamId { get; set; }

        /// <summary>
        /// Gets or sets the exiled team id of the user
        /// </summary>
        public virtual Int32? ExiledTeamId { get; set; }

        [Index("IX_User_Email", IsUnique = true)]
        [MaxLength(256)]
        public override string Email
        {
            get
            {
                return base.Email;
            }

            set
            {
                base.Email = value;
            }
        }

        /// <summary>
        /// Gets or sets the notification settings for this user
        /// </summary>
        [InverseProperty("User")]
        public virtual UserNotificationSettings NotificationSettings { get; set; }

        /// <summary>
        /// Gets or sets the team of this user
        /// </summary>
        [ForeignKey("TeamId")]
        public virtual Team Team { get; set; }

        /// <summary>
        /// Gets or sets the team this use has been exiled from
        /// </summary>
        [ForeignKey("ExiledTeamId")]
        public virtual Team ExiledTeam { get; set; }

        public virtual Int32? InvitedById { get; set; }

        [ForeignKey("InvitedById")]
        public virtual User InvitedBy { get; set; }

        /// <summary>
        /// Gets all of the lunches this user has agreed to got to
        /// </summary>
        [JsonIgnore]
        public IEnumerable<Lunch> LunchesEaten
        {
            get
            {
                return this.LunchesDecided.Where(l => l.Decision).Select(l => l.Lunch).ToArray();
            }
        }

        /// <summary>
        /// Get all of the venues that this user loves (and not the ones they are indifferent towards or hate)
        /// </summary>
        [JsonIgnore]
        public IEnumerable<Venue> VenuesPreferring
        {
            get
            {
                if (this.VenueDecisions == null)
                {
                    return new Venue[0];
                }

                return this.VenueDecisions.Where(v => v.Degree == DegreeOfPreference.WantToGo).Select(v => v.Venue);
            }
        }

        /// <summary>
        /// Get all of the venues that this user hates (and not the ones they are indifferent towards or love)
        /// </summary>
        [JsonIgnore]
        public IEnumerable<Venue> VenuesRefusing
        {
            get
            {
                if (this.VenueDecisions == null)
                {
                    return new Venue[0];
                }

                return this.VenueDecisions.Where(v => v.Degree == DegreeOfPreference.WillNotGo).Select(v => v.Venue);
            }
        }

        [NotMapped]
        [JsonIgnore]
        int ITeamSpecificEntity.TeamId
        {
            get
            {
                return this.TeamId.GetValueOrDefault(-1);
            }

            set
            {
                this.TeamId = value;
            }
        }
    }

    public class UserLogin : IdentityUserLogin<int> { }

    public class UserRole : IdentityUserRole<int> { }

    public class UserClaim : IdentityUserClaim<int> { }

    public class Role : IdentityRole<int, UserRole> { }
}