﻿using System;

namespace LunchCommand.Core.Models.Settings
{
    public interface ISettings
    {
        /// <summary>
        /// The key used for bing maps
        /// </summary>
        String BingMapsKey { get; }

        /// <summary>
        /// Exactly what it says so verbosely
        /// </summary>
        Int32 NumberOfDaysBeforeVenuesAreConsideredEquallyOld { get; }

        /// <summary>
        /// The maximum ratio of outliers (venues with no likes nor disliked) to all venues allowed.  This limits 
        /// the likelyhood of outiers being picked when there are small groups
        /// </summary>
        Double MaximumLikelyhoodOfOutlierBeingPicked { get; }

        /// <summary>
        /// The URL for the reCAPTCHA verification service from google
        /// </summary>
        Uri GoogleReCaptchaUrl { get; }

        /// <summary>
        /// The query parameter that the recaptcha value is stored in 
        /// </summary>
        String GoogleReCaptchaQueryParamName { get; }

        /// <summary>
        /// The secret key for the reCAPTCHA verification service from google
        /// </summary>
        String GoogleReCaptchaSecretKey { get; }

        /// <summary>
        /// Whether or not reCAPTCHA is used in this application
        /// </summary>
        Boolean EnableReCaptcha { get; }

        /// <summary>
        /// Gets the settings for emails
        /// </summary>
        IMailSettings MailSettings { get; }
    }
}
