﻿using System;

namespace LunchCommand.Core.Models.Settings
{
    public interface IMailSettings
    {
        String InvitationFromAddress { get; }

        String PasswordResetFromAddress { get; }

        String SmtpServer { get; }

        UInt16 SmtpPort { get; }

        String SmtpUserName { get; }

        String SmtpPassword { get; }
    }
}
