﻿using System;

namespace LunchCommand.Core.Models.Data
{
    public interface IInviteRepository
    {
        Invite FindByCode(String code);
        Invite Generate(User user);
        Invite Generate(Team team);
        void Delete(Invite invite);
    }
}