﻿using System.Collections.Generic;

namespace LunchCommand.Core.Models.Data
{
    public interface IGenreRepository
    {
        IEnumerable<Genre> FindAll();
    }
}
