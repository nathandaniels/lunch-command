﻿using System;
using LunchCommand.Core.Models;

namespace LunchCommand.Core.Models.Data
{
    public interface ILunchRepository
    {
        Lunch FindLunchOfDay(DateTime date, Team team);
        void Save(Lunch lunch);
        Lunch FindById(Int32 id);
    }
}