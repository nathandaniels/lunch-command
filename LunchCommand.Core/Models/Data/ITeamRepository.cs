﻿using System;

namespace LunchCommand.Core.Models.Data
{
    public interface ITeamRepository
    {
        Team FindByName(String name);
        Team FindById(Int32 id);
        void Save(Team team);
    }
}