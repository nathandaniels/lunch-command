﻿using System;

namespace LunchCommand.Core.Models.Data
{
    public interface IVenueRepository
    {
        Venue FindById(Int32 id);
        void Save(Venue venue);
    }
}
