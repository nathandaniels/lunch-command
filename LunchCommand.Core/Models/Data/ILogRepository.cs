﻿using LunchCommand.Core.Models.Logging;

namespace LunchCommand.Core.Models.Data
{
    public interface ILogRepository
    {
        void SaveLogEntry(LogEntry entry);
    }
}