﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchCommand.Core.Models.Data
{
    public interface IPropositionRepository
    {
        void Save(Proposition proposition);
        void Delete(Proposition proposition);
    }
}
