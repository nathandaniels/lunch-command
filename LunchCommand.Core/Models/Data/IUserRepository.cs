﻿using System;
using System.Collections.Generic;

namespace LunchCommand.Core.Models.Data
{
    public interface IUserRepository
    {
        User FindById(Int32 id);

        User Update(User user);

        IEnumerable<User> FindAllActiveWithEmailNotifications();
    }
}
