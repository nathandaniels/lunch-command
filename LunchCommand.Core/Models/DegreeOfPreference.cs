﻿namespace LunchCommand.Core.Models
{
    public enum DegreeOfPreference
    {
        WillNotGo = -1,
        WillGo = 0,
        WantToGo = 1
    }
}