﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchCommand.Core.Models
{
    public class PropositionFollowing
    {
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [Key, Column(Order = 1)]
        public Int32 UserId { get; set; }

        [ForeignKey("PropositionId")]
        public virtual Proposition Proposition { get; set; }

        [Key, Column(Order = 0)]
        public Int32 PropositionId { get; set; }
    }
}
