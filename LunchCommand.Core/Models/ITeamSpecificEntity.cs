﻿using System;

namespace LunchCommand.Core.Models
{
    public interface ITeamSpecificEntity
    {
        Int32 TeamId { get; set; }

        Team Team { get; set; }
    }
}
