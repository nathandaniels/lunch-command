﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchCommand.Core.Models
{
    public class UserVenueChoice
    {
        [Key, Column(Order = 0)]
        public virtual Int32 UserId { get; set; }

        [Key, Column(Order = 1)]
        public virtual Int32 VenueId { get; set; }

        public virtual User User { get; set; }

        public virtual Venue Venue { get; set; }

        public virtual DegreeOfPreference Degree { get; set; }
    }
}