﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchCommand.Core.Models
{
    public class Invite
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Int32 InviteId { get; set; }

        [Index(IsUnique = true)]
        [MaxLength(100)]
        [MinLength(3)]
        [Required]
        public virtual String InviteCode { get; set; }

        public virtual Int32 TeamId { get; set; }

        public virtual Team Team { get; set; }

        public Int32? CreatingUserId { get; set; }

        [ForeignKey("CreatingUserId ")]
        public virtual User CreatingUser { get; set; }
    }
}