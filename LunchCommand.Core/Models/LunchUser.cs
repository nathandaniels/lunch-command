﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchCommand.Core.Models
{
    /// <summary>
    /// Represents the association between a lunch and a user
    /// </summary>
    public class LunchUser
    {
        public LunchUser()
        {

        }

        [Key, Column(Order = 1)]
        public virtual Int32 UserId { get; set; }

        [Key, Column(Order = 0)]
        public virtual Int32 LunchId { get; set; }

        public virtual User User { get; set; }

        public virtual Boolean Decision { get; set; }

        public virtual Lunch Lunch { get; set; }
    }
}