﻿using System;
namespace LunchCommand.Core.Models
{
    public class Notification
    {
        public Notification(NotificationType type, String message)
        {
            this.Type = type;
            this.Message = message;
        }

        public String Message { get; set; }

        public NotificationType Type { get; set; }

        public Int32? PropositionId { get; set; }
    }

    public enum NotificationType
    {
        LunchStarted,
        NotDecided,
        NewProposition
    }
}
