﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchCommand.Core.Models.Logging
{
    public interface ILogger
    {
        void Log(LogEntryType entryType, String message);
    }
}
