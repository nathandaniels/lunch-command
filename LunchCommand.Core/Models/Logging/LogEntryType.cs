﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LunchCommand.Core.Models.Logging
{
    public enum LogEntryType
    {
        Info,
        Warn,
        Error
    }
}