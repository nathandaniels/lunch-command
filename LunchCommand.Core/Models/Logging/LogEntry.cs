﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LunchCommand.Core.Models.Logging
{
    public class LogEntry
    {
        [Required]
        [Index]
        public LogEntryType Type { get; set; }

        [Required]
        [Index]
        public DateTime Timestamp { get; set; }

        [Required]
        public String Message { get; set; }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 EntryId { get; set; }
    }
}