﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace LunchCommand.Core.Models
{
    public class Genre
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 GenreID { get; set; }

        [Index(IsUnique = true)]
        [MaxLength(255)]
        [MinLength(3)]
        [Required]
        public String Name { get; set; }

        [JsonIgnore]
        public virtual ICollection<Venue> Venues { get; set; }

        public Int32 VenueCount
        {
            get
            {
                if (this.Venues == null)
                {
                    return 0;
                }

                return this.Venues.Count();
            }
        }

        public Int32 GetDaysSinceLastVisited(Boolean asQuick, DateTime def)
        {
            return this.Venues.Min(v => v.GetDaysSinceLastVisit(asQuick, def));
        }

        /// <summary>
        /// Makes it esaier to create these on database initialization
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static implicit operator Genre(String name)
        {
            return new Genre()
            {
                Name = name
            };
        }
    }
}