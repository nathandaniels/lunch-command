﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchCommand.Core.Models
{
    public class Proposition
    {
        public Proposition()
        {
            this.Followers = new List<PropositionFollowing>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int32 PropositionId { get; set; }

        [Index("IX_Proposition_LunchId_ProposerId", IsUnique = true, Order = 1)]
        public Int32 ProposerId { get; set; }

        public Int32 VenueId { get; set; }

        [Index("IX_Proposition_LunchId_ProposerId", IsUnique = true, Order = 0)]
        public Int32 LunchId { get; set; }
        
        [ForeignKey(nameof(ProposerId))]
        public virtual User ProposedBy { get; set; }

        [ForeignKey(nameof(VenueId))]
        public virtual Venue Venue { get; set; }

        [ForeignKey(nameof(LunchId))]
        public virtual Lunch Lunch{ get; set; }

        [InverseProperty(nameof(PropositionFollowing.Proposition))]
        public virtual ICollection<PropositionFollowing> Followers { get; set; }
    }
}