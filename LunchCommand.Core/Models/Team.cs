﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace LunchCommand.Core.Models
{
    public class Team
    {
        public Team()
        {
            this.Venues = new List<Venue>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Int32 TeamId { get; set; }

        [Index(IsUnique = true)]
        [MaxLength(50)]
        [MinLength(3)]
        [Required]
        public String Name { get; set; }

        public String TeamAddress { get; set; }

        [InverseProperty("Team")]
        public virtual ICollection<User> Users { get; set; }

        [InverseProperty("ExiledTeam")]
        public virtual ICollection<User> ExiledUsers { get; set; }

        public virtual ICollection<Lunch> Lunches { get; set; }

        public virtual ICollection<Venue> Venues { get; set; }

        public Int32 AdminRoleId { get; set; }

        public virtual Role AdminRole { get; set; }

        public Int32 LunchStartTimeMinutes { get; set; }

        [NotMapped]
        public TimeSpan LunchStartTimeUtc
        {
            get
            {
                return new TimeSpan(0, this.LunchStartTimeMinutes, 0);
            }
            set
            {
                this.LunchStartTimeMinutes = (int)value.TotalMinutes;
            }
        }

        [NotMapped]
        public IEnumerable<Venue> ActiveVenues
        {
            get
            {
                return this.Venues.Where(v => !v.Deleted);
            }
        }

        [NotMapped]
        public Boolean IsEmpty
        {
            get
            {
                return !(this.Users.Any() || this.AdminRole.Users.Any());
            }
        }

        public Boolean UserIsAdmin(User user)
        {
            return this.Users.Any(u => u.Id == user.Id) && this.AdminRole.Users.Any(ur => ur.UserId == user.Id);
        }

        public void AddAdmin(User user)
        {
            if (this.Users.Any(u => u.Id == user.Id))
            {
                this.AdminRole.Users.Add(new UserRole() { RoleId = this.AdminRole.Id, UserId = user.Id });
            }
            else
            {
                throw new InvalidOperationException("User is not a member of the team and cannot be made an admin");
            }
        }

        public void RemoveAdmin(User user)
        {
            if (this.Users.Any(u => u.Id == user.Id))
            {
                this.AdminRole.Users.Remove(this.AdminRole.Users.FirstOrDefault(u => u.UserId == user.Id));
            }
            else
            {
                throw new InvalidOperationException("User is not a member of the team and cannot be made an admin");
            }
        }

        public User RemoveUser(Int32 UserId)
        {
            var user = this.Users.FirstOrDefault(u => u.Id == UserId);
            if (user != null)
            {
                user.Team = null; ;
                user.ExiledTeam = this;
                return user;
            }
            else
            {
                throw new InvalidOperationException("User is not a member of the team and cannot be removed");
            }
        }
    }
}