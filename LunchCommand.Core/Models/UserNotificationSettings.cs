﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace LunchCommand.Core.Models
{
    /// <summary>
    /// A user's settings for notifications
    /// </summary>
    public class UserNotificationSettings
    {
        /// <summary>
        /// Creates a new notifications settings
        /// </summary>
        public UserNotificationSettings()
        {
        }

        /// <summary>
        /// Creates a new notifications settings
        /// </summary>
        /// <param name="user">The corresponding user</param>
        public UserNotificationSettings(User user)
        {
            this.User = user;
        }

        /// <summary>
        /// The ID of the user
        /// </summary>
        [Key, ForeignKey(nameof(User))]
        public virtual Int32 UserId { get; set; }

        /// <summary>
        /// The user whose settings these are.
        /// </summary>
        [InverseProperty(nameof(Models.User.NotificationSettings))]
        public virtual User User { get; set; }

        /// <summary>
        /// Gets or sets the medium by which the user wishes to revieve notifications.
        /// </summary>
        [Required]
        public NotificationMedium Medium { get; set; } = NotificationMedium.Browser;

        /// <summary>
        /// If they want to receive notifications when they have not yet decided whether or not they are going to lunch
        /// </summary>
        [Required]
        public Boolean NotifyNotDecided { get; set; } = true;

        /// <summary>
        /// If they want to receive notifications when lunch has been decided
        /// </summary>
        [Required]
        public Boolean NotifyLunchStarted { get; set; } = true;

        /// <summary>
        /// If the user should be notified of the start of lunch even if they hadn't decided whether or not they want to go
        /// </summary>
        [Required]
        public Boolean NotifyLunchStartedEvenWhenNotDecided { get; set; } = true;

        /// <summary>
        /// If they want to recive a notifiation when a venue is proposed
        /// </summary>
        [Required]
        public Boolean NotifyVenueProposed { get; set; } = true;

        /// <summary>
        /// How long before lunch starts to tell the user they have not decided
        /// </summary>
        [Required]
        public Int16 MinutesToNotifyOfIndecision { get; set; } = 10;

        /// <summary>
        /// Whether or not notifications are enabled for this user
        /// </summary>
        [NotMapped]
        public Boolean NotificationsEnabled
        {
            get
            {
                return this.Medium != NotificationMedium.None;
            }
        }
    }
}
