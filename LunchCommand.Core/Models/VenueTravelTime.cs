﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LunchCommand.Core.Models
{
    public class VenueTravelTime
    {
        public DateTime Obtained { get; set; }

        public TimeSpan? TravelTime { get; set; }
    }
}