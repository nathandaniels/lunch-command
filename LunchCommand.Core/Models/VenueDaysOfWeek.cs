﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace LunchCommand.Core.Models
{
    public class VenueDaysOfWeek
    {
        public VenueDaysOfWeek()
        {
            this.Monday = true;
            this.Tuesday = true;
            this.Wednesday = true;
            this.Thursday = true;
            this.Friday = true;
            this.Saturday = true;
            this.Sunday = true;
        }

        [Column("Mondays")]
        public virtual Boolean Monday { get; set; }

        [Column("Tuesdays")]
        public virtual Boolean Tuesday { get; set; }

        [Column("Wednesdays")]
        public virtual Boolean Wednesday { get; set; }

        [Column("Thursdays")]
        public virtual Boolean Thursday { get; set; }

        [Column("Fridays")]
        public virtual Boolean Friday { get; set; }

        [Column("Saturdays")]
        public virtual Boolean Saturday { get; set; }

        [Column("Sundays")]
        public virtual Boolean Sunday { get; set; }

        public Boolean CanGoOnDay(DayOfWeek dow)
        {
            // yay!
            return (bool)this.GetType().GetProperty(Enum.GetName(typeof(DayOfWeek), dow)).GetValue(this);
        }
    }
}